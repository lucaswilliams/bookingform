<div id="page-courses" class="booking-page-container <?php echo ($cid !== null ? 'prefilled' : ''); echo ($cid !== null || $iid > 0 ? ' nevershow' : ''); ?>">
    <div id="page-courses-1" class="booking-page">
        <h1><?php the_field('course_page_title', $page->ID); ?></h1>
        <?php
        $postmaster = get_posts([
            'post_type' => 'rfa_courses',
            'numberposts' => -1
        ]);

        $posts = [];

        foreach($postmaster as $c) {
            $cq = get_field('cq_first_aid', $c->ID);
            if(strpos($_SERVER['HTTP_HOST'], 'cq') !== false) {
                if($cq === true) {
                    $posts[] = $c;
                }
            } else {
                if ($cq === null || $cq === false) {
                    $posts[] = $c;
                }
            }
        }

        $coursemaps = [];
        $csmap = get_field('linked_courses', 5059);

        foreach($csmap as $map) {
            foreach($map['public_course'] as $pc) {
                $coursemaps[$pc]['public'][] = $pc;

                foreach($map['online_course'] as $oc) {
                    $coursemaps[$pc]['online'][] = $oc;
                }
            }

            foreach($map['online_course'] as $pc) {
                $coursemaps[$pc]['online'][] = $pc;

                foreach($map['public_course'] as $oc) {
                    $coursemaps[$pc]['public'][] = $oc;
                }
            }
        }

        //echo '<pre>'; var_dump($coursemaps); echo '</pre>';

        foreach($posts as $post) {
            if((count($allowed_courses) == 0 && get_field('private_course', $post->ID) != true) || isset($allowed_courses[$post->ID])) {
            $pricedata = get_field('pricing_information', $post->ID);
            $prices = array();
            if (is_array($pricedata) && count($pricedata) > 0) {
                foreach ($pricedata as $i => $price) {
                    if ($i == (count($pricedata) - 1)) {
                        $price['maximum_participants'] = 1000;
                    }

                    $prices[] = $price;
                }
            }
            $pricing = json_encode($prices, JSON_NUMERIC_CHECK );
            $pricing = str_replace("'", "\'", $pricing);

            $public = (int)get_field('available_as_public_course', $post->ID);
            $onsite = (int)get_field('available_as_onsite_course', $post->ID);
            $online = (int)get_field('available_as_online_course', $post->ID);
            $duration = (int)get_field('duration', $post->ID);
            $hours = (float)get_field('duration_hours', $post->ID);
            $second = (get_field('allow_second_participant', $post->ID) ? get_field('second_participant_cost', $post->ID) : -1);

            $axids = [];
            $axf2fids = [];
            $ax_details = get_field('axcelerate_options', $post->ID);
            foreach($ax_details as $detail) {
                //echo '<pre>'; var_dump($detail); echo '</pre>';
                if($detail['delivery_type'] == 0) {
                    $axids[] = $detail['course_id'];
                } else {
                    $axf2fids[] = $detail['course_id'];
                }
            }

            $axid = implode(',', $axids);
            $axf2fid = implode(',', $axf2fids);

            if(isset($coursemaps[$post->ID]['public'])) {
                $axid_pu = implode(',', $coursemaps[$post->ID]['public']);
            } else {
                $axid_pu = '';
            }
            if(isset($coursemaps[$post->ID]['online'])) {
                $axid_on = implode(',', $coursemaps[$post->ID]['online']);
            } else {
                $axid_on = '';
            }

            if($axf2fid != '') {
                $axf2fid = 'data-axf2fid="'.$axf2fid.'"';
            }

            $ctype = get_field('axcelerate_type', $post->ID) ?? 'w';

            $alsoactive = ($cid > 0 && strpos($axid, (string)$cid));
            $clocations = get_field('available_at', $post->ID);
            $cls = [];
            if(is_array($clocations) && count($clocations) > 0) {
                foreach($clocations as $cloc) {
                    //get the terms
                    $cls[] = $cloc;
                }
            }
            $clocation = implode(',', $cls);
            $category = '';
            $catID = get_the_terms($post->id, 'rfa_course_types');
            if(is_array($catID) && count($catID) > 0) {
                $category = $catID[0]->name;
            }
            $course_name = str_replace(' Geelong', '', $post->post_title);
            $result = ($cid !== null && ($cid == $post->ID || $alsoactive !== false || $cid == intval($axid)) ? 'active' : '');

            $equipment = get_field('needs_equipment', $post->ID) ?? 0;
            //echo '<pre>'; var_dump($cid); echo '<br>'; var_dump($post->ID); echo '<br>'; var_dump($axid); echo '<br>'; var_dump($result); echo '</pre>';
                $adata = [
                    'template' => '',
                    'coupon' => '',
                    'course_type' => '0'
                ];

                if(isset($allowed_courses[$post->ID])) {
                    $adata = $allowed_courses[$post->ID];
                }
            ?>
            <div class="training-course <?php echo $result; ?>" data-public="<?php echo $public ?>" data-onsite="<?php echo $onsite; ?>" data-online="<?php echo $online; ?>" data-id="<?php echo $post->ID; ?>" data-pricing='<?php echo $pricing; ?>' data-duration="<?php echo $duration; ?>" data-axid="<?php echo $axid; ?>" <?php echo $axf2fid; ?> data-hours="<?php echo $hours; ?>" data-category="<?php echo $category; ?>" data-locations="[<?php echo $clocation; ?>]" data-type="<?php echo $ctype; ?>" data-axid-pu="<?php echo $axid_pu; ?>" data-axid-on="<?php echo $axid_on; ?>" data-participant="<?php echo $second; ?>" data-equipment="<?php echo $equipment; ?>" data-template="<?php echo $adata['template']; ?>" data-coupon="<?php echo $adata['coupon']; ?>" data-pubpriv="<?php echo $adata['course_type']; ?>" data-price="<?php echo $adata['price']; ?>">
                <p><strong><?php echo $course_name; ?>
                        <?php
                        $keyinfo = get_field('key_information', $course->ID);
                        if(strlen($keyinfo) > 0) {
                            echo '<i class="fa fa-chevron-down"></i></strong></p>';
                            echo '<div class="key-info">'.$keyinfo.'</div>';
                        } else {
                            echo '</strong></p>';
                        }
                        ?>
            </div>
            <?php
            }
        }
        ?>

        <?php if(strlen($out_link_url) == 0) { ?>
            <div class="training-course" data-public="1" data-onsite="1" data-id="0">
                <p><strong>Other</strong></p>
            </div>
        <?php } ?>
    </div>
</div>