<div id="page-public-match" class="booking-page-container">
    <div id="page-public-match-1" class="booking-page">
        <h1><?php the_field('public_participant_page_title', $page->ID); ?></h1>
        <?php the_field('public_participant_page_text', $page->ID); ?>
        <form id="participant-match-form" class="form-group">

        </form>
        <div class="form-group">
            <button class="btn btn-primary pull-right" id="public-match-button">Next</button>
        </div>
    </div>
</div>