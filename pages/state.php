<?php
//var_dump($showLocations);
$onlineOnly = !$showLocations && ($haveOwnEquipment || ($showOnline && !$showPublic && !$showOnsite));
//var_dump($onlineOnly);
?>
<div id="page-state" class="booking-page-container <?php echo ($stateid !== null || $onlineOnly ? 'prefilled' : ''); echo ($stateid !== null || $iid > 0 || $onlineOnly ? ' nevershow' : ''); ?>">
    <div class="booking-page">
        <h1><?php the_field('location_page_title', $page->ID); ?></h1>
        <div class="row">
            <?php
            if($onlineOnly) {
                //dummy "other" location because it doesn't matter
            ?>
                <div class="training-state active" data-id="other" data-deliverfees="0" data-deliverdays="1">
                    <div class="content">
                        <div class="inner">
                            <img src="/booking/img/realresponse.png">
                            <p><strong>Other</strong></p>
                        </div>
                    </div>
                </div>
            <?php
            } else {
                foreach($states as $state)
                {
                    if((($geelong || $colac) && strtoupper($state->slug) == 'VIC') || (!$geelong && !$colac)) {
                        $deliveryfee = get_field('delivery_fees', 'rfa_states_'.$state->term_id);
                        $deliverydays = get_field('delivery_days', 'rfa_states_'.$state->term_id);
                        ?>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="training-state <?php echo (strtoupper($stateid) == strtoupper($state->slug) ? 'active' : ''); ?>" data-id="<?php echo strtoupper($state->slug); ?>" data-deliverfees="<?php echo $deliveryfee; ?>" data-deliverdays="<?php echo $deliverydays; ?>">
                                <div class="content">
                                    <div class="inner">
                                        <?php
                                        $img = '';
                                        $image = get_field('image', 'rfa_states_'.$state->term_id);
                                        if($image) {
                                            $img = $image['url'];
                                        }
                                        ?>
                                        <img src="<?php echo $img; ?>">
                                        <p><strong><?php echo $state->name; ?></strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                }
            }
            ?>

            <div class="row">
                <div class="col">
                    <?php the_field('location_page_text', $page->ID); ?>
                </div>
            </div>
        </div>
    </div>
</div>