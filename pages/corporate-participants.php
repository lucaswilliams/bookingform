<div id="page-corporate-participants" class="booking-page-container">
    <div id="page-corporate-participants-1" class="booking-page">
        <h1><?php the_field('participant_page_title', $page->ID); ?></h1>
        <div class="form-group row">
            <div class="col-6">
                <input type="number" id="participant-number" class="form-control" value="1" min="1" max="100">
            </div>
            <div class="col-6">
                <span id="participant-info"></span>
            </div>
        </div>
        <div class="form-group">
            <button class="btn btn-primary pull-right" id="participant-button">Next</button>
        </div>
    </div>
</div>