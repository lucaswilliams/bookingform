<div id="page-type" class="booking-page-container <?php echo ($tid !== null ? 'prefilled' : ''); echo ($tid !== null || $iid > 0 ? ' nevershow' : ''); ?>">
    <div class="booking-page">
        <h1><?php the_field('public_v_onsite_courses_title', $page->ID); ?></h1>
        <div class="row">
            <?php if($showOnline) { ?>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="training-type <?php echo ($tid === 2 ? 'active' : ''); ?>" data-id="2">
                    <div class="content">
                        <div class="inner">
                            <img src="https://realresponse.com.au/booking/img/online.png">
                            <p><strong>Online</strong></p>
                            <p><?php the_field('online_courses_text', $page->ID); ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>

            <?php if(!$geelong && !$colac && $showOnsite) { ?>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="training-type <?php echo ($tid === 0 ? 'active' : ''); ?>" data-id="0">
                        <div class="content">
                            <div class="inner">
                                <img src="https://realresponse.com.au/booking/img/onsite.png">
                                <p><strong>Onsite</strong></p>
                                <?php echo wpautop(get_field('onsite_courses_text', $page->ID)); ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

            <?php if($showPublic) { ?>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="training-type <?php echo ($tid === 1 ? 'active' : ''); ?>" data-id="1">
                    <div class="content">
                        <div class="inner">
                            <img src="https://realresponse.com.au/booking/img/public.png">
                            <p><strong>Public</strong></p>
                            <p><?php the_field('public_courses_text', $page->ID); ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>

            <?php if($showPrivate) { ?>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="training-type" data-id="3">
                        <div class="content">
                            <div class="inner">
                                <img src="https://realresponse.com.au/booking/img/private.png">
                                <p><strong>Private</strong></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>