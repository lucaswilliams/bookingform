<div id="page-public-participants" class="booking-page-container">
    <div id="page-public-participants-1" class="booking-page">
        <h1><?php the_field('public_participant_page_title', $page->ID); ?></h1>
        <?php the_field('public_participant_page_text', $page->ID); ?>
        <form id="participant-form">
            <div id="participant-booker" class="participant-details">

            </div>
            <div id="participants">

            </div>
            <input type="hidden" id="participant-instance" name="instance_id">
            <input type="hidden" id="participant-type" name="ctype">
        </form>
        <div class="form-group" id="add-participant-container">
            <button class="btn btn-primary" id="add-participant-button">Add participant</button>
        </div>
        <div class="form-group" id="too-many-participants">
            <div class="alert alert-danger">
                You cannot add any more participants as there <span id="places"></span>
            </div>
        </div>

        <div class="form-group" id="participant-count-error">
            <div class="alert alert-danger">
                You cannot make a booking for 0 participants.  Please add at least one participant, or select to make the booking for yourself.
            </div>
        </div>

        <div class="form-group">
            <button class="btn btn-primary pull-right" id="public-participant-button">Next</button>
        </div>

        <div class="participant-details" id="participant-details-template">
            <h2>Participant <span class="participant-number">1</span></h2>
            <div class="form-group">
                <label for="public-participant-first">First Name</label>
                <input type="text" id="public-participant-first" name="public-participant-first[]" class="form-control">
            </div>
            <div class="form-group">
                <label for="public-participant-last">Last Name</label>
                <input type="text" id="public-participant-last" name="public-participant-last[]" class="form-control">
            </div>
            <div class="form-group">
                <label for="public-participant-email">Email</label>
                <input type="text" id="public-participant-email" name="public-participant-email[]" class="form-control">
            </div>
            <div class="form-group">
                <label for="public-participant-phone">Phone</label>
                <input type="text" id="public-participant-phone" name="public-participant-phone[]" class="form-control">
            </div>
        </div>
    </div>
</div>