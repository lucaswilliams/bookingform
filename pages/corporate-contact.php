<div id="page-corporate-contact" class="booking-page-container">
    <div id="page-corporate-contact-1" class="booking-page">
        <h1><?php the_field('contact_page_title', $page->ID); ?></h1>
        <div class="form-group">
            <label for="contact-first">First Name</label>
            <input type="text" id="contact-first" class="form-control">
            <div class="alert alert-danger" id="error-first">Please enter your first name</div>
        </div>

        <div class="form-group">
            <label for="contact-last">Last Name</label>
            <input type="text" id="contact-last" class="form-control">
            <div class="alert alert-danger" id="error-last">Please enter your last name</div>
        </div>

        <div class="form-group">
            <label for="contact-email">Email</label>
            <input type="email" id="contact-email" class="form-control">
            <div class="alert alert-danger" id="error-email">Please enter a valid email</div>
        </div>

        <div class="form-group">
            <label for="contact-number">Contact No</label>
            <input type="text" id="contact-number" class="form-control">
            <div class="alert alert-danger" id="error-number">Please enter your phone</div>
        </div>

        <div class="form-group">
            <label for="contact-organisation">Organisation Name</label>
            <input type="text" id="contact-organisation" class="form-control">
            <div class="alert alert-danger" id="error-organisation">Please enter your organisation</div>
        </div>

        <div class="form-group">
            <label for="contact-position">Position / Job Title</label>
            <input type="text" id="contact-position" class="form-control">
        </div>

        <div class="form-group">
            <button class="btn btn-primary pull-right" id="contact-button">Next</button>
        </div>
    </div>
</div>