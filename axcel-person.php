<?php
    include 'config.php';

    //echo '<pre>'; var_dump($_POST); echo '</pre>';

    $out = '<p>You are booking this course for the following '.(count($_POST['public-participant-first']) == 1 ? 'person' : 'people').':</p>';
    
    $notnew = 0;

    $instance_id = $_REQUEST['instance_id'];
    $type = $_REQUEST['ctype'];

	foreach($_POST['public-participant-first'] as $key => $value) {
		$given = $value;
		$surname = $_POST['public-participant-last'][$key];
		$email = $_POST['public-participant-email'][$key];
		$phone = $_POST['public-participant-phone'][$key];
		$phoneFormatted = preg_replace('/[^0-9+]/', '', $phone);

		$service_url = $ax_url.'contacts/search?givenName='.urlencode($given).'&surname='.urlencode($surname);

		//echo $service_url;

		$headers = array(
			'WSToken: ' . $ws_token,
			'APIToken: ' . $api_token,
			'Expect: '
		);

		$curl = curl_init($service_url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
		if ($proxy) {
			curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
		}

		$curl_response = curl_exec($curl);
		$data = json_decode($curl_response);

		//echo '<pre>'; var_dump($data); echo '</pre>';

		$contact_id = 0;
		$fullMatches = 0;
		$partialMatches = 0;
		$matchArray = [];
		foreach ($data as $item) {
		    //verify email or phone
            $workphone = preg_replace('/[^0-9+]/', '', $item->WORKPHONE);
            $mobilephone = preg_replace('/[^0-9+]/', '', $item->MOBILEPHONE);
            $homephone = preg_replace('/[^0-9+]/', '', $item->PHONE);
			if($email == $item->EMAILADDRESS && ($phoneFormatted == $mobilephone || $phoneFormatted == $workphone || $phoneFormatted == $homephone)) {
				$fullMatches++;
				$fullMatchId = $item->CONTACTID;
			} elseif($email == $item->EMAILADDRESS || $phoneFormatted == $mobilephone || $phoneFormatted == $workphone || $phoneFormatted == $homephone) {
				$partialMatches++;
				$partialMatchId = $item->CONTACTID;
            }
            $contact_id = $item->CONTACTID;
            $matchArray[] = $item;
		}

		//echo '<pre>'; var_dump($matchArray); echo '</pre>';
        $out .= '<div class="participant-match"
					  data-given="'.$given.'"
					  data-surname="'.$surname.'"
					  data-email="'.$email.'"
					  data-phone="'.$phone.'">';
		//echo 'Full: '.$fullMatches.'; Partial: '.$partialMatches;

        $recs = 'no matches';

        if(count($data) == 0) {
            $out .= '<div class="matchInfo">';
            $out .= '<p><strong>'.$given.' '.$surname.'</strong></p>';
            $out .= '<p>This person was not found in our system and will be created.</p>';
            $out .= '<input type="hidden" name="public-participant-id[]" class="public-participant public-participant-detail" value="0">';
            $out .= '<input type="hidden" name="participant-given[]" class="public-participant-detail" value="'.$given.'">';
            $out .= '<input type="hidden" name="participant-surname[]" class="public-participant-detail" value="'.$surname.'">';
            $out .= '<input type="hidden" name="participant-email[]" class="public-participant-detail" value="'.$email.'">';
            $out .= '<input type="hidden" name="participant-phone[]" class="public-participant-detail" value="'.$phone.'">';
            $out .= '</div>';
        } else {
            $notnew++;
            if ($fullMatches == 1) {
                $recs = 'one full';
                //nothing, this is just the person
                $out .= singleMatch($given, $surname, $email, $phone, $fullMatchId, $instance_id, $type);
            } else {
                $recs = 'many full';
                $contact_id = 0;
                $out .= multipleMatches($matchArray, $given, $surname, $email, $phone, $instance_id, $type);
            }
        }
        
        $out .= '</div>';
	}

	global $wpdb;
	$wpdb->insert('wp_rr_axcel_person', [
	    'postdata' => json_encode($_POST),
        'out' => $out,
        'recs' => $recs,
        'data' => json_encode($data)
    ]);

	$errors = strpos($out, 'alert-danger');

	header('HTTP/1.1 200 OK');
	echo json_encode(array('notnew' => $notnew, 'html' => $out, 'errors' => $errors));
	
	function singleMatch($given, $surname, $email, $phone, $contact_id, $instance_id, $type) {
        $ret = '<p><strong>'.$given.' '.$surname.'</strong></p>';
        $ret .= '<p>'.$email.'</p>';
        $ret .= '<p>'.$phone.'</p>';
        $ret .= '<input type="hidden" name="public-participant-id[]" class="public-participant public-participant-detail" value="'.$contact_id.'">
            <input type="hidden" name="participant-given[]" class="public-participant-detail" value="'.$given.'">
            <input type="hidden" name="participant-surname[]" class="public-participant-detail" value="'.$surname.'">
            <input type="hidden" name="participant-email[]" class="public-participant-detail" value="'.$email.'">
            <input type="hidden" name="participant-phone[]" class="public-participant-detail" value="'.$phone.'">';

        //Are they enrolled
       $ret .= checkEnrolled($contact_id, $instance_id, $type);
        
        return $ret;
    }
	
	function multipleMatches($matchArray, $given, $surname, $email, $phone, $instance_id, $type) {
        $ret = '';

        $ret .= '<div class="matchInfo">';
        foreach($matchArray as $match) {
            $ret .= '<div class="pMatch" 
            			  data-id="'.$match->CONTACTID.'" 
            			  data-given="'.$match->GIVENNAME.'"
            			  data-surname="'.$match->SURNAME.'"
            			  data-dob="'.$match->DOB.'"
            			  data-usi="'.$match->USI.'"
            			  data-email="'.$match->EMAILADDRESS.'"
            			  data-phone="'.$match->MOBILEPHONE.'">';


            $ret .= checkEnrolled($match->CONTACTID, $instance_id, $type);

            $ret .= '</div>';
        }

        $ret .= '<p><strong>'.$given.' '.$surname.'</strong></p>';
        //$ret .= '<p>'.$email.'</p>';
        //$ret .= '<p>'.$phone.'</p>';
        $ret .= '<p class="text-danger">Multiple records were found for this person in our system.  Please enter their DOB to match the person with the correct user in our system.  Alternatively click "next" to continue without matching and a new user will be created.</p>';
        $ret .= '<div class="row">';
        $ret .= '<div class="col-12 col-md-7">
			<label style="width: 100%;">Date of Birth</label>
			<select class="form-control input-dob-m" style="width: 40%; float: left;">
			    <option value="">Month</option>
			    <option value="01">Jan</option>
			    <option value="02">Feb</option>
			    <option value="03">Mar</option>
			    <option value="04">Apr</option>
			    <option value="05">May</option>
			    <option value="06">Jun</option>
			    <option value="07">Jul</option>
			    <option value="08">Aug</option>
			    <option value="09">Sep</option>
			    <option value="10">Oct</option>
			    <option value="11">Nov</option>
			    <option value="12">Dec</option>
            </select>
			<input type="text" class="form-control input-dob-d" placeholder="DD" style="width: 30%; float: left;">
			<input type="text" class="form-control input-dob-y" placeholder="YYYY" style="width: 30%; float: left;">';
		/*$ret .=	'<input type="hidden" name="participant-given[]" class="public-participant-detail" value="'.$given.'">
            <input type="hidden" name="participant-surname[]" class="public-participant-detail" value="'.$surname.'">
            <input type="hidden" name="participant-email[]" class="public-participant-detail" value="'.$email.'">
            <input type="hidden" name="participant-phone[]" class="public-participant-detail" value="'.$phone.'">';*/
		$ret .= '</div>';
        $ret .= '<div class="col-10 col-md-3">';
		//$ret .= '	<label>USI</label>';
		//$ret .= '	<input type="text" class="form-control input-usi">';
		$ret .= '</div>';
        $ret .= '<div class="col-2">
			<button type="button" class="btn btn-success match-button pull-right">Verify</button>
		</div>';
        $ret .= '<input type="hidden" name="public-participant-id[]" class="public-participant public-participant-detail" value="0">
            <input type="hidden" name="participant-given[]" class="public-participant-detail" value="'.$given.'">
            <input type="hidden" name="participant-surname[]" class="public-participant-detail" value="'.$surname.'">
            <input type="hidden" name="participant-email[]" class="public-participant-detail" value="'.$email.'">
            <input type="hidden" name="participant-phone[]" class="public-participant-detail" value="'.$phone.'">';
        $ret .= '</div>';
        $ret .= '</div>';
        
        return $ret;
    }

    function checkEnrolled($contact_id, $instance_id, $type) {
	    global $ax_url, $ws_token, $api_token, $proxy;
        $service_url = $ax_url.'course/enrolments?contactID='.$contact_id.'&instanceID='.$instance_id.'&type='.$type;

        //echo $service_url;

        $headers = array(
            'WSToken: ' . $ws_token,
            'APIToken: ' . $api_token,
            'Expect: '
        );

        $curl = curl_init($service_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        if ($proxy) {
            curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        }

        $curl_response = curl_exec($curl);
        $data = json_decode($curl_response);
        //var_dump($data);

        return (count($data) > 0 ? '<div class="alert alert-danger">This person is already enrolled in the selected course.</div>' : '');
    }