<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
include('config.php');

require_once((__DIR__).'/vendor/autoload.php');
    
$webhook = $webhooks[$_POST['calendar']];
$client = $_POST['client'];
$date = date('d/m/Y', strtotime($_POST['date']));
$time = $_POST['time'];
$finishtime = $_POST['finish-time'];
$type = $_POST['type'];
$course = $_POST['course'];
$participants = $_POST['participants'];
$first_name = $_POST['first_name'];
$last_name = $_POST['last_name'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$position = $_POST['position'];
$street = $_POST['street_name'];
$city = $_POST['city'];
$state = $_POST['state'];
$postcode = $_POST['postcode'];
$comments = $_POST['comments'];
$invoice = $_POST['chargeid'];
$price = $_POST['price'];

$slackdemo = new Slack("https://hooks.slack.com/services/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");
$slackdemo->setDefaultUsername("Booking Form Robot");
$slackdemo->setDefaultChannel("#logging-rr");
// Set the default icon for messages to a custom image
$slackdemo->setDefaultIcon("https://www.realresponse.com.au/booking/img/realresponse.png");

//var_dump($invoice);
//echo '<br>';
//var_dump(strlen($invoice));
//echo '<br>';

if(strlen($invoice) > 0) {
    $admin_invoice = "Paid with Credit Card.";
} else {
    //Get the extra words
    $posts = get_posts([
        'post_type' => 'page',
        'name' => 'booking'
    ]);
    if($posts) {
        $page = $posts[0];
    }
    
    $invoice_words = "
    
".get_field('invoice_text', $page->ID);
    
    $admin_invoice = "An invoice is required.";
}

//var_dump($admin_invoice);
//die();

if(strlen($comments) > 0) {
    $comment = "

Additional Information: $comments";
}

$description = "<p>Dear $first_name,</p>
<p>Your booking for training has been received and is being processed by our team. You will receive a formal confirmation email shortly.$invoice_words</p>

<p>For your information here are your booking details:</p>
<ul>
<li>Course type: $course</li>
<li>Number of participants: $participants</li>
<li>Course date: $date</li>
<li>Preferred time: $time - $finishtime</li>
<li>Contact: $first_name $last_name ".(strlen($position) > 0 ? '('.$position.')' : '')."</li>
<li>Address: $street<br>
$city $state $postcode</li>
</ul>
<p>$comment</p>
<p>Feel free to contact our team at any point with any questions you may have at training@realresponse.com.au or on 03 9021 8156.</p>

<p>Regards<br>
The Real Response Team</p>";
    
    $admin_email = "<p>A new booking has arrived through the ".$_POST['createdfrom'].".</p>
<p>The details are as follows</p>
<ul>
<li>Course: $course</li>
<li>Course date: $date</li>
<li>Preferred time: $time - $finishtime</li>
<li>Participants: $participants</li>
<li>Price: $price</li>
<li>Contact: $first_name $last_name ".(strlen($position) > 0 ? '('.$position.')' : '')."<br>
$client<br>
$email<br>
$phone</li>
<li>Address: $street<br>
$city $state $postcode</li>
</ul>

<p>$comment</p>

<p>$admin_invoice</p>";

$admin_email .= '<ul></ul><li>Client ID: '.$_POST['clientid'].'</li>';
$admin_email .= '<li>GCLID: '.$_POST['gclid'].'</li>';
$admin_email .= '<li>Traffic Source: '.$_POST['trafficsource'].'</li></ul>';

// Create a new message
$messageDemo = new SlackMessage($slackdemo);
//$message->setText("New corporate booking from $first_name $last_name in $course on $date.");
$messageDemo->setText("webhook: $webhook \n
client: $client \n
date: $date \n
time: $time \n
finishtime: $finishtime \n
type: $type \n
course: $course \n
participants: $participants \n
first_name: $first_name \n
last_name: $last_name \n
email: $email \n
phone: $phone \n
position: $position \n
street: $street \n
city: $city \n
state: $state \n
postcode: $postcode \n
comments: $comments \n
invoice: $invoice \n
price: $price \n
$admin_email");
//$messageDemo->send();
die($admin_email);
    
    
//send description to email address
require_once 'PHPMailer/src/Exception.php';
require_once 'PHPMailer/src/PHPMailer.php';
require_once 'PHPMailer/src/SMTP.php';
$msgs = [];
$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    if($mail_type == 'gmail') {
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $mail_user;                 // SMTP username
        $mail->Password = $mail_pass;                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to
    } else {
        //Mailtrap for local development.
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.mailtrap.io';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $mail_user;                 // SMTP username
        $mail->Password = $mail_pass;                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 2525;                                    // TCP port to connect to
    }

    //Recipients
    $mail->setFrom('calendarreal112@gmail.com', 'Real Response');
    $mail->addReplyTo('training@realresponse.com.au', 'Real Response');
    $mail->addAddress($email, $first_name.' '.$last_name);

   //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Course Booking';
    $mail->Body    = str_replace("\r\n", '<br>', $description);
    $mail->AltBody = $description;

    $mail->send();

    $mail->clearAddresses();
    $mail->clearAllRecipients();
    //Recipients
    $mail->setFrom('calendarreal112@gmail.com', 'Real Response');
    $mail->addReplyTo($email, $first_name.' '.$last_name);
    if(intval($_REQUEST['geelong']) == 1) {
        $mail->addAddress('jross@realresponse.com.au', 'Real Response');
        $mail->addAddress('csnowden@realresponse.com.au', 'Real Response');
        $mail->addAddress('sgroen@realresponse.com.au', 'Real Response');
    } else {
        $mail->addAddress('sales@realresponse.com.au', 'Real Response');
    }

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'New Course Booking';
    $mail->Body    = str_replace("\r\n", '<br>', $admin_email);
    $mail->AltBody = $admin_email;

    $mail->send();
    $msgs[] = 'Message has been sent';
} catch (Exception $e) {
    $msgs[] = 'Message could not be sent. Mailer Error: '. $mail->ErrorInfo;
}

$isodate = date('Y-m-d', strtotime($_POST['date']));

$fields = http_build_query(array(
    'start' => $isodate.' '.$time,
    'end' => $isodate.' '.$finishtime,
    'title' => $client.' ('.$type.')',
    'description' => $description
));

//var_dump($webhook);
//var_dump($fields);
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, $webhook);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = curl_exec($ch);

curl_close ($ch);

global $wpdb;
$wpdb->insert('wp_rr_bookings', [
    'course_id' => $_POST['course_id'],
    'location_id' => $_POST['location_id'],
    'booking_contact' => $first_name.' '.$last_name,
    'booking_content' => $admin_email,
    'coupon' => '',
    'booking_date' => date('Y-m-d H:i:s'),
    'course_date' => $date,
    'price' => $price,
    'participants' => $participants,
    'places' => $participants,
    'clientid' => $_POST['clientid'],
    'gclid' => $_POST['gclid'],
    'trafficsource' => $_POST['trafficsource']
]);

$slack = new Slack('https://hooks.slack.com/services/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');
if(strpos($_SERVER['SERVER_NAME'], '.local') > 0) {
    $slack->setDefaultUsername("Booking Form Robot - Test Mode");
} else {
    $slack->setDefaultUsername("Booking Form Robot");
}
$slack->setDefaultChannel("#sales");

// Set the default icon for messages to a custom image
$slack->setDefaultIcon("https://www.realresponse.com.au/booking/img/realresponse.png");

// Create a new message
$message = new SlackMessage($slack);
//$message->setText("New corporate booking from $first_name $last_name in $course on $date.");
$message->setText(strip_tags($admin_email));
if(strpos($_SERVER['HTTP_HOST'], '.local') === FALSE) {
    $message->send();
}

header('HTTP/1.1 200 OK');
$success = new stdClass();
$success->success = true;
$success->messages = $msgs;
echo json_encode($success);