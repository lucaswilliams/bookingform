<?php
header("HTTP/1.1 200 OK");
require('config.php');
$posts = get_posts([
    'post_type' => 'page',
    'name' => 'booking'
]);
if($posts) {
    $page = $posts[0];
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta name="google-site-verification" content="gUXCJmVa2EAIIX4PvkEjAt1kSSHQ2Q2xT0zditzkXjk" />
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <title>Booking - Real Response</title>

    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="apple-touch-icon" sizes="76x76" href="../favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../favicon/favicon-16x16.png">
    <link rel="manifest" href="../favicon/site.webmanifest">
    <link rel="mask-icon" href="../favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <!-- Gravity Forms -->
    <link rel='stylesheet' id='gforms_reset_css-css'  href='<?php echo site_url(); ?>/wp-content/plugins/gravityforms/css/formreset.min.css?ver=2.3.6' type='text/css' media='all' />
    <link rel='stylesheet' id='gforms_formsmain_css-css'  href='<?php echo site_url(); ?>/wp-content/plugins/gravityforms/css/formsmain.min.css?ver=2.3.6' type='text/css' media='all' />
    <link rel='stylesheet' id='gforms_ready_class_css-css'  href='<?php echo site_url(); ?>/wp-content/plugins/gravityforms/css/readyclass.min.css?ver=2.3.6' type='text/css' media='all' />
    <link rel='stylesheet' id='gforms_browsers_css-css'  href='<?php echo site_url(); ?>/wp-content/plugins/gravityforms/css/browsers.min.css?ver=2.3.6' type='text/css' media='all' />
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="css/perfect-scrollbar.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <link rel="stylesheet" href="css/style.css">
    <style>
        #page-finish {
            display: block !important;
        }
        
        .booking-page-container .btn {
            width: calc(50% - 2px);
            margin: 0;
        }
        
        #elearning {
            width: 100%;
            margin-bottom: 48px;
        }
    </style>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <!-- Start Visual Website Optimizer Asynchronous Code -->
    <script type='text/javascript'>
        var _vwo_code=(function(){
            var account_id=333205,
                settings_tolerance=2000,
                library_tolerance=2500,
                use_existing_jquery=false,
                f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
    </script>
    <!-- End Visual Website Optimizer Asynchronous Code -->

    <!-- Google Tag Manager for WordPress by gtm4wp.com -->
    <script data-cfasync="false" type="text/javascript">//<![CDATA[
        var gtm4wp_datalayer_name = "dataLayer";
        var dataLayer = dataLayer || [];
        //]]>
    </script>
    <!-- End Google Tag Manager for WordPress by gtm4wp.com -->

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-596GZC');</script>
    <!-- End Google Tag Manager -->
</head>
<?php if($_SERVER['SERVER_NAME'] != 'localhost') { ?>
<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
    window.__lc = window.__lc || {};
    window.__lc.license = 5515581;
    (function() {
        var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
        lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
    })();
</script>
<!-- End of LiveChat code -->
<?php } ?>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <div class="row">
            <div class="col-3 d-md-none align-items-center" style="display: flex;">

            </div>
            <div class="col-6 offset-md-3">
                <a href="https://realresponse.com.au" id="home-link">
                    <img src="/booking/img/realresponse.png" id="rr-logo" class="rr-only">
                </a>
            </div>
            <!--<div class="d-none d-lg-flex col-lg-4 align-items-center">
                <p id="questions-text">Got questions? <strong>1300 744 980</strong></p>
            </div>-->
            <div class="col-3 col-md-2 text-right align-items-center" style="display: flex;">
                <a id="help-button"><span class="circle">?</span>Help</a>
            </div>
        </div>
    </div>
</nav>

<div class="container-fluid" id="progressBar">
    <div class="progress-bar active" id="progress-1"><div class="color"></div></div>
</div>

<div class="container">
    <div class="row h100">
        <div class="col-12 col-md-8" id="current-page">
            <div id="page-finish" class="booking-page-container">
                <div id="page-finish-1" class="booking-page">
                    <h1><?php the_field('finish_page_title', $page->ID); ?><br><span class="red"><?php the_field('finish_page_subtitle'); ?></span></h1>
                    <div class="words">
                        <p>An email of the receipt and course details have been sent to <a href="mailto:<?php echo $_POST['email']; ?>" id="myEmail"><?php echo $_POST['email']; ?></a></p>
                    </div>
                    
                    <div class="row">
                        <div class="col">
                            <a id="elearning" href="https://realresponse.app.axcelerate.com.au/learnerPortal/index.cfm" class="btn btn-primary">Start your e-learning</a></p>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col">
                            <a href="https://realresponse.com.au" class="btn btn-default" style="width: 100%">Return to Homepage</a>
                        </div>
                        <div class="col">
                            <a href="/booking" class="btn btn-default" style="width: 100%">Book another course</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4" id="summary-panel">
            <p class="header">Booking Details</p>
            <div id="booking-summary">
                <?php echo $_POST['message']; ?>
            </div>
            <p class="header" id="summary-total">Total $<span class="total-amount"></span></p>
        </div>
    </div>
</div>

<div id="help-panel">
    <div class="container">
        <div class="row" id="help-header">
            <div class="col-12 col-lg-4 offset-lg-8 text-right align-items-center">
                <div class="content">
                    <a id="help-close"><span class="circle">X</span>Close</a>
                </div>
            </div>
        </div>
        <div class="row" id="help-content">
            <div class="col-12 col-lg-4 offset-lg-8">
                <div class="content">
                    <p class="header">Frequently Asked Questions</p>
                    <div id="help-scroll">
                        <?php
                        $faqs = get_posts([
                            'post_type' => 'faq',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'faq_category',
                                    'field' => 'slug',
                                    'terms' => 'booking-faq',
                                    'operator' => 'IN',
                                )
                            )
                        ]);
                        $i = 0;
                        echo '<div class="accordion" id="faqAccordion">';
                        foreach($faqs as $faq) { $i++;
                        ?>
                            
                                <div class="card">
                                    <div class="card-header" id="heading<?php echo $i; ?>">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?php echo $i; ?>" aria-expanded="false" aria-controls="collapse<?php echo $i; ?>">
                                                <i class="fa fa-caret-down"></i>
                                                <?php echo $faq->post_title; ?>
                                            </button>
                                        </h2>
                                    </div>

                                    <div id="collapse<?php echo $i; ?>" class="collapse" aria-labelledby="heading" data-parent="#faqAccordion">
                                        <div class="card-body">
                                            <?php echo wpautop($faq->post_content); ?>
                                        </div>
                                    </div>
                                </div>
                            
                        <?php
                        }
                        echo '</div>';
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include_once('../wp-includes/wp-db.php');

global $wpdb;

//get events
$sql = "SELECT * FROM {$wpdb->prefix}bookingrules WHERE rule_archived = 0";
//echo $sql;
$rules = $wpdb->get_results($sql, OBJECT);
?>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script>
    var _rules = <?php echo json_encode($rules); ?>;
    var _privkey = '<?php echo $stripe['publishable_key']; ?>';
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script type='text/javascript' src='<?php echo site_url(); ?>/wp-content/plugins/gravityforms/js/jquery.json.min.js?ver=2.3.6'></script>
<script type='text/javascript' src='<?php echo site_url(); ?>/wp-content/plugins/gravityforms/js/gravityforms.min.js?ver=2.3.6'></script>
<script type='text/javascript' src='<?php echo site_url(); ?>/wp-content/plugins/gravityforms/js/placeholders.jquery.min.js?ver=2.3.6'></script>
<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript" src="js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
</body>
</html>