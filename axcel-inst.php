<?php
    include 'config.php';
    
    $service_url = $ax_url . 'course/instance/detail';

    $post = array(
        'type' => 'w',
        'instanceID' => $_GET['iid']
    );

    $postfields = http_build_query($post);

    $headers = array(
        'WSToken: ' . $ws_token,
        'APIToken: ' . $api_token,
        'Expect: ',
        //'Content-Length: ' . strlen($postfields)
    );

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $service_url.'?'.$postfields);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    //curl_setopt($curl, CURLOPT_POST, 0);
    //curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    if ($proxy) {
        curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    }

    $curl_response = curl_exec($curl);
    curl_close($curl);
    $ret = json_decode($curl_response);

    header('HTTP/1.1 200 OK');
    //var_dump($ret);

    $cid = $ret->ID;

    $templateQuery = "SELECT pm.post_id, pm.meta_value AS course_id, pm2.meta_value AS template_id 
    FROM wp_postmeta pm
    JOIN wp_postmeta pm2 ON pm2.post_id = pm.post_id AND pm2.meta_key = REPLACE(pm.meta_key, 'course_id', 'template_id')
    WHERE pm.meta_key LIKE 'axcelerate_options_%_course_id' AND pm.meta_value = ".$cid;

    global $wpdb;
    $templateResults = $wpdb->get_results($templateQuery);

    if(count($templateResults) > 0) {
        $res = $templateResults[0];

        $course = $res->post_id;
        $public = get_field('available_as_public_course', $course);
        $online = get_field('available_as_online_course', $course);

        $out = new stdClass();
        $out->course = $course;
        $out->public = $public;
        $out->online = $online;

        echo json_encode($out);
    }