<?php
	include 'config.php';
	
    $month = intval($_GET['month'] ?? date('m'));
	$year = intval($_GET['year'] ?? date('Y'));

    $first = $year.'-'.nf($month).'-01';
    $last = $year.'-'.nf($month).'-'.cal_days_in_month(CAL_GREGORIAN, $month, $year);
    
    $monthnum = $month;

    include_once('../wp-config.php');
    include_once('../wp-includes/wp-db.php');
    
    global $wpdb;

    //draw calendar
	//what's the first monday?
	$first .= ' 12:00:00';
	$last .= ' 12:00:00';
	$day = date('N', strtotime($first));
	//echo 'First Day '.$day.'<br>';
	
	//subtract $day - 1 days from the start of the month
	$start = date('Y-m-d', strtotime($first.' -'.($day - 1).' days'));
	//echo 'Start '.$start.'<br>';
	
	$day = date('N', strtotime($last));
	//echo 'Last Day '.$day.'<br>';
	
	//add 7 - $day days to the end of the month
	$end = date('Y-m-d', strtotime($last.' +'.(7 - $day).' days'));
	//echo 'End '.$end.'<br>';

    $return = new stdClass();
	$return->month = date('F Y', strtotime($first));
	$return->nextMonth = $monthnum == 12 ? 1 : $monthnum + 1;
	$return->prevMonth = $monthnum == 1 ? 12 : $monthnum - 1;
	$return->nextYear = $monthnum == 12 ? $year + 1 : $year;
	$return->prevYear = $monthnum == 1 ? $year - 1 : $year;
	$calendar = '';
	
	//Draw a calendar
	$startTime = strtotime( $start.' 12:00' );
	$endTime = strtotime( $end.' 14:00' );

    //get events
    $sql = "SELECT * FROM {$wpdb->prefix}rr_booking_dates WHERE calendar = '" . $_GET['calendar'] . " Availability' and start between '".$start."' and '".$end." 23:59:59' and category = '".$_GET['category']." Unavailable'";
    //echo $sql;
    $results = $wpdb->get_results($sql, ARRAY_A);
    
    $bookedDays = [];
    foreach($results as $result) {
        $bookedDays[substr($result['start'], 0, 10)] = 'booked';
    }
    
    //var_dump($bookedDays);

    date_default_timezone_set('Australia/Sydney');
    //$tz = date_default_timezone_get();
    //echo $tz;
	//ob_start();
	// Loop between timestamps, 24 hours at a time
	for ( $i = $startTime; $i <= $endTime; $i = $i + 86400 ) {
		if(date('N', $i) == 1) {
			echo '<div class="week">';
		}
		$thisDate = date( 'Y-m-d', $i );
		echo '<div data-day="'.$thisDate.'" class="day ';
        $c_date = date('Y-m-d', strtotime('+1 day'));
        
		//echo $i.': '.$thisDate.'; '.$c_date;
		if(isset($bookedDays[$thisDate]) || $thisDate <= $c_date) {
			echo 'booked';
		}
		//echo '<br>';
		echo '">'.date('j', $i).'</div>';
		if(date('N', $i) == 7) {
			echo '</div>';
		}
	}
	$calendar = ob_get_clean();
	
	$return->calendar = $calendar;

	header('HTTP/1.1 200 OK');
	echo json_encode($return);
	
	function nf($int) {
		return str_pad($int, 2, '0', STR_PAD_LEFT);
	}