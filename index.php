<?php
require('config.php');
include_once('../wp-includes/wp-db.php');
global $wpdb;

$posts = get_posts([
    'post_type' => 'page',
    'name' => 'booking'
]);
if($posts) {
    $page = $posts[0];
}

	$cid = 0;
	$lid = 0;
	$tid = -1;
    $iid = 0;
    $stateid = 0;
    $idate = '';
    $iamt = 0;
    $haveOwnEquipment = false;
    $showOnline = true;
    $showOnsite = true;
    $showPublic = true;
    $showPrivate = false;
    $booking_url_id = 0;

    $slackChannel = 'sales';

    if(isset($_REQUEST['id'])) {
        $id = $_REQUEST['id'];
        //could start with "geelong"
        $geelong = false;
        $colac = false;
        if(strpos($id, 'geelong') === 0) {
            //Do "geelong stuff"
            $geelong = true;
            $id = substr($id, 7);
            $lid = ($_SERVER['HTTP_HOST'] == 'realresponse.local' ? 12393 : 23978);
            //$tid = 1;
            $stateid = 'VIC';
        }
    
        if(strpos($id, 'colac') === 0) {
            //Do "colac stuff"
            //$geelong = true;
            $colac = true;
            $id = substr($id, 5);
            $lid = ($_SERVER['HTTP_HOST'] == 'realresponse.local' ? 12394 : 23985);
            //$tid = 1;
            $stateid = 'VIC';
        }

        $allowed_courses = [];

        $id_parts = explode('/', $id);
        $more_id = '';
        if(count($id_parts) > 1) {
            $id = $id_parts[0];
            $more_id = $id_parts[1];
        }

        //echo '<pre>'; var_dump($id); var_dump($more_id); echo '</pre>';

        //Look up ID in the portal database.  Should be done via the API or something, probably.
        $sql = "SELECT DISTINCT u.id, u.link_url, u.equipment, u.password, c.name, c.logo, u.invoices,
         c.xero_guid, c.axid, u.freetext, u.priority_courses, 
         case when u.booking_email = 1 then u.confirmation_template else 0 end as template_id, 
         u.confirmation_email, u.real_response, u.cq_first_aid, u.locations, u.slack_channel
        FROM booking_urls u
        LEFT JOIN clients c ON c.id = u.client_id
        WHERE u.link_url = '$id'";

        //echo '<pre>'.$sql.'</pre>';

        $wpdb->select($portaldb);
        $res = $wpdb->get_results($sql, ARRAY_A);
        //echo '<pre>'; var_dump($res); echo '</pre>';
        if(count($res) == 0) {
            //An incorrect booking URL has been supplied.  We need to die with an error if it's not a magic link
            if(!is_object(json_decode(base64_decode($id)))) {
                include((__DIR__) . DIRECTORY_SEPARATOR . 'pages' . DIRECTORY_SEPARATOR . 'no-link.php');
                die();
            }
        } elseif(strlen($id) > 0) {
            $theres = null;
            foreach($res as $row) {
                $theres = $row;
            }
            //Is it for the site we're on?
            if(strpos($_SERVER['HTTP_HOST'], 'cq') !== false) {
                //cq first aid
                if($theres['cq_first_aid'] == 0) {
                    //Not allowed
                    include((__DIR__) . DIRECTORY_SEPARATOR . 'pages' . DIRECTORY_SEPARATOR . 'no-link.php');
                    die();
                }
            } else {
                if($theres['real_response'] == 0) {
                    //Not allowed
                    include((__DIR__) . DIRECTORY_SEPARATOR . 'pages' . DIRECTORY_SEPARATOR . 'no-link.php');
                    die();
                }
            }
            $id = $more_id;
        }

        $wpdb->select(DB_NAME);

        //echo '<pre>'; var_dump($id); var_dump($more_id); echo '</pre>';

        //echo '<pre>'; var_dump($res); echo '</pre>';
        foreach($res as $row) {
            //echo '<pre>'; var_dump($row); echo '</pre>';
            $out_id = $row['id'];
            $booking_url_id = $out_id;

            $out_link_url = $row['link_url'];
            $out_equipment = $row['equipment'];
            $out_password = $row['password'];
            $out_name = $row['name'];
            $out_logo = $row['logo'];
            $out_invoices = $row['invoices'];
            $out_xero = $row['xero_guid'];
            $out_axid = $row['axid'];
            $out_text = $row['freetext'];
            $out_priority = $row['priority_courses'];
            $template_id = $row['template_id'];
            $conf_email = $row['confirmation_email'];
            $allowed_locations = $row['locations'];
            $slackChannel = $row['slack_channel'];

            $haveOwnEquipment = $out_equipment;
            if(strlen($out_password) > 0) {
                //We need a password
                if(!isset($_REQUEST['password'])) {
                    //there isn't one set
                    $password_correct = true;
                    include((__DIR__).DIRECTORY_SEPARATOR.'pages'.DIRECTORY_SEPARATOR.'password.php');
                    die();
                } else {
                    //There is one set
                    if($_REQUEST['password'] != $out_password) {
                        //It's wrong though.
                        $password_correct = false;
                        include((__DIR__).DIRECTORY_SEPARATOR.'pages'.DIRECTORY_SEPARATOR.'password.php');
                        die();
                    }
                }
            }
        }

        if($out_axid > 0) {
            echo '<input type="hidden" id="portal_axid" value="'.$out_axid.'">';
        }
        echo '<input type="hidden" id="priority_courses" value="'.$out_priority.'">';

        if(count($res) > 0) {
            $showOnline = false;
            $showOnsite = false;
            $showPublic = false;
            $showLocations = false;
        }

        $sql2 = "SELECT course_id, coupon_code, template_id, course_type, price FROM ".$portaldb.".booking_url_courses WHERE booking_url_id = '$out_id'";
        $res = $wpdb->get_results($sql2, ARRAY_A);
        foreach($res as $row) {
            $out_course = $row['course_id'];
            $out_template = $row['template_id'];
            $out_coupon = $row['coupon_code'];
            $out_type = $row['course_type'];
            $out_price = $row['price'];

            $c = [
                'template' => $out_template,
                'coupon' => $out_coupon,
                'id' => $out_course,
                'course_type' => $out_type,
                'price' => $out_price
            ];

            if(intval($out_type) == 1) {
                $showPrivate = true;
            }

            $allowed_courses[$out_course] = $c;
            if((int)get_field('available_as_public_course', $out_course) == 1) {
                //echo $out_course.' is public<br>';
                $showPublic = true;
            }

            /*if((int)get_field('available_as_onsite_course', $out_course) == 1) {
                //echo $out_course.' is onsite<br>';
                $showOnsite = true;
            }*/

            if((int)get_field('available_as_online_course', $out_course) == 1) {
                //echo $out_course.' is online<br>';
                $showOnline = true;
            }

            if((int)get_field('needs_equipment', $out_course) == 1 && !$haveOwnEquipment) {
                //echo $out_course.' is online<br>';
                $showLocations = true;
            }
        }

        /*echo '<pre>';
        var_dump($out_course);
        var_dump($allowed_courses);
        var_dump($showPublic);
        var_dump($showOnsite);
        var_dump($showOnline);
        echo '</pre>';
        die();*/
        
        //could also be a magic code
        //does ID exist as one of these codes?  it's probably a meta thing is all, so that's harder to find
        
        //pull it apart
        //echo '<pre>'; var_dump($id); echo '</pre>';
        $json = base64_decode($id);
        //echo '<pre>'; var_dump($json); echo '</pre>';
        $decoded = json_decode($json);
        //echo '<pre>'; var_dump($decoded); echo '</pre>';
        $requestdata = (array)$decoded;

        if(isset($requestdata['cid'])) {
			$cid = $requestdata['cid'];
            if(!is_numeric($cid)) {
                $posts = get_posts(array(
                    'numberposts'	=> -1,
                    'post_type'		=> 'rfa_courses',
                    'meta_key'		=> 'axcelerate_id',
                    'meta_value'	=> $cid
                ));
                if(count($posts) > 0) {
                    $cid = $posts[0]->ID;
                }
            } else {
                $cid = intval($cid);
            }
		}

		if(isset($requestdata['sid'])) {
		    $sid = $requestdata['sid'];
            $stateTerm = get_term_by('term_id',  $sid, 'rfa_states');
            //var_dump($stateTerm);
            if($stateTerm) {
                $stateid = strtoupper($stateTerm->slug);
            }
        }

		if(strlen($stateid) == 0 || $stateid === 0) {
            $stateid = null;
        }

		if(isset($requestdata['lid'])) {
		    $lid = $requestdata['lid'];
		    if(!is_numeric($lid)) {
		        $posts = get_posts(array(
                    'numberposts'	=> -1,
                    'post_type'		=> 'rfa_states',
                    'post_title'	=> $lid
                ));
                if(count($posts) > 0) {
                    foreach($posts as $post) {
                        if(strpos($id, 'geelong') === 0 && $post->post_title == 'Geelong') {
                            $lid = $post->ID;
                            $stateid = 'VIC';
                        } elseif(strpos($id, 'colac') === 0 && $post->post_title == 'Colac') {
                            $lid = $post->ID;
                            $stateid = 'VIC';
                        } else {
                            $lid = $post->ID;
                            if($stateid === null) {
                                $stateid = get_field('axcelerate_id', $lid);
                            }
                        }
                    }
                }
            } else {
                $lid = intval($lid);
                if($stateid === null) {
                    $stateid = get_field('axcelerate_id', $lid);
                }
            }
		}
        if(isset($requestdata['tid'])) {
            $tid = intval($requestdata['tid']);
        }
        if(isset($requestdata['iid'])) {
            $iid = $requestdata['iid'];
            $idate = $requestdata['date'];
            $iamt = $requestdata['amt'];
        }
    }

    if(strlen($stateid) == 0) {
        $stateid = null;
    }
    if($sid != 0 && $stateid == null) {
        $stateid = $sid;
    }

    if($stateid === 0) {
        $stateid = null;
    }

    if($lid == 0) {
        $lid = null;
    }

    if($tid == -1) {
        $tid = null;
    }

    if($cid == 0) {
        $cid = null;
    }

    if($cid != null && $tid == null) {
        //Can we work out a type based off this?
        $online = get_field('available_as_online_course', $cid);
        $onsite = get_field('available_as_onsite_course', $cid);
        $public = get_field('available_as_public_course', $cid);

        if($online + $onsite + $public == 1) {
            if($online == 1) {
                $tid = 2;
            }

            if($public == 1) {
                $tid = 1;
            }

            if($onsite == 1) {
                $tid = 0;
            }
        }
    }

    if($_SERVER['HTTP_HOST'] == 'realresponse.local') {
        echo '<pre>';
        var_dump($requestdata);

        echo 'state: ';
        var_dump($stateid);
        echo '<br>';

        echo 'location: ';
        var_dump($lid);
        echo '<br>';

        echo 'type: ';
        var_dump($tid);
        echo '<br>';

        echo 'course: ';
        var_dump($cid);
        echo '<br>';

        echo 'instance: ';
        var_dump($iid);
        echo '</pre>';
    }
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="google-site-verification" content="gUXCJmVa2EAIIX4PvkEjAt1kSSHQ2Q2xT0zditzkXjk" />
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php if(strpos($_SERVER['HTTP_HOST'], 'cq') !== false) { ?>
        <title>Booking - CQ First Aid</title>
    <?php } else { ?>
        <title>Booking - Real Response</title>
    <?php } ?>

    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo site_url(); ?>/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo site_url(); ?>/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo site_url(); ?>/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo site_url(); ?>/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo site_url(); ?>/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    
    <!-- Gravity Forms -->
    <link rel='stylesheet' id='gforms_reset_css-css'  href='<?php echo site_url(); ?>/wp-content/plugins/gravityforms/css/formreset.min.css?ver=2.3.6' type='text/css' media='all' />
    <link rel='stylesheet' id='gforms_formsmain_css-css'  href='<?php echo site_url(); ?>/wp-content/plugins/gravityforms/css/formsmain.min.css?ver=2.3.6' type='text/css' media='all' />
    <link rel='stylesheet' id='gforms_ready_class_css-css'  href='<?php echo site_url(); ?>/wp-content/plugins/gravityforms/css/readyclass.min.css?ver=2.3.6' type='text/css' media='all' />
    <link rel='stylesheet' id='gforms_browsers_css-css'  href='<?php echo site_url(); ?>/wp-content/plugins/gravityforms/css/browsers.min.css?ver=2.3.6' type='text/css' media='all' />
    
    <!-- Bootstrap CSS -->
    <style> .container { max-width: 100%; } </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $booking_base; ?>/css/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?php echo $booking_base; ?>/css/jquery-ui.min.css">
    <link rel="stylesheet" href="<?php echo $booking_base; ?>/css/font-awesome.min.css">
    <?php if(strpos($_SERVER['HTTP_HOST'], 'cq') !== false) { ?>
        <link rel="stylesheet" href="<?php echo $booking_base; ?>/css/stylecq.css?ver=<?php echo time(); ?>">
    <?php } else { ?>
        <link rel="stylesheet" href="<?php echo $booking_base; ?>/css/style.css?ver=<?php echo time(); ?>">
    <?php } ?>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <!-- Start Visual Website Optimizer Asynchronous Code -->
    <script type='text/javascript'>
        var _vwo_code=(function(){
            var account_id=333205,
                settings_tolerance=2000,
                library_tolerance=2500,
                use_existing_jquery=false,
                f=false,d=document;return{use_existing_jquery:function(){return use_existing_jquery;},library_tolerance:function(){return library_tolerance;},finish:function(){if(!f){f=true;var a=d.getElementById('_vis_opt_path_hides');if(a)a.parentNode.removeChild(a);}},finished:function(){return f;},load:function(a){var b=d.createElement('script');b.src=a;b.type='text/javascript';b.innerText;b.onerror=function(){_vwo_code.finish();};d.getElementsByTagName('head')[0].appendChild(b);},init:function(){settings_timer=setTimeout('_vwo_code.finish()',settings_tolerance);this.load('//dev.visualwebsiteoptimizer.com/j.php?a='+account_id+'&u='+encodeURIComponent(d.URL)+'&r='+Math.random());var a=d.createElement('style'),b='body{opacity:0 !important;filter:alpha(opacity=0) !important;background:none !important;}',h=d.getElementsByTagName('head')[0];a.setAttribute('id','_vis_opt_path_hides');a.setAttribute('type','text/css');if(a.styleSheet)a.styleSheet.cssText=b;else a.appendChild(d.createTextNode(b));h.appendChild(a);return settings_timer;}};}());_vwo_settings_timer=_vwo_code.init();
    </script>
    <!-- End Visual Website Optimizer Asynchronous Code -->

    <!-- Google Tag Manager for WordPress by gtm4wp.com -->
    <script data-cfasync="false" type="text/javascript">//<![CDATA[
        var gtm4wp_datalayer_name = "dataLayer";
        var dataLayer = dataLayer || [];
        //]]>
    </script>
    <!-- End Google Tag Manager for WordPress by gtm4wp.com -->

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-596GZC');</script>
    <!-- End Google Tag Manager -->
</head>
<body>
<input type="hidden" id="geelong" value="<?php echo (int)$geelong; ?>">
<input type="hidden" id="colac" value="<?php echo (int)$colac; ?>">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <div class="row">
            <div class="col-3 d-md-none align-items-center" style="display: flex;">

            </div>
            <div class="col-6 offset-md-3">
                <?php
                if(strpos($_SERVER['HTTP_HOST'], 'cq') !== false) {
                    echo '<a href="https://cqfirstaid.com.au" id="home-link">';
                    echo '<img src="/img/cqfa-logo.png" id="cq-logo" class="rr-only">';
                    echo '</a>';
                } else {
                    echo '<a href="https://realresponse.com.au" id="home-link">';
                    if ($geelong || $colac) {
                        echo '<img src="img/geelongfirstaid.png" id="gfa-logo"><span id="partof">is now part of</span><img src="/booking/img/realresponse.png" id="rr-logo">';
                    } else {
                        echo '<img src="/booking/img/realresponse.png" id="rr-logo" class="rr-only">';
                    }
                    echo '</a>';
                }
                ?>
            </div>
            <!--<div class="d-none d-lg-flex col-lg-4 align-items-center">
                <p id="questions-text">Got questions? <strong>1300 744 980</strong></p>
            </div>-->
            <div class="col-3 col-md-2 text-right align-items-center" style="display: flex;">
                <a id="help-button"><span class="circle">?</span>Help</a>
            </div>
        </div>
    </div>
</nav>

<div class="container-fluid" id="progressBar">
    <div class="progress-bar"><div class="color" id="progress-color"></div></div>
</div>

<div class="container">
    <div class="row h100 justify-content-center">
        <div class="col-12 col-md-8" id="current-page">
            <input type="hidden" id="contactEmail">
            <input type="hidden" id="courseAmt" value="<?php echo $iamt; ?>">
            <input type="hidden" id="courseAmtEach" value="<?php echo $iamt; ?>">
            <input type="hidden" id="courseInstance" value="<?php echo $iid; ?>">
            <input type="hidden" id="courseDate" value="<?php echo ($idate != '' ? date('j F Y', strtotime($idate)) : ''); ?>">
            <input type="hidden" id="selected-date">
            <input type="hidden" id="homeURL" value="<?php echo get_site_url(); ?>/booking">
            <input type="hidden" id="thisURL" value="<?php echo get_site_url().$_SERVER['REQUEST_URI']; ?>">
            <input type="hidden" id="refererURL" value="<?php echo $_SERVER['HTTP_REFERER']; ?>">
            <input type="hidden" id="slackChannel" value="<?php echo $slackChannel; ?>">
            <?php
            if($haveOwnEquipment) {
                echo '<input type="hidden" id="ownEquipment" value="1">';
            }
            ?>

            <input type="hidden" id="booking-clientid">
            <input type="hidden" id="booking-gclid">
            <input type="hidden" id="booking-trafficsource">

            <input type="hidden" id="iid" value="<?php echo $iid; ?>">
            <input type="hidden" id="allow_invoice" value="<?php echo $out_invoices; ?>">
            <input type="hidden" id="template_id" value="<?php echo $template_id; ?>">
            <input type="hidden" id="conf_email" value="<?php echo $conf_email; ?>">


            <input type="hidden" id="clicks">
            <?php
                //take into account the ones we're allowed to, based off booking url
                $location_args = array(
                    'post_type' => 'rfa_locations',
                    'hide_empty' => false,
                    'numberposts' => -1
                );

                if(strpos($_SERVER['HTTP_HOST'], 'cq') !== false) {
                    $location_args['include'] = [26737];
                } else {
                    if (strlen($allowed_locations) > 0) {
                        $aloc = explode(',', $allowed_locations);
                        $location_args['include'] = $aloc;
                    }
                }
                $allregions = get_posts( $location_args );

                $regions = [];
                $stateids = [];
                foreach($allregions as $r) {
                    $catID = get_the_terms($r->ID, 'rfa_states');
                    if($catID) {
                        $r->slug = strtoupper($catID[0]->slug);
                        $regionState = $catID[0]->term_id;
                    }
                    if(!in_array($regionState, $stateids)) {
                        $stateids[] = $regionState;
                    }

                    $cq = get_field('cq_first_aid', $r->ID);
                    if(strpos($_SERVER['HTTP_HOST'], 'cq') !== false) {
                        if ($cq === true) {
                            $regions[] = $r;
                        }
                    } else {
                        if ($cq === null || $cq === false) {
                            $regions[] = $r;
                        }
                    }
                }

                //echo '<pre>'; var_dump($regions); echo '</pre>';
                //echo '<pre>'; var_dump($stateids); echo '</pre>';

                $stateArgs = array(
                    'taxonomy' => 'rfa_states',
                    'parent' => 0,
                    'hide_empty' => false
                );
                if(strpos($_SERVER['HTTP_HOST'], 'cq') !== false || strlen($out_link_url) > 0) {
                    $stateArgs['include'] = $stateids;
                }
                $states = get_terms($stateArgs);
                //echo '<pre>'; var_dump($states); echo '</pre>';

                include('pages/state.php');
                include('pages/region.php');
                include('pages/type.php');
                include('pages/course.php');

                include('pages/corporate-participants.php');
                include('pages/corporate-calendar.php');
                include('pages/corporate-contact.php');
                include('pages/corporate-address.php');

                include('pages/instance.php');
                include('pages/public-contact.php');
                include('pages/public-participants.php');
                include('pages/public-match.php');

                include('pages/payment.php');
                include('pages/error.php');
            ?>
        </div>
        <div class="d-none d-md-block col-md-4" id="summary-panel">
            <p class="header">Booking Details</p>
            <div id="booking-summary">
                <p id="location-text"></p>
            </div>
            <p class="header" id="summary-total">Total <span class="total-amount"></span></p>
        </div>
    </div>
</div>

<div id="help-panel">
    <div class="container">
        <div class="row" id="help-header">
            <div class="col-12 col-lg-4 offset-lg-8 text-right align-items-center">
                <div class="content">
                    <a id="help-close"><span class="circle">X</span>Close</a>
                </div>
            </div>
        </div>
        <div class="row" id="help-content">
            <div class="col-12 col-lg-4 offset-lg-8">
                <div class="content">
                    <p class="header">Frequently Asked Questions</p>
                    <div id="help-scroll">
                        <?php
                        $faqs = get_posts([
                            'post_type' => 'faq',
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'faq_category',
                                    'field' => 'slug',
                                    'terms' => 'booking-faq',
                                    'operator' => 'IN'
                                )
                            ),
                            'numberposts' => -1
                        ]);
                        $i = 0;
                        echo '<div class="accordion" id="faqAccordion">';
                        foreach($faqs as $faq) { $i++;
                        ?>
                            
                                <div class="card">
                                    <div class="card-header" id="heading<?php echo $i; ?>">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?php echo $i; ?>" aria-expanded="false" aria-controls="collapse<?php echo $i; ?>">
                                                <i class="fa fa-caret-down"></i>
                                                <?php echo $faq->post_title; ?>
                                            </button>
                                        </h2>
                                    </div>

                                    <div id="collapse<?php echo $i; ?>" class="collapse" aria-labelledby="heading" data-parent="#faqAccordion">
                                        <div class="card-body">
                                            <?php echo wpautop($faq->post_content); ?>
                                        </div>
                                    </div>
                                </div>
                            
                        <?php
                        }
                        echo '</div>';
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
//get events
$sql = "SELECT * FROM {$wpdb->prefix}bookingrules WHERE rule_archived = 0";
//echo $sql;
$rules = $wpdb->get_results($sql, OBJECT);
?>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script>
    var _rules = <?php echo json_encode($rules); ?>;
    var _baseurl = '<?php echo $booking_base; ?>';
    var _privkey = '<?php echo $stripe['publishable_key']; ?>';
    var _formType = 'Website';
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
<script type='text/javascript' src='<?php echo site_url(); ?>/wp-content/plugins/gravityforms/js/jquery.json.min.js?ver=2.3.6'></script>
<script type='text/javascript' src='<?php echo site_url(); ?>/wp-content/plugins/gravityforms/js/gravityforms.min.js?ver=2.3.6'></script>
<script type='text/javascript' src='<?php echo site_url(); ?>/wp-content/plugins/gravityforms/js/placeholders.jquery.min.js?ver=2.3.6'></script>
<script src="https://js.stripe.com/v3/"></script>
<script type="text/javascript" src="<?php echo $booking_base; ?>/js/perfect-scrollbar.min.js"></script>
<script type="text/javascript" src="<?php echo $booking_base; ?>/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo $booking_base; ?>/js/booking.js?ver=<?php echo time(); ?>"></script>
</body>
</html>