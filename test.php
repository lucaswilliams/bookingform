<?php
    $description = 'Course: Apply First Aid
    Contact: Motti Blum
    Organisation: Real Response
    Address: 123 St Kilda Road, St Kilda VIC 3012
    Paid via Stripe';
    
    $url = 'https://hooks.zapier.com/hooks/catch/1253732/xw4wq8/';
	$fields = http_build_query(array(
        'start' => '2019-02-02 09:00:00',
        'end' => '2019-02-02 17:00:00',
        'title' => 'Real Response',
        'description' => $description
    ));
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	$server_output = curl_exec($ch);

	curl_close ($ch);
	
	echo '<pre>'; var_dump($server_output); echo '</pre>';