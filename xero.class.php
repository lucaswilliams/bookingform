<?php
require_once((__DIR__).'/vendor/autoload.php');
use XeroPHP\Models\Accounting\Contact;
use XeroPHP\Models\Accounting\Invoice;

class XeroFunctions
{
    private $siteurl, $provider, $expires, $config, $xeroTenantId, $accountingApi, $first_name, $last_name, $course_name, $date, $xerooptions, $portaldb, $maindb;

    function __construct($client, $secret, $first_name, $last_name, $course_name, $date)
    {
        $this->getOptions();

        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->course_name = $course_name;
        $this->date = $date;
        //$this->siteurl = site_url() . '/wp-admin/admin.php?page=xero-settings';
        $this->provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId' => $client,
            'clientSecret' => $secret,
            'redirectUri' => $this->xerooptions['redirecturl'],
            'urlAuthorize' => 'https://login.xero.com/identity/connect/authorize',
            'urlAccessToken' => 'https://identity.xero.com/connect/token',
            'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation'
        ]);

        $options = ['scope' => ['openid email profile offline_access accounting.settings accounting.transactions accounting.contacts accounting.journals.read accounting.reports.read accounting.attachments']];

        try {
            $this->expires = $this->xerooptions['expires'];
            if (time() > $this->expires) {
                //Refresh the token
                $newAccessToken = $this->provider->getAccessToken('refresh_token', [
                    'refresh_token' => $this->xerooptions['refresh_token']
                ]);

                $this->xerooptions['access_token'] = $newAccessToken->getToken();
                $this->xerooptions['expires'] = $newAccessToken->getExpires();
                $this->xerooptions['refresh_token'] =  $newAccessToken->getRefreshToken();
                //$this->xerooptions['tenant_id'] = $newAccessToken->getValues()["id_token"];
                $this->saveOptions();
            }

            $this->config = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken($this->xerooptions['access_token']);
            $this->xeroTenantId = $this->xerooptions['tenant_id'];

            //var_dump($this->xeroTenantId);

            $this->accountingApi = new XeroAPI\XeroPHP\Api\AccountingApi(
                new GuzzleHttp\Client(),
                $this->config
            );
        } catch (Exception $ex) {
            echo '<pre>'; var_dump($ex->getMessage()); echo '</pre>';
            echo '<pre>'; var_dump($this->xerooptions); echo '</pre>';
            throw new Exception('Could not initialise Xero.  Please check your credentials.'.$ex->getMessage()."\n".print_r($this->xerooptions, true)."\n");
        }
    }

    private function getOptions() {
        global $wpdb;

        $baseurl = 'https://portal.realresponse.com.au';
        $this->portaldb = 'portal';
        $this->maindb = 'realresponse';
        if($_SERVER['HTTP_HOST'] == 'realresponse.local') {
            $baseurl = 'https://realportal.local';
            $this->portaldb = 'realportal';
        }
        if($_SERVER['HTTP_HOST'] == 'dev.realresponse.com.au') {
            $baseurl = 'https://devportal.realresponse.com.au';
            $this->portaldb = 'devportal';
            $this->maindb = 'dev';
        }

        $wpdb->select($this->portaldb);
        $res = $wpdb->get_results('SELECT * FROM xero_config', ARRAY_A);
        //var_dump($res);
        $wpdb->select($this->maindb);
        if(count($res) > 0) {
            $this->xerooptions = $res[0];
        }
        $this->xerooptions['redirecturl'] = $baseurl.'/xero/auth/callback';
    }

    private function saveOptions() {
        global $wpdb;
        $sql = 'UPDATE '.$this->portaldb.'.xero_config SET access_token = \''.$this->xerooptions['access_token'].'\', refresh_token = \''.$this->xerooptions['refresh_token'].'\', expires = '.$this->xerooptions['expires'];
        $wpdb->query($sql);
    }

    function emailInvoice($invoice_id) {
        $this->accountingApi->emailInvoice($this->xeroTenantId, $invoice_id, new \XeroAPI\XeroPHP\Models\Accounting\RequestEmpty());
    }

    function getOrCreateXeroContact($inv_guid, $inv_org, $inv_first, $inv_last, $inv_email, $wpid, $sessionid)
    {
        $inv_org = trim($inv_org);
        $inv_first = trim($inv_first);
        $inv_last = trim($inv_last);
        $inv_email = trim($inv_email);

        global $wpdb;
        $contactParams = [];
        try {
            if (strlen($inv_guid) > 0) {
                //We have got a GUID for this person, so now we need to just look them up.  I make that sound easy...
                $this->apiResponse = $this->accountingApi->getContact($this->xeroTenantId, $inv_guid);
                $xcustomer = $this->apiResponse->getContacts()[0];
            } else {
                $name = str_replace('\\', '', (strlen($inv_org) > 0 ? $inv_org : $inv_first . ' ' . $inv_last . ' (' . $inv_email . ')'));

                $apiResponse = $this->accountingApi->getContacts($this->xeroTenantId, null, 'Name=="' . $name . '"');
                $customers = $apiResponse->getContacts();
                if (count($customers) == 0) {
                    //Make them
                    $contactParams = [
                        'name' => $name,
                        'first_name' => $inv_first,
                        'last_name' => $inv_last,
                        'email_address' => $inv_email,
                        'is_customer' => true
                    ];
                    $contact = new \XeroAPI\XeroPHP\Models\Accounting\Contact($contactParams);
                    $customer = $this->accountingApi->createContacts($this->xeroTenantId, $contact, true);
                    $xcustomer = $customer->getContacts()[0];
                } else {
                    foreach ($customers as $cus) {
                        $xcustomer = $cus;
                    }
                }
            }

            $wpdb->update('wp_rr_bookings', ['xero_contact_id' => $xcustomer->getContactId()], ['id' => $wpid]);
            if($wpdb->last_error !== '') {
                $wpdb->print_error();
            }
            $wpdb->update('wp_rr_booking_log', [
                'xero_contact_in' => print_r($contactParams, true),
                'xero_contact_out' => print_r($xcustomer, true)
            ], ['id' => $sessionid]);

            if(strlen($inv_guid) == 0) {
                //Update our contact in all cases
                $xcustomer->setFirstName($inv_first);
                $xcustomer->setLastName($inv_last);
                $xcustomer->setEmailAddress($inv_email);
                $this->accountingApi->updateContact($this->xeroTenantId, $xcustomer->getContactId(), $xcustomer);
            } else {
                //It's a company, add to list of contacts if they are not?
                //$contacts = $xcustomer->getContactPersons();
                //Iterate and add

                //$this->accountingApi->updateContact($this->xeroTenantId, $xcustomer->getContactId(), [$xcustomer]);
            }

            return $xcustomer;
        } catch (\Exception $ex) {
            $wpdb->update('wp_rr_booking_log', [
                'xero_contact_in' => print_r($contactParams, true),
                'xero_contact_out' => print_r($ex, true)
            ], ['id' => $sessionid]);
            $message = $ex->getMessage();
            $this->log_error($message, $this->first_name, $this->last_name, $this->course_name, $this->date);
            $wpdb->update('wp_rr_bookings', ['xero_contact_error' => $message], ['id' => $wpid]);
            return false;
        }
    }

    function getOrCreateXeroInvoice($xcustomer, $gst_status, $amount, $course, $account_code, $taxcode, $wpid, $divisor, $sessionid, $inv_po = '')
    {
        /*var_dump($xcustomer); echo '<br>';
        echo $amount.'<br>';
        echo $course.'<br>';
        echo $account_code.'<br>';
        echo $taxcode.'<br>';
        echo $wpid.'<br>';
        echo $divisor.'<br>';*/

        global $wpdb;
        //does the supplied wpid have an invoice id?
        $sql = "SELECT xero_invoice_id FROM wp_rr_bookings WHERE id = ".$wpid;
        $recs = $wpdb->get_results($sql, ARRAY_A);
        if(count($recs) > 0) {
            $invoiceId = $recs[0]['xero_invoice_id'];
            if(strlen($invoiceId) > 0) {
                //congrats
            } else {
                $invoiceId = false;
            }
        }

        if(!$invoiceId) {
            $invoicedata = [];
            try {
                /*$accountcodes = $accountingApi->getAccounts($xeroTenantId, null, 'Code="'.$account_code.'"');
                $accountcodelist = $accountcodes->getAccounts();
                $account_code = $accountcodelist[0]->getAccountId();*/

                $invoicedata = [
                    'date' => new DateTime(),
                    'due_date' => new DateTime(),
                    'status' => 'AUTHORISED',
                    'type' => 'ACCREC',
                    'contact' => $xcustomer,
                    'reference' => $inv_po
                ];
                $invoice = new \XeroAPI\XeroPHP\Models\Accounting\Invoice($invoicedata);

                $invoicedata['items'] = [];
                $invAmount = round($amount / $divisor, 2, PHP_ROUND_HALF_EVEN);
                $line_item_desc = str_replace('&#8211;', '-', get_the_title($course)) . ' - ' . $this->date;

                parse_str(urldecode($_POST['participant-detail']), $participant_details);

                $items = [];
                if (isset($participant_details['participant-given']) && is_array($participant_details['participant-given']) && count($participant_details['participant-given']) > 0) {
                    foreach ($participant_details['participant-given'] as $key => $part) {
                        $pgiven = $participant_details['participant-given'][$key];
                        $psurname = $participant_details['participant-surname'][$key];

                        $itemData = [
                            'description' => $line_item_desc . ' - ' . $pgiven . ' ' . $psurname,
                            'quantity' => 1,
                            'unit_amount' => $invAmount / count($participant_details['participant-given']),
                            'line_amount' => $invAmount / count($participant_details['participant-given']),
                            'account_code' => $account_code,
                            'tax_type' => $taxcode
                        ];
                        $item = new \XeroAPI\XeroPHP\Models\Accounting\LineItem($itemData);

                        if (intval($_POST['geelong']) == 1) {
                            $tracking = new \XeroAPI\XeroPHP\Models\Accounting\LineItemTracking();
                            $categories = $this->accountingApi->getTrackingCategories($this->xeroTenantId, 'Name=="Location"');
                            $category = $categories->getTrackingCategories();
                            foreach ($category as $cat) {
                                $tracking->setTrackingCategoryId($cat->getTrackingCategoryId());
                                foreach ($cat->getOptions() as $option) {
                                    if ($option->getName() == 'Geelong') {
                                        $tracking->setTrackingOptionId($option->getTrackingOptionId());
                                    }
                                }
                            }
                            $item->setTracking([$tracking]);
                        }

                        $items[] = $item;
                        $invoiceData['items'][] = $itemData;
                    }
                } else {
                    $itemData = [
                        'description' => $line_item_desc,
                        'quantity' => 1,
                        'unit_amount' => $invAmount,
                        'line_amount' => $invAmount,
                        'account_code' => $account_code,
                        'tax_type' => $taxcode
                    ];
                    $item = new \XeroAPI\XeroPHP\Models\Accounting\LineItem($itemData);

                    $items[] = $item;
                    $invoiceData['items'][] = $itemData;
                }

                $invoice->setLineItems($items);
                $invoice_create = $this->accountingApi->updateOrCreateInvoices($this->xeroTenantId, $invoice, true);
                $invoice = $invoice_create->getInvoices()[0];
                $invoice_number = $invoice->getInvoiceId();

                if ($invoice_number == '00000000-0000-0000-0000-000000000000') {
                    throw new Exception('An invoice was not created. Check for validation exceptions. ' . print_r($invoice, true));
                }

                $wpdb->update('wp_rr_bookings', ['xero_invoice_id' => $invoice_number], ['id' => $wpid]);
                $wpdb->update('wp_rr_booking_log', [
                    'xero_invoice_in' => print_r($invoiceData, true),
                    'xero_invoice_out' => print_r($invoice, true)
                ], ['id' => $sessionid]);
            } catch (\Exception $ex) {
                $wpdb->update('wp_rr_booking_log', [
                    'xero_invoice_in' => print_r($invoiceData, true),
                    'xero_invoice_out' => print_r($ex, true)
                ], ['id' => $sessionid]);
                $message = $ex->getMessage();
                $this->log_error($message, $this->first_name, $this->last_name, $this->course_name, $this->date);
                $wpdb->update('wp_rr_bookings', ['xero_invoice_error' => $message], ['id' => $wpid]);
                $invoice = false;
            }
        } else {
            //get the invoice by id
            $invoice = $this->accountingApi->getInvoice($this->xeroTenantId, $invoiceId)->getInvoices()[0];
        }

        //var_dump($invoice);

        return $invoice;
    }

    function getXeroInvoicePDF($invoice_id) {
        $response = $this->accountingApi->getInvoiceAsPdf($this->xeroTenantId, $invoice_id);
        return $response;
    }

    function addPaymentToInvoice($invoice, $invAmount, $chargeid, $wpid, $bank_code, $sessionid)
    {
        /*var_dump($invoice); echo '<br>';
        echo $invAmount.'<br>';
        echo $chargeid.'<br>';
        echo $wpid.'<br>';
        echo $bank_code.'<br>';*/

        global $wpdb;
        $paymentdata = [];
        $bank_codes = $this->accountingApi->getAccounts($this->xeroTenantId, null, 'Code="' . $bank_code . '"');
        $accountcodelist = $bank_codes->getAccounts();
        $bank_code_obj = $accountcodelist[0];

        try {
            $paymentdata = [
                'invoice' => $invoice,
                'account' => $bank_code_obj,
                'amount' => $invAmount,
                'reference' => $chargeid,
                'is_reconciled' => false,
            ];

            $payment = new \XeroAPI\XeroPHP\Models\Accounting\Payment($paymentdata);

            $newpayment = $this->accountingApi->createPayment($this->xeroTenantId, $payment);
            $paymentID = $newpayment->getPayments()[0];
            //echo 'updating '.$wpid.' to payment '.$newpayment->getPayments()[0]->getPaymentId().'<br>';
            $wpdb->update('wp_rr_bookings', ['xero_payment_id' => $paymentID->getPaymentId()], ['id' => $wpid]);
            $wpdb->update('wp_rr_booking_log', [
                'xero_payment_in' => print_r($paymentdata, true),
                'xero_payment_out' => print_r($paymentID, true)
            ], ['id' => $sessionid]);

            return true;
        } catch (\Exception $ex) {
            $wpdb->update('wp_rr_booking_log', [
                'xero_payment_in' => print_r($paymentdata, true),
                'xero_payment_out' => print_r($ex, true)
            ], ['id' => $sessionid]);
            $message = $ex->getMessage();
            $this->log_error($message, $this->first_name, $this->last_name, $this->course_name, $this->date);
            $wpdb->update('wp_rr_bookings', ['xero_payment_error' => $message], ['id' => $wpid]);
            return false;
        }
    }

    function log_error($text, $first_name, $last_name, $course_name, $course_date) {
        //Log to LW #logging-rr channel
        $slack = new Slack('https://hooks.slack.com/services/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');
        $slack->setDefaultUsername($_SERVER['HTTP_HOST']);
        $slack->setDefaultChannel("#logging-rr");
        $message = new SlackMessage($slack);
        $message->setText($first_name.' '.$last_name.' just tried booking in '.$course_name.' on '.$course_date.', but got the error'."\n".$text);
        $message->send();

        //Log to RR #sales channel.  Not if localdev.
        if(strpos($_SERVER['HTTP_HOST'], '.local') === FALSE) {
            $rrslack = new Slack('https://hooks.slack.com/services/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');
            if(strpos($_SERVER['SERVER_NAME'], 'dev.') > 0) {
                $rrslack->setDefaultUsername("Booking Form Robot - Test Mode");
            } else {
                $rrslack->setDefaultUsername("Booking Form Robot");
            }
            $rrslack->setDefaultChannel("#booking-errors-from-website");
            $rrslack->setDefaultIcon("https://www.realresponse.com.au/booking/img/realresponse.png");
            $message = new SlackMessage($rrslack);
            $message->setText($first_name.' '.$last_name.' just tried booking in '.$course_name.' on '.$course_date.', but got the error'."\n".$text);
            $message->send();
        }
    }
}