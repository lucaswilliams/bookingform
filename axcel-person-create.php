<?php
	require 'config.php';

	$given = $_POST['given'];
	$surname = $_POST['surname'];
	$email = $_POST['email'];
	$phone = $_POST['phone'];

	$service_url = $ax_url . 'contact';

	$headers = array(
		'wstoken: ' . $ws_token,
		'apitoken: ' . $api_token
	);

	$params = array(
		'givenName' => $given,
		'surname' => $surname,
		'emailAddress' => $email,
		'mobilephone' => $phone
	);

	$fieldsstring = http_build_query($params);

	$curl = curl_init($service_url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $fieldsstring);
	if ($proxy) {
		curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	}

	$curl_response = curl_exec($curl);
	$data = json_decode($curl_response);
	$contact_id = $data->CONTACTID;

	header('HTTP/1.1 200 OK');
	echo $contact_id;