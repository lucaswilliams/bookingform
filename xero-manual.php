<pre>
<?php
include('config.php');
ini_set('max_execution_time', 0);
ini_set('display_errors', 'on');
ini_set('error_reporting', E_ALL);
require_once((__DIR__).'/vendor/autoload.php');
use XeroPHP\Application\PrivateApplication;
if(isset($xeroconfig)) {
    $xero = new PrivateApplication($xeroconfig);
    $xeroguids = $wpdb->get_results("SELECT DISTINCT xero_guid FROM xero_invoices");
    
    foreach($xeroguids as $guidObj) {
        echo '<pre>';
        var_dump($guidObj);
        echo '</pre>';
        
        $guid = $guidObj->xero_guid;
        try {
            $invoices = $xero->loadByGUIDs(\XeroPHP\Models\Accounting\Invoice::class, $guid);
            $invoice = $invoices->first();
            echo '<pre>';
            var_dump($invoice);
            echo '</pre>';
            //If the invoice is now paid (other things could have been updated)
            $axInvoices = $wpdb->get_results("SELECT * FROM xero_invoices WHERE xero_guid = '" . $guid . "'");
    
            if ($invoice->getStatus() == 'PAID' && $axInvoices != null) {
                foreach($axInvoices as $axInvoice) {
                    //make a payment in aXcelerate
                    $headers = array(
                        'wstoken: ' . $ws_token,
                        'apitoken: ' . $api_token
                    );
            
                    $invParams = array(
                        'contactID' => $axInvoice->contact_id,
                        'invoiceID' => $axInvoice->axcelerate_id,
                        'amount' => $axInvoice->amount,
                        'reference' => 'Paid in Xero',
                        'paymentMethodID' => 2
                    );
            
                    $fieldsstring = http_build_query($invParams);
                    $service_url = $ax_url . 'accounting/transaction/';
                    $curl = curl_init($service_url);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($curl, CURLOPT_HEADER, 1);
                    curl_setopt($curl, CURLOPT_VERBOSE, 1);
                    curl_setopt($curl, CURLOPT_POST, true);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $fieldsstring);
                    if ($proxy) {
                        curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                    }
                    $curl_response = curl_exec($curl);
            
                    $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
                    $header = substr($curl_response, 0, $header_size);
                    $body = substr($curl_response, $header_size);
            
                    echo '<pre>';
                    var_dump($header);
                    var_dump($body);
                    echo '</pre>';
            
                    $wpdb->delete('xero_invoices', ['axcelerate_id' => $axInvoice->axcelerate_id]);
                }
            }
        } catch (Exception $ex) {
            echo "Invoice ".$guid." resulted in a bad request\n".print_r($ex, true);
        }
        sleep(1);
    }
}
?>
</pre>
