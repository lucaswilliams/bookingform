<?php
ini_set('display_errors', 'off');
require('config.php');

header('HTTP/1.1 200 OK');
$postarray = [
    'success' => true
];
$coupon_code = trim($_POST['coupon']);

//echo '<pre>'; var_dump($_POST); echo '</pre>';

//Check if all of the details are passed in here
//echo $_POST['courseDate'].'<br>';
//echo $_POST['course'].'<br>';
//echo $_POST['location'].'<br>';
//echo $_POST['participants'].'<br>';

if($_POST['courseDate'] == null) {
    $postarray['success'] = false;
    $postarray['message'] = 'A course date has not been supplied.';
    $postarray['details'] = ['A course date has not been supplied'];
}

if($_POST['course'] == null) {
    $postarray['success'] = false;
    $postarray['message'] = 'A course has not been supplied.';
    $postarray['details'] = ['A course has not been supplied'];
}

if($_POST['location'] == null && $_POST['type'] != 2) {
    $postarray['success'] = false;
    $postarray['message'] = 'A location has not been supplied.';
    $postarray['details'] = ['A location has not been supplied'];
}

if($_POST['participants'] == null) {
    $postarray['success'] = false;
    $postarray['message'] = 'The participants have not been supplied.';
    $postarray['details'] = ['The participants have not been supplied'];
}

if($postarray['success'] == false) {
    echo json_encode($postarray); die();
}

if(strlen($coupon_code) > 0) {
    $posts = new WP_Query([
        'title' => $coupon_code,
        'post_type' => 'rfa_coupons'
    ]);
    
    if ( $posts->have_posts() ) {
        while ($posts->have_posts()) {
            $posts->the_post();
            $coupon = get_post();
            if (strcasecmp($coupon->post_title, $coupon_code) == 0) {
                $error = false;
                $message = 'The supplied coupon code is invalid.';
                $start_date = get_field('start_date', $coupon->ID);
                $end_date = get_field('finish_date', $coupon->ID);
                $uses = get_field('uses', $post_id);
                $sql = "SELECT sum(b.places) as used
                    FROM wp_rr_bookings b
                    WHERE b.coupon = '" . $coupon->post_title . "'";
                $uses_data = $wpdb->get_results($sql);
                
                //$course_date = time();
                $details = [];
                $details[] = 'POST date: '.$_POST['courseDate'];
                $course_date = strtotime($_POST['courseDate']);
                
                foreach ($uses_data as $use) {
                    $remaining = $uses - $use->used;
                }
                
                $courses = get_field('course', $coupon->ID);
                $details[] = 'Courses: ' . json_encode($courses);
                $locations = get_field('location', $coupon->ID);
                if(is_array($locations) && count($locations) > 0) {
                    if(is_object($locations[0])) {
                        //pull out the IDs only
                        
                        $newlocations = [];
                        foreach($locations as $location) {
                            $newlocations[] = $location->ID;
                        }
                        
                        $locations = $newlocations;
                    }
                }
                $details[] = 'Locations: ' . json_encode($locations);
                $participants = get_field('usage_type', $coupon->ID) == 2 ? $_POST['participants'] : 1;
                $details[] = 'Participants: ' . json_encode($participants);
                
                if (is_array($courses) && count($courses) > 0 && !in_array($_POST['course'], $courses)) {
                    $error = true;
                    $details[] = 'Bad Course: ' . $_POST['course'] . '; ' . json_encode($courses);
                }
                
                if (is_array($locations) && count($locations) > 0 && !in_array($_POST['location'], $locations)) {
                    $error = true;
                    $details[] = 'Bad Location: ' . $_POST['location'] . '; ' . json_encode($locations);
                }
                
                $date_type = get_field('date_type', $coupon->ID);
                if ($date_type == 0) {
                    if ($start_date != null && strtotime($start_date) > $course_date) {
                        $error = true;
                        $message = 'This coupon can only be used for courses after ' . date('d/m/Y', strtotime($start_date)).'; yours starts on '.date('d/m/Y', $course_date);
                        $details[] = 'Start date: ' . date('Y-m-d', strtotime($start_date)) . '; ' . date('Y-m-d', $course_date);
                    }
            
                    if ($end_date != null && strtotime($end_date) < $course_date) {
                        $error = true;
                        $message = 'This coupon can only be used for courses before ' . date('d/m/Y', strtotime($end_date)).'; yours starts on '.date('d/m/Y', $course_date);
                        $details[] = 'End date: ' . date('Y-m-d', strtotime($end_date)) . '; ' . date('Y-m-d', $course_date);
                    }
                } else {
                    if ($start_date != null && strtotime($start_date) > time()) {
                        $error = true;
                        $message = 'This coupon can only be used for bookings made after ' . date('d/m/Y', strtotime($start_date));
                        $details[] = 'Start date: ' . date('Y-m-d', strtotime($start_date)) . '; ' . date('Y-m-d');
                    }
                    
                    if ($end_date != null && strtotime($end_date) < time()) {
                        $error = true;
                        $message = 'This coupon can only be used for bookings made before ' . date('d/m/Y', strtotime($end_date));
                        $details[] = 'End date: ' . date('Y-m-d', strtotime($end_date)) . '; ' . date('Y-m-d');
                    }
                }
                
                if ($uses > 0 && $participants > $remaining) {
                    $error = true;
                    $message = 'This coupon only has ' . $remaining . ' uses remaining, and you are trying to use ' . $participants;
                }
                
                if (!$error) {
                    $postarray['amount'] = get_field('amount', $coupon->ID);
                    $postarray['type'] = get_field('amount_type', $coupon->ID);
                    $postarray['useType'] = get_field('usage_type', $coupon->ID);
                    $postarray['axType'] = get_field('axcelerate_pricing', $coupon->ID) ?? 0;
                    $postarray['message'] = 'Valid coupon code';
                } else {
                    $postarray['success'] = !$error;
                    $postarray['message'] = $message;
                    $postarray['details'] = $details;
                    $postarray['type'] = -1;
                }
            }
        }
    } else {
        $postarray['success'] = false;
        $postarray['message'] = 'The supplied access code is invalid.';
        $postarray['details'] = ['Coupon Code not found in DB'];
        $postarray['type'] = -1;
    }
} else {
    $postarray['success'] = false;
    $postarray['message'] = 'The supplied access code is invalid.';
    $postarray['details'] = ['Coupon Code not supplied'];
    $postarray['type'] = -1;
}
echo json_encode($postarray);