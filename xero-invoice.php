<?php
include('config.php');
require_once((__DIR__).'/vendor/autoload.php');
use XeroPHP\Application\PrivateApplication;
if(isset($xeroconfig)) {
    $xero = new PrivateApplication($xeroconfig);
    $headers1 = getallheaders();
    $headers = array_change_key_case($headers1, CASE_LOWER);
    $request_body = file_get_contents('php://input');
    $req = json_decode($request_body);
    $sig = base64_encode(hash_hmac('sha256', $request_body, $xeroconfig['webhook'], true));
    
    if (strcmp($sig, $headers["x-xero-signature"]) == 0) {
        $header = "HTTP/1.0 200 OK";
    } else {
        $header = "HTTP/1.0 401 Unauthorized";
    }
    
    $slack = new Slack('https://hooks.slack.com/services/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');
    $slack->setDefaultUsername("Real Response Logger");
    $slack->setDefaultChannel("#logging-rr");
    
    // Create a new message
    $message = new SlackMessage($slack);
    //$message->setText("New public booking from $first_name $last_name in $course on $date. ($instance_url)");
    $message->setText($header."\n".strip_tags(print_r($headers, true)."\n".print_r($req, true))."\n".$sig."\n".$headers["x-xero-signature"]);
    //$message->send();
    
    header($header);
    
    if(is_array($req->events)) {
        //Now we've got that out of the way, we can look some stuff up
        foreach ($req->events as $event) {
            if ($event->eventType == "UPDATE") {
                $invoices = $xero->loadByGUIDs(\XeroPHP\Models\Accounting\Invoice::class, $event->resourceId);
                $invoice = $invoices->first();
            
                $axInvoice = $wpdb->get_row("SELECT * FROM xero_invoices WHERE xero_guid = '" . $event->resourceId . "'");
            
                //If the invoice is now paid (other things could have been updated)
                if ($invoice->getStatus() == 'PAID' && $axInvoice != null) {
                    //make a payment in aXcelerate
                    $headers = array(
                        'wstoken: ' . $ws_token,
                        'apitoken: ' . $api_token
                    );
                
                    $invParams = array(
                        'contactID' => $axInvoice->contact_id,
                        'invoiceID' => $axInvoice->axcelerate_id,
                        'amount' => $axInvoice->amount,
                        'reference' => 'Paid in Xero',
                        'paymentMethodID' => 2
                    );
                
                    $fieldsstring = http_build_query($invParams);
                    $service_url = $ax_url . 'accounting/transaction/';
                    $curl = curl_init($service_url);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($curl, CURLOPT_POST, true);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $fieldsstring);
                    if ($proxy) {
                        curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                    }
                    $curl_response = curl_exec($curl);
                
                    $wpdb->delete('xero_invoices', ['xero_guid' => $event->resourceId]);
                }
            }
        }
    }
}