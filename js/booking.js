var stripe;
var cardNumber;
var cardExpiry;
var cardCVC;
var _equip;
var _couponApplied = false;
var _theParticipants = undefined;
var _theParticipantDetails = undefined;

$(document).on('click', '.calendar-control', function(e) {
    updateCalendar($(this).data('year'), $(this).data('month'), $('.training-location.active').text().trim(), $('.training-course.active').data('category'));
});

$(document).on('click', '.day', function(e) {
	if(!$(this).hasClass('booked')) {
    	$('#selected-date').val($(this).data('day'));
    	$('.day').removeClass('active');
    	$(this).addClass('active');
    }
});

function updateCalendar(_year, _month, _location, _category) {
    _url = _baseurl + 'calendar.php';
    if(_year !== undefined) {
        _url += '&year=' + _year;
    }
    if(_month !== undefined) {
        _url += '&month=' + _month;
    }
    if(_location !== undefined) {
        _url += '&calendar=' + _location;
    }
	if(_category !== undefined) {
		_url += '&category=' + _category;
	}

    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: _url.replace('&', '?'),
        success: function(data) {
            $('#calendar-month').text(data.month);
            $('#calendar-prev').data('year', data.prevYear).data('month', data.prevMonth);
            $('#calendar-next').data('year', data.nextYear).data('month', data.nextMonth);
            $('#calendar-out').html(data.calendar);
        }
    });
}

$(document).on('click', '#help-button', function(e) {
    $('#help-panel').addClass('expanded');
    setTimeout(function() {
       helpscroller.update();
    }, 500);
});

$(document).on('click', '#help-close', function(e) {
    $('#help-panel').removeClass('expanded');
});

$(document).on('click', '#summary-button', function(e) {
    $('#summary-panel').toggleClass('expanded');
});

function flashSummary() {
    $('#summary-button').addClass('color');
    setTimeout(function() {
        $('#summary-button').removeClass('color');
    }, 200);
}

$('#faqAccordion').on('show.bs.collapse', function(e) {
    var $clickedBtn = $(e.target).attr('id');
    $('.fa-caret-up').addClass('fa-caret-down').removeClass('fa-caret-up');
    $.each($('button[data-target="#' + $clickedBtn + '"]').find('i'), function(k,v) {
        $(v).addClass('fa-caret-up').removeClass('fa-caret-down');
    });
});

$('#faqAccordion').on('hide.bs.collapse', function(e) {
	var $clickedBtn = $(e.target).attr('id');
	$('.fa-caret-up').addClass('fa-caret-up').removeClass('fa-caret-down');
	$.each($('button[data-target="#' + $clickedBtn + '"]').find('i'), function(k,v) {
		$(v).addClass('fa-caret-down').removeClass('fa-caret-up');
	});
});

function scrollPage(element) {
	//Hide all pages after the one we're scrolling to?
	$(element).show();
	$(element).nextAll().hide();

	return $.when($(element).slideDown(300, function() {
		var _scroll = $('#current-page').scrollTop() + $(element).position().top;
		$('#current-page').animate({scrollTop: _scroll}, 500);
	}));
}

$(document).ready(function() {
	if(_formType !== undefined && _formType == 'Portal') {
		$('#portal-total').show();
		$('#public-total').hide();
	} else {
		$('#portal-total').hide();
		$('#public-total').show();
		setUpMCB();
	}

	if ($('#courseInstance').val() > 0) {
		console.log('nah, but really');
		//probably a single instance, skip state and location
		//Can we do something in axcelerate to get the instance here instead of later?
		$.ajax({
			'url': _baseurl + 'axcel-inst.php',
			'dataType': 'json',
			'data': {
				'iid' : $('#courseInstance').val()
			},
			'success': function (data) {
				if(data.online) {
					$('#page-type').addClass('prefilled');
					$('.training-type').removeClass('active');
					$('.training-type[data-id=2]').addClass('active');
				} else {
					if(data.public) {
						$('#page-type').addClass('prefilled');
						$('.training-type').removeClass('active');
						$('.training-type[data-id=1]').addClass('active');
					}
				}
				$('#page-courses').addClass('prefilled');
				$('.training-course.active').removeClass('active');
				$('.training-course[data-id="' + data.course + '"]').addClass('active');

				//But does it need equipment?
				_equip = ($('.training-course.active').data('equipment') == 1);
				if(_equip) {
					//remove the prefilled, nevershow and active classes
					$('#page-state').removeClass('prefilled').removeClass('nevershow');
					$('#page-location').removeClass('prefilled').removeClass('nevershow');
					$('#page-public-instance').removeClass('prefilled').removeClass('nevershow');
					$('#page-public-instance-1').addClass('nevershow');
					console.log('showing state or location because equipment');
					scrollPage('.booking-page-container:first-of-type');
				} else {
					setTimeout(function () {
						console.log('clicking active course, no equipment.');
						$('.training-course.active').click();
					}, 1000);
				}
			}
		});
	} else {
		if($('.training-course.active').data('equipment') == 0 && $('.training-type.active').data('id') == 2) {
			//Skip state and location.
			$('#page-state').addClass('prefilled').addClass('nevershow');
			$('#page-location').addClass('prefilled').addClass('nevershow');
			$('.training-course.active').click();
		} else {
			console.log('is state prefilled');
			if ($('#page-state').hasClass('prefilled')) {
				console.log('yeah, it is');
				setTimeout(function () {
					console.log('state is prefilled, clicking it');
					$('.training-state.active').click();
				}, 1000);
			} else {
				updateCalendar();
				scrollPage('.booking-page-container:first-of-type').then(function() {
					if ($('.training-state').length == 1) {
						$('.training-state').click();
					}
				});
			}
		}
	}
});

$(document).on('click', '.training-type', function(e) {
	trackClick($(this));

	$('.training-type').removeClass('active');

	//Hide all pages after this one
	_parent = $(this).parents('.booking-page-container');
	_parent.show();
	_parent.nextAll().hide();
	$(this).addClass('active');

	updateLocationText();

	_location = $('.training-location.active');
	console.log(_location.length,  _location.text().trim(), $(this).data('id'));
	if((_location.length == 0 || _location.text().trim() == 'Other') && $(this).data('id') != 2) {
		scrollPage('#page-error');
		$('#unavailable-location').show();
		$('#unavailable-course').hide();
	} else {
		updateLocations();

		scrollPage('#page-courses').then(function() {
			//console.log('page scrolled');
			if($('#page-courses').hasClass('prefilled')) {
				//Set the active course again in here
				myval = $('.training-type.active').data('id');
				_public_id = $('.training-course.active').data('axid-pu');
				_online_id = $('.training-course.active').data('axid-on');
				//console.log(myval, _public_id, _online_id);
				//console.log(_public_id.length, _online_id.length, parseInt(_public_id), parseInt(_online_id));
				if(_public_id.length > 0 || parseInt(_public_id) > 0 || _online_id.length > 0 || parseInt(_online_id) > 0) {
					//console.log('remove active');
					$('.training-course.active').removeClass('active');
					//console.log('dumb removal')
					if(myval == 2) {
						$('.training-course[data-id=' + _online_id + ']').addClass('active');
						//console.log(_online_id);
					} else {
						$('.training-course[data-id=' + _public_id + ']').addClass('active');
						//console.log(_public_id);
					}
				}
				$('.training-course.active').click();
			} else {
				//Is there only one?
				if($('.training-course:visible').length == 1) {
					//make it active and click it.
					$('.training-course:visible').addClass('active').click();
				} else {
					$('.training-course').removeClass('active');
				}
			}
		});

		flashSummary();
	}
});

$(document).on('blur', '#total-amount', function() {
	if($(this).val() == 0) {
		//Hide some stuff, show only confirm.  Like a course having $0
		zeroDollarFixes();
	}
});

function updateLocations() {
	_trainType = $('.training-type.active').data('id');
	//console.log(_trainType);

	$('#online-tc').hide();
	$('#online-multiples').hide();

	//hide options that are dumb
	$.each($('.MultiCheckBoxDetailBody .mulinput'), function(k,v) {
		$(v).prop('checked', false);
		if($('.training-type.active').data('id') == 1) {
			//Public
			if ($(v).data('state') == $('.training-state.active').data('id')) {
				$(v).parents('.cont').show();
				$(v).prop('checked', true);
			} else {
				$(v).parents('.cont').hide();
				$(v).prop('checked', false);
			}

			if($('.training-state.active').data('id') == 'VIC') {
				if($('.training-location.active').text().trim() == 'Colac') {
					if ($(v).parent().next().text().trim() == 'Colac') {
						//$(v).parents('.cont').show();
						$(v).prop('checked', true);
					} else {
						//$(v).parents('.cont').hide();
						$(v).prop('checked', false);
					}
				} else {
					if($('.training-location.active').text().trim() == 'Geelong') {
						if($(v).parent().next().text().trim() == 'Geelong') {
							//$(v).parents('.cont').show();
							$(v).prop('checked', true);
						} else {
							//$(v).parents('.cont').hide();
							$(v).prop('checked', false);
						}
					} else {
						if($(v).parent().next().text().trim() == 'Geelong' || $(v).parent().next().text().trim() == 'Colac') {
							$(v).prop('checked', false);
						}
					}
				}
			}
			$('#booking_location').parent().show();
		} else {
			//Online - only show online, right?
			if($(v).parent().next().text() == 'Online') {
				$(v).parents('.cont').show();
				$(v).prop('checked', true);
			} else {
				$(v).parents('.cont').hide();
			}
			$('#booking_location').parent().hide();
		}
	});

	_location = $('.training-location.active').data('id');
	$('.training-course').show();

	switch (_trainType) {
		case 1: //public
			$.each($('.training-course'), function (k, v) {
				if($(v).data('public') == 0) {
					$(v).hide();
				}

				if($(v).data('locations') === undefined) {
					$(v).show();
				} else {
					if(!$(v).data('locations').includes(_location)) {
						$(v).hide();
					}
				}
			});
			break;
		case 0: //onsite
			$.each($('.training-course'), function (k, v) {
				if($(v).data('onsite') == 0) {
					$(v).hide();
				}

				if($(v).data('locations') === undefined) {
					$(v).show();
				} else {
					if(!$(v).data('locations').includes(_location)) {
						$(v).hide();
					}
				}
			});
			break;
		case 2: //online
			$.each($('.training-course'), function (k, v) {
				if($(v).data('online') == 0) {
					$(v).hide();
				} else {
					$(v).show();
				}
			});
			$('#online-multiples').show();
			break;
	}
}

function trackClick(_elem) {
	console.log(_elem);
	_clickVal = $('#clicks').val();
	if(_clickVal.length > 0) {
		_clicks = JSON.parse(_clickVal);
	} else {
		_clicks = [];
	}
	//console.log('clicks', _clicks);
	//console.log('elem', $(_elem));
	data = $(_elem).data();
	//console.log('data', data);
	classes = 'ID: ' + $(_elem).attr('id') + '; Classes: ' + $(_elem)[0].classList;
	//console.log('classes', classes);

	var _myText = $(_elem).find('strong').text().trim();
	if(_myText.length < 1) {
		_myText = $(_elem).text();
	}

	newData = {
		'data': data,
		'classes': classes,
		'text': _myText
	};
	//console.log(newData);
	_clicks.push(newData);
	//console.log(_clicks);
	$('#clicks').val(JSON.stringify(_clicks));

	if(_couponApplied && $(_elem).attr('id') != 'payment-button') {
		if($('#ownEquipment').length == 1) {
			//pfft
		} else {
			_couponApplied = false;
			$('#coupon-message').show();
			updateCosts(true);
		}
	}
}

$(document).on('click', '.training-state', function(e) {
	trackClick($(this));

	$('.training-state').removeClass('active');

	$('#course-text').remove();
	//Hide all pages after this one
	_parent = $(this).parents('.booking-page-container');
	_parent.show();
	_parent.nextAll().hide();
    $(this).addClass('active');

    updateLocationText();

    $('.training-location').parent().hide();
    $('.training-location[data-state="' + $(this).data('id') + '"]').parent().show();

    _locs = $('.training-location[data-state="' + $(this).data('id') + '"]');

    if(_locs.length > 0) {
		scrollPage('#page-location').then(function() {
			if($('#page-location').hasClass('prefilled')) {

			} else {
				$('.training-location').removeClass('active');
			}

			if($('#page-courses').hasClass('prefilled')) {

			} else {
				$('.training-course').removeClass('active');
			}

			if($('#page-type').hasClass('prefilled')) {

			} else {
				$('.training-type').removeClass('active');
			}

			if(_locs.length == 1) {
				$(_locs[0]).addClass('active');
			}

			$('.training-location.active').click();
		});
	} else {
    	//There are no locations; hide "Public"
		$('.training-type[data-id="1"]').hide();
		scrollPage('#page-type').then(function() {
			if($('#page-location').hasClass('prefilled')) {

			} else {
				$('.training-location').removeClass('active');
			}

			if($('#page-courses').hasClass('prefilled')) {

			} else {
				$('.training-course').removeClass('active');
			}

			if($('#page-type').hasClass('prefilled')) {

			} else {
				$('.training-type').removeClass('active');
			}

			$('.training-type.active').click();
		});

		$('#other-location .training-location').click();
	}
});

$(document).on('click', '.training-location', function(e) {
	trackClick($(this));

	$('.training-location').removeClass('active');
	$('#course-text').remove();
	//Hide all pages after this one
	_parent = $(this).parents('.booking-page-container');
	_parent.show();
	_parent.nextAll().hide();
	$(this).addClass('active');

	_locationFees = $(this).data('deliverfees');
	_stateFees = $('.training-state.active').data('deliverfees');

	_fees = (_locationFees === undefined ? _stateFees : _locationFees);

	$('.delivery-fee').val(_fees).text(_fees);

	if($(this).data('public') == '1') {
		$('.training-type[data-id="1"]').show();
	} else {
		$('.training-type[data-id="1"]').hide();
	}

	$('.pickup-location').hide();
	$('.pickup-location[data-location="' + $('.training-location.active').data('id') + '"]').show();

	updateLocationText();
	scrollPage('#page-type').then(function() {
		if($('#page-courses').hasClass('prefilled')) {

		} else {
			$('.training-course').removeClass('active');
		}

		if($('#page-type').hasClass('prefilled')) {
			$('.training-type.active').click();
		} else {
			$('.training-type').removeClass('active');

			//Is there only one shown?
			if($('.training-type:visible').length == 1) {
				$('.training-type:visible').click();
			}
		}
	});

    flashSummary();
});

$(document).on('click', '.training-course', function(e) {
	trackClick($(this));

	$('.training-course').removeClass('active');
	$('#course-text').remove();
	//Hide all pages after this one
	_parent = $(this).parents('.booking-page-container');
	_parent.show();
	_parent.nextAll().hide();
	$(this).addClass('active');
	$(this).addClass('active');

	if($('.training-type.active').data('id') == 2 && $(this).data('category') == 'First Aid') {
		$('#online-tc').show();
	}

	//check the rules
	_redirect = false;

	_hours = $('.training-course.active').data('hours');
	_items = $('#start-time-template').children().clone();

	$('#start-time').html('<option value="0">(please select)</option>');
	for(i = 0; i < (_items.length - (_hours * 2)); i++) {
		$('#start-time').append(_items[i]);
	}
	$('#start-time').val(0);

	$('#progress-3').addClass('active');
	$('#course-text').remove();
	$('#booking-summary').append('<p id="course-text">The course you are booking is <a href="#" class="back-link" id="back-course">' + $('.training-course.active p strong').text().trim() + '</a></p>');

	var show = ($('.training-type').data('id') < 2);
	updateCosts(show);

	if ($(this).data('duration') == 0) {
		//Half day, so show the prompt, and allow half days in the calendar
		$('#booking-calendar .half-day').show();
		$('#half-day').show();
	} else {
		$('#booking-calendar .half-day').hide();
		$('#half-day').hide();
	}

	if(_redirect) {
		window.location.href = _redirect;
	} else if($('.training-course.active').text().trim() == "Other") {
		$('#unavailable-course').show();
		$('#unavailable-location').hide();
		scrollPage('#page-error');
	} else {
		if($('.training-type.active strong').text().trim() == 'Onsite') {
			scrollPage('#page-corporate-participants');
		} else {
			//Go to aXcelerate and get the course data.
			//Should the equipment page be shown?

			if ($('.training-course.active').data('equipment') == '1') {
				//Show equipment
				if($('#ownEquipment').length == 1) {
					//They have their own equipment.  Maybe show a message
					$('.online-delivery[data-id="0"]').hide();
					$('.online-delivery[data-id="1"]').hide();
					$('.online-delivery[data-id="2"]').hide();

					$('.online-delivery[data-id="3"]').show().click();
					//$('#equipment-alert').hide();
					$('#online-delivery-button').show().click();
				} else {
					$('.online-delivery[data-id="3"]').hide();
					$('.online-delivery[data-id="1"]').show();

					if ($('.training-location.active').data('equippick') == 1) {
						$('.online-delivery[data-id="0"]').show(); //pickup
					} else {
						$('.online-delivery[data-id="0"]').hide(); //pickup
					}

					if ($('.training-location.active').data('equipface') == 1 && $('.training-course.active').data('axf2fid') !== undefined) {
						$('.online-delivery[data-id="2"]').show(); //face-to-face
					} else {
						$('.online-delivery[data-id="2"]').hide(); //face-to-face
					}

					console.log('page-equipment');
					$('#page-equipment').show();
					$('#page-public-instance-1').hide();

					if ($('#deliveryFee').val() > 0) {
						$('#equipment-alert').hide();
						$('#online-delivery-button').show();
					} else {
						$('#equipment-alert').show();
						$('#online-delivery-button').hide();
					}
				}

				scrollPage('#page-public-instance');
			} else {
				$('.online-delivery[data-id="3"]').hide();
				//Show instances
				$('#page-equipment').hide();
				$('#page-public-instance-1').show();
				getAxcelCourses();

				if ($('#page-public-instance').hasClass('prefilled')) {
					//If the instance is pre-filled, we shouldn't show this page.
					if ($('#courseDate').val() != '') {
						$('#booking-summary').append('<p id="date-text">The course is on <a href="#" class="back-link" id="back-public-instance">' + $('#courseDate').val() + '</a></p>');
					}
					$('#page-public-instance').removeClass('prefilled');
					//Show the contact details page instead
					scrollPage('#page-public-contact');
					updateCosts();
				} else {
					scrollPage('#page-public-instance');
				}
			}
		}
	}

	$('.booking-for-container').hide();
	if($('.training-course.active').data('participant') == -1) {
		//normal, show public
		$('.booking-for-container.public').show();
	} else {
		//show partner
		$('.booking-for-container.partner').show();
	}

	var date = new Date();

	updateCalendar(date.getFullYear(), date.getMonth() + 1, $('.training-location.active').text().trim(), $('.training-course.active').data('category'));

	flashSummary();
});

$(document).on('click', '#equipment-location', function(e) {
	e.preventDefault();
	$('#page-state').removeClass('prefilled').removeClass('nevershow');
	$('#page-location').removeClass('prefilled').removeClass('nevershow');
	scrollPage('#page-state');
});

function updateLocationText() {
	if($('.training-state.active').length > 0) {
		_location = ($('.training-location.active').length > 0 ? '<a href="#" class="back-link" id="back-location">' + $('.training-location.active').text().trim() + '</a>, ' : '');

		if(_location == '<a href="#" class="back-link" id="back-location">Other</a>, ') {
			_location = '';
		}

		$('#location-text').html('You have selected a' + ($('.training-type.active strong').text().trim() == 'Onsite' ? 'n' : '') + ' <a href="#" class="back-link" id="back-type">' + $('.training-type.active strong').text().trim() + '</a> course in ' + _location + '<a href="#" class="back-link" id="back-state">' + $('.training-state.active').text().trim() + '</a>').show();
	} else {
		$('#location-text').hide();
	}
}

$(document).on('click', '#showFull', function(e) {
	e.preventDefault();
	$('.row.session').removeClass('odd').removeClass('even');
	$('.session.full').toggleClass('shown');
	$('.row.session:visible:odd').addClass('odd');
	$('.row.session:visible:even').addClass('even');
	if($(this).text() == 'Show Full Courses') {
		$(this).text('Hide Full Courses');
	} else {
		$(this).text('Show Full Courses');
	}
});

function getAxcelCourses() {
	$('#axcel-courses').html('<div class="course-grid container"><div class="row">    <div class="col-12 text-center loading"><span><i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i><br>Loading available courses</span></div></div></div>');

	var _axid = 0;

	if($('.online-delivery.active').data('id') == 2) {
		_axid = $('.training-course.active').data('axf2fid');
	}

	if(_axid == 0) {
		_axid = $('.training-course.active').data('axid');
	}

	var venues = $.map($('.MultiCheckBoxDetailBody .mulinput:checked'), function(n, i){
		return n.value;
	}).join(',');

	_axcelData = {
		'aid': _axid,
		'cid': $('.training-course.active').data('id'),
		'location_id': $('.pickup-location.active').data('location'),
		'venue_ids': venues,
		'type': $('.training-course.active').data('type'),
		'geelong': $('#geelong').val(),
		'colac': $('#colac').val(),
		'altLocation': $('.training-course.active').data('axid-on') != '',
		'delType': $('#booking_delivery').val(),
		'iid' : $('#iid').val(),
		'showTimezone' : ($('.training-type.active').data('id') == 2 ? 1 : 0),
		'private' : $('.training-course.active').data('pubpriv'),
		'priority' : $('#priority_courses').val()
	};

	if($('.training-type.active').data('id') != 2) {
		_axcelData.lid = $('.training-state.active').data('id');
	}

	_stateDays = $('.training-state.active').data('deliverdays');
	_locationDays = $('.training-location.active').data('deliverdays');

	//send days if pickup or delivery, BUT ONLY IF ONLINE
	if($('.training-type.active').data('id') == 2) {
		_days = ($('.online-delivery.active').data('id') == 2 ? 0 : (_locationDays === undefined ? _stateDays : _locationDays));
		_axcelData.days = _days;
	}

	if($('#portal_axid')) {
		_axcelData.company = $('#portal_axid').val();
	}

	if($('.training-course.active').data('price') != '') {
		_axcelData.price = $('.training-course.active').data('price');
	}

	$.ajax({
		'url': _baseurl + 'axcel.php',
		'dataType': 'html',
		'data': _axcelData,
		'success': function (data) {
			$('#axcel-courses').html(data);
			updateInstancePage();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			$('#axcel-courses').html(jqXHR.responseText);
			updateInstancePage();
		}
	});
	scrollPage('#page-public-instance-1');
}

function updateInstancePage() {
	$('[data-toggle="popover"]').popover();

	$('#booking_delivery').val($('.training-type.active').data('id'));

	$('#booking_equipment').html('');

	if($('#booking_delivery').val() == 1) {
		$('#booking_equipment').parent().hide();
		$('#page-equipment').hide();
	} else {
		if($('.training-course.active').data('category') == 'First Aid') {
			$('#booking_equipment').parent().show();
			$('#page-equipment').show();
		} else {
			$('#booking_equipment').parent().hide();
			$('#page-equipment').hide();
		}

		//Hide face to face?
		var _scroll = $('#current-page').scrollTop() + $('#page-public-instance-1').position().top;
		$('#current-page').animate({scrollTop: _scroll}, 1);
	}

	showHideEquipment();

	if($('.online-delivery[data-id=0]').is(':visible')) {
		$('#booking_equipment').append('<option value="0">Pick-up</option>');
	}

	if($('.online-delivery[data-id=1]').is(':visible')) {
		$('#booking_equipment').append('<option value="1">Delivery (+ $' + $('#deliveryFee').val() + ')</option>');
	}

	if($('.online-delivery[data-id="2"]').is(':visible')) {
		//Have face to face in the dropdown
		$('#booking_equipment').append('<option value="2">Face to Face</option>');
	}

	if($('.online-delivery[data-id="3"]').is(':visible')) {
		//Have face to face in the dropdown
		$('#booking_equipment').append('<option value="3">Our own equipment</option>');
	}

	$('#booking_equipment').val($('.online-delivery.active').data('id'));

	if($('.training-course.active').data('axid-on') == '') {
		$('#booking_delivery').attr('disabled', 'disabled');
	} else {
		$('#booking_delivery').removeAttr('disabled');
	}

	$('#back-type').text($('.training-type.active strong').text().trim());

	if(($('.training-location.active').text().trim() != 'Melbourne') && ($('.training-location.active').text().trim() != 'Geelong')) {
		$('.public-instance-button[data-available="1"]').parents('.session').hide();
	}

	$('.row.session:visible:odd').addClass('odd');
	$('.row.session:visible:even').addClass('even');

	var _newTemplate = $('.training-course.active').data('template');
	//console.log(_newTemplate);
	if(_newTemplate != '') {
		$.each($('.public-instance-button'), function(k,v) {
			//console.log($(v));
			//console.log(_newTemplate);
			$(v).attr('data-template', _newTemplate);
		});
	}

	var _preclick = $('.public-instance-button.preclick');
	//console.log(_preclick);
	if($('#courseInstance').val() > 0 && _preclick.length == 0) {
		$('#page-public-instance-1').removeClass('nevershow');
		$('#unavailable-instance').show();
		scrollPage('#page-public-instance-1');
	}

	if(_preclick.length > 0) {
		$(_preclick[0]).click();
	}
}

$(document).on('click', '#filterAxcel', function(e) {
	trackClick($(this));

	e.preventDefault();
	getAxcelCourses();
});

$(document).on('change', '#booking_location', function(e) {
	trackClick($(this));

	$this = $(this);
	$this.addClass('active');
	//hide all other
	$('.axcel-location-courses:not([data-id=' + $this.val() + '])').slideUp();
	$('.axcel-location-courses[data-id=' + $this.val() + ']').slideDown();
});

$(document).on('change', '#booking_delivery', function(e) {
	trackClick($(this));

	myval = $(this).val();
	//console.log('delivery_val ', myval);

	$('.training-type').removeClass('active');
	$('.training-type[data-id="' + myval + '"]').addClass('active');
	updateLocations();

	//Set the active course again in here
	_public_id = $('.training-course.active').data('axid-pu');
	_online_id = $('.training-course.active').data('axid-on');
	$('.training-course.active').removeClass('active');
	if(myval == 2) {
		$('.training-course[data-id=' + _online_id + ']').addClass('active');
		//alert(_online_id);
	} else {
		$('.training-course[data-id=' + _public_id + ']').addClass('active');
		//alert(_public_id);
	}

	$('#course-text').html('The course you are booking is <a href="#" class="back-link" id="back-course">' + $('.training-course.active p strong').text().trim() + '</a>');

	//$('.training-course.active').click();

	getAxcelCourses();
});

$(document).on('change', '#booking_equipment', function(e) {
	trackClick($(this));

	myval = $(this).val();
	console.log('equipment_val ', myval);
	$('#booking_equipment').removeClass('validation_error');

	$('.online-delivery').removeClass('active');
	$('.online-delivery[data-id="' + myval + '"]').addClass('active');

	updateOnlineDelivery($('.online-delivery[data-id="' + myval + '"]'));
});

$('.popover-dismiss').popover({
	trigger: 'focus'
});

$(document).on('change', '#start-time', function(e) {
	trackClick($(this));

	_hours = $('.training-course.active').data('hours');
	_items = $('#start-time-template').children();
	_idx = $('#start-time').prop('selectedIndex') - 1;

	$('#finish-time').text($(_items[_idx + (_hours * 2)]).val());
	$('#fin-time').val($(_items[_idx + (_hours * 2)]).val());
});

$(document).on('click', '.training-course i', function(e) {
	//trackClick($(this));

    e.preventDefault();
    e.stopPropagation();
    var _kids = $(this).parents('.training-course').children('.key-info');
    if(_kids) {
        if($(this).hasClass('fa-chevron-down')) {
            $(_kids[0]).slideDown();
            $(this).removeClass('fa-chevron-down');
            $(this).addClass('fa-chevron-up');
        } else {
            $(_kids[0]).slideUp();
            $(this).removeClass('fa-chevron-up');
            $(this).addClass('fa-chevron-down');
        }
    }

    flashSummary();
});

$(document).on('click', '#participant-button', function(e) {
	trackClick($(this));

    $('#progress-4').addClass('active');
	$('#page-start-1').show();
	if($('.training-type.active').data('id') != 2) {
		$('#page-start-2').show();
	} else {
		$('#page-start-2').hide();
	}
	$('#page-courses').show();
	$('#page-corporate-participants').show();
	$('#page-corporate-calendar').hide();
	$('#page-corporate-contact').hide();
	$('#page-corporate-address').hide();
	$('#page-public-instance').hide();
	$('#page-public-contact').hide();
	$('#page-public-participants').hide();
	$('#page-public-match').hide();
	$('#page-payment').hide();
	$('#page-finish').hide();
	$('#page-error').hide();
  	$('#participant-text').remove();
	$('#booking-summary').append('<p id="participant-text">for <a href="#" class="back-link" id="back-participant">' + $('#participant-number').val() + ' people</a></p>');
    var _amt = 0;

	scrollPage('#page-corporate-calendar');

    updateCosts();

    flashSummary();
});

$(document).on('click', '#apply-coupon', function(e) {
	trackClick($(this));
	_couponApplied = true;

	$('#coupon-errors').hide();
	$('#payment-button').attr('disabled', 'disabled').addClass('disabled');
	e.preventDefault();
	_participants = $('#participants').children('div').length + ($('.booking-for.btn-primary').data('id') != '2' ? 1 : 0);
	$.ajax({
		'type' : 'POST',
		'url' : _baseurl + 'coupon-check.php',
		'dataType' : 'json',
		'data' : {
			'coupon' : $('#coupon-code').val(),
			'participants' : _participants,
			'course' : $('.training-course.active').data('id'),
			'location' : $('.training-location.active').data('id'),
			'type': $('.training-type.active').data('id'),
			'courseDate' : $('#selected-date').val()
		},
		'success' : function(data) {
			//console.log(data);
			if(data.success == true) {
				//Apply it
				_costs = $('#courseAmt').val() / 100;
				if(data.type == 2) {
					//percentage
					_costs = _costs * ((100 - data.amount) / 100);
				} else {
					if(data.useType == 1) {
						_costs = _costs - data.amount;
					} else {
						_costs = _costs - (data.amount * _participants);
					}
				}

				if (_costs < 0) {
					_costs = 0;
				}

				_costs = _costs.toFixed(2);

				$('.total-amount').html('<strike>$' + ($('#courseAmt').val() / 100) + '</strike> $' + _costs);
				$('#total-amount').val(_costs);
				$('#courseAmt').val(_costs * 100);
				/*if(data.axType == 0) {
					$('#courseAmtEach').val(_costs / _participants);
				}*/
				$('#coupon-success').show();
				$('#coupon-code-valid').val($('#coupon-code').val());

				if(_costs == 0) {
					$('#payment-form').hide();
					$('#payment-button').text('Confirm');
				} else {
					$('#payment-form').show();
					$('#payment-button').text('Pay and confirm');
				}
				$('#apply-coupon').attr('disabled', 'disabled').addClass('disabled');
			} else {
				//Show an error
				$('#coupon-errors').text(data.message).show();
				$('#coupon-errors').append('<div id="coupon-error-details"></div>');
				$.each(data.details, function(k,v) {
					$('#coupon-error-details').append('<p>' + v + '</p>');
				});
				$('#coupon-code-valid').val('');
			}

			$('#payment-button').removeAttr('disabled').removeClass('disabled');
		}
	});
});

$(document).on('dblclick', '#coupon-errors', function(e) {
	$('#coupon-error-details').show();
});

$(document).on('click', '.public-instance-button', function(e) {
	$('.public-instance-button').removeClass('active');
	$(this).attr('disabled', 'disabled').html('<i class="fa fa-spinner fa-pulse"></i>').addClass('active');
	$('#places').text(($(this).data('places') == 1 ? 'is only 1 place remaining.' :  'are only ' + $(this).data('places') + ' places remaining.'));
	trackClick($(this));

	_parents = $(this).parents('.session');
	_address = _parents.find('.address a');
	_loc = _address.data('original-title');
	//console.log('parents', _parents);
	//console.log('address', _address);
	//console.log('location name', _loc);

	if($('#location-text').text().length == 0) {
		$('#booking-summary').prepend('<p id="location-text">You have selected a <a href="#" class="back-link" id="back-type">Public</a> course in <a href="#" class="back-link" id="back-location">' + _loc + '</a></p>');
	}

	let dateString = 'The date you have selected is ' + nicedate($(this).data('date'));
	if($('#date-text').length > 0) {
		$('#date-text').text(dateString);
	} else {
		$('#booking-summary').append('<p id="date-text">' + dateString + '</p>');
	}

	e.preventDefault();
	$('#selected-date').val($(this).data('date'));
	$('#page-start-1').show();
	if($('.training-type.active').data('id') != 2) {
		$('#page-start-2').show();
	} else {
		$('#page-start-2').hide();
	}

	$('#page-courses').show();
	$('#page-corporate-participants').hide();
	$('#page-corporate-calendar').hide();
	$('#page-corporate-contact').hide();
	$('#page-corporate-address').hide();
	$('#page-public-instance').show();
	$('#page-public-contact').hide();
	$('#page-public-participants').hide();
	$('#page-public-match').hide();
	$('#page-payment').hide();
	$('#page-finish').hide();
	$('#page-error').hide();
	$('#progress-4').addClass('active');
	$('#participant-instance').val($(this).data('id'));
	$('#participant-type').val($('.training-course.active').data('type'));

	var _continue = true;
	if($('#booking_equipment').is(':visible') && $('#booking_equipment').val() == null) {
		//Prevent moving on and highlight it as an error?
		$('#booking_equipment').addClass('validation-error');
		_continue = false;
		$('#page-equipment-details').hide();
		//console.log('not continuing');
	}

	if(_continue) {
		showHideEquipment();
		//console.log('continuing');
		if ($('#ownEquipment').val() === undefined && $('.training-type.active').data('id') == 2 && $('.training-course.active').data('type') == 'w') {
			$('#page-equipment-details').slideDown();
			scrollPage('#page-equipment-details');
		} else {
			scrollPage('#page-public-contact');
		}

		$('#courseInstance').val($(this).data('id'));
		$('#courseAmtEach').val($(this).data('amt'));
		if(_formType == 'Website') {
			$('#div-credit').show();
		}
		//$('#btn-credit').hide();
		//$('#btn-invoice').hide();

		var _amt = 0;
		updateCosts();

		_costs = $('#courseAmt').val() / 100;
		if (_costs == 0) {
			$('#payment-form').hide();
			$('#payment-button').text('Confirm');
		} else {
			$('#payment-form').show();
			$('#payment-button').text('Pay and confirm');
		}

		flashSummary();
	}

	$('.public-instance-button.active').removeAttr('disabled').html('<i class="fa fa-chevron-right"></i>');
});

function showHideEquipment() {
	//Show equipment
	if($('.training-location.active').data('equippick') == 1) {
		$('.online-delivery[data-id="0"]').show(); //pickup
	} else {
		$('.online-delivery[data-id="0"]').hide(); //pickup
	}

	if($('.training-location.active').data('equipface') == 1 && $('.training-course.active').data('axf2fid') !== undefined) {
		$('.online-delivery[data-id="2"]').show(); //face-to-face
	} else {
		$('.online-delivery[data-id="2"]').hide(); //face-to-face
	}
}

$(document).on('click', '.online-delivery', function(e) {
	trackClick($(this));

	updateOnlineDelivery($(this));
});

function updateOnlineDelivery(elem) {
	$('.online-delivery').removeClass('active');
	elem.addClass('active');
	$('#error-online-type').hide();

	switch (elem.data('id')) {
		case 1:
			$('#online-delivery-delivered').slideDown();
			$('#online-delivery-pickup').slideUp();
			$('#online-delivery-face').slideUp();
			break;

		case 0:
			$('#online-delivery-delivered').slideUp();
			$('#online-delivery-pickup').slideDown();
			$('#online-delivery-face').slideUp();

			$('#online-address-street').val('');
			$('#online-address-city').val('');
			$('#online-address-state').val('');
			$('#online-address-postcode').val('');
			$('#online-address-comments').val('');
			break;

		case 2:
			//Face to Face. Show Calendly?
			$('#online-delivery-delivered').slideUp();
			$('#online-delivery-pickup').slideUp();
			$('#online-delivery-face').slideDown();
			break;
	}

	updateCosts(false);
}

$(document).on('click', '#online-delivery-button', function(e) {
	trackClick($(this));

	//Validate
	$('#error-online-pickup').hide();
	$('#error-online-street').hide();
	$('#error-online-city').hide();
	$('#error-online-state').hide();
	$('#error-online-postcode').hide();
	_error = false;

	if($('.online-delivery.active').length > 0) {

	} else {
		$('#error-online-type').show();
		_error = true;
	}

	if(!_error) {
		getAxcelCourses();
	}
});

$(document).on('click', '#online-delivery-option-button', function(e) {
	trackClick($(this));

	var _error = false;
	switch ($('.online-delivery.active').data('id')) {
		case 1:
			if ($('#online-address-street').val().trim().length == 0) {
				$('#error-online-street').show();
				_error = true;
			}

			if ($('#online-address-city').val().trim().length == 0) {
				$('#error-online-city').show();
				_error = true;
			}

			if ($('#online-address-state').val().trim().length == 0) {
				$('#error-online-state').show();
				_error = true;
			}

			if ($('#online-address-postcode').val().trim().length == 0) {
				$('#error-online-postcode').show();
				_error = true;
			}
			break;

		case 0:
			if($('.pickup-location.active').length > 0) {
				//We have a location, we good
			} else {
				$('#error-online-pickup').show();
				_error = true;
			}
			break;

		case 2:

			break;
	}

	if(!_error) {
		scrollPage('#page-public-contact');
	}
});

$(document).on('click', '.pickup-location', function() {
	trackClick($(this));

	$('.pickup-location').removeClass('active');
	$(this).addClass('active');
})

function validateEmail(email) {
	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
}

$(document).on('click', '#public-contact-button', function(e) {
	trackClick($(this));

	$('#page-start-1').show();
	if($('.training-type.active').data('id') != 2) {
		$('#page-start-2').show();
	} else {
		$('#page-start-2').hide();
	}
	$('#page-courses').show();
	$('#page-corporate-participants').hide();
	$('#page-corporate-calendar').hide();
	$('#page-corporate-contact').hide();
	$('#page-corporate-address').hide();
	$('#page-public-instance').show();
	$('#page-public-contact').show();
	$('#page-public-participants').hide();
	$('#page-public-match').hide();
	$('#page-payment').hide();
	$('#page-finish').hide();
	$('#page-error').hide();

	_error = false;
	if($('#public-contact-first').val().trim().length == 0) {
		$('#error-public-first').show();
		_error = true;
	}

	if($('#public-contact-last').val().trim().length == 0) {
		$('#error-public-last').show();
		_error = true;
	}

	if($('#public-contact-email').val().trim().length == 0) {
		$('#error-public-email').show();
		_error = true;
	} else {
		if(!validateEmail($('#public-contact-email').val().trim())) {
			$('#error-public-email').show();
			_error = true;
		}
	}

	if($('#public-contact-phone').val().trim().length == 0) {
		$('#error-public-phone').show();
		_error = true;
	}

	if($('.booking-for.btn-primary') == null || $('.booking-for.btn-primary').length == 0) {
		$('#error-public-type').show();
		_error = true;
	}

	if(!_error) {
		_bookType = $('.booking-for.btn-primary').data('id');
		$('#progress-5').addClass('active');
		$('#participant-booker').show().html('<h2>Participant <span class="participant-number">1</span></h2>' +
			'<p><strong>First Name:</strong> ' + $('#public-contact-first').val() + '</p>' +
			'<p><strong>Last Name:</strong> ' + $('#public-contact-last').val() + '</p>' +
			'<p><strong>Email:</strong> ' + $('#public-contact-email').val() + '</p>' +
			'<p><strong>Phone:</strong> ' + $('#public-contact-phone').val() + '</p>' +
			'<input type="hidden" name="public-participant-first[]" value="' + $('#public-contact-first').val() + '">' +
			'<input type="hidden" name="public-participant-last[]" value="' + $('#public-contact-last').val() + '">' +
			'<input type="hidden" name="public-participant-email[]" value="' + $('#public-contact-email').val() + '">' +
			'<input type="hidden" name="public-participant-phone[]" value="' + $('#public-contact-phone').val() + '">'
		);

		$('#add-participant-button').show();
		if(_bookType == 0) {
			//Only myself
			$('#contact-text').remove();
			$('#booking-summary').append('<p id="contact-text">The person being booked for is<br><a href="#" class="back-link" id="back-public-contact">' + $('#public-contact-first').val() + ' ' + $('#public-contact-last').val() + '</a></p>');
			//only me, kill all others
			$('#participants').html('');
			showMatchPage();
		} else {
			scrollPage('#page-public-participants');

			$('#contact-text').remove();
			$('#booking-summary').append('<p id="contact-text">The point of contact will be<br><a href="#" class="back-link" id="back-public-contact">' + $('#public-contact-first').val() + ' ' + $('#public-contact-last').val() + '</a></p>');

			if(_bookType == 1) {
				//Myself and others
			} else {
				if(_bookType == 3) {
					$('#add-participant-button').hide();
				} else {
					$('#participant-booker').html('').hide();
				}
			}

			_count = $('#participants').children('div').length;
			if(!$('#participant-booker').is(':hidden')) {
				_count++;
			}

			if(_count < 2) {
				addParticipant();
			}
		}

		updateCosts();
		flashSummary();
	}
});

function addParticipant() {
	//how many are already there?
	_count = $('#participants').children('div').length;
	if(!$('#participant-booker').is(':hidden')) {
		_count++;
	}

	if(_count < $('.public-instance-button.active').data('places')) {

		$clone = $('#participant-details-template').clone();
		$.each($clone.find('input'), function (k, v) {
			$(v).attr('id', $(v).attr('id') + (_count + 1));
		});

		$.each($clone.find('.participant-number'), function (k, v) {
			$(v).text((_count + 1)).parent().append('<a href="#" class="btn btn-primary remove-participant"><i class="fa fa-times"></i></a>');
		});

		$clone.removeAttr('id').appendTo('#participants');

		if((_count + 1) < $('.public-instance-button.active').data('places')) {
			$('#too-many-participants').hide();
			$('#add-participant-container').show();
		} else {
			$('#too-many-participants').show();
			$('#too-many-participants .alert').show();
			$('#add-participant-container').hide();
		}

		updateCosts();
	} else {
		$('#too-many-participants').show();
		$('#too-many-participants .alert').show();
		$('#add-participant-container').hide();
	}
}

$(document).on('click', '#add-participant-button', function(e) {
	trackClick($(this));

	e.preventDefault();

	$('#participant-count-error').hide().children().hide();

	$('#page-public-match').hide();
	$('#page-payment').hide();
	$('#page-finish').hide();
	$('#page-error').hide();

	addParticipant();
});

$(document).on('click', '.remove-participant', function(e) {
	trackClick($(this));

	e.preventDefault();

	$('#page-public-match').hide();
	$('#page-payment').hide();
	$('#page-finish').hide();
	$('#page-error').hide();

	$(this).parents('.participant-details').remove();

	$('#too-many-participants').hide();
	$('#add-participant-container').show();

	//Update numbers or whatever...
	$i = 1;
	$.each($('.participant-number'), function(k,v) {
		$(v).text($i);
		$i++;
	});

	var _count = $('#participants').children('div').length + ($('.booking-for.btn-primary').data('id') != '2' ? 1 : 0);
	if(_count == 0) {
		$('#participant-count-error').show().children().show();
	}

	updateCosts();
});

$(document).on('click', '#public-participant-button', function(e) {
	trackClick($(this));
	var _count = $('#participants').children('div').length + ($('.booking-for.btn-primary').data('id') != '2' ? 1 : 0);
	if(_count == 0) {
		$('#participant-count-error').show().children().show();
	} else {

		$('#page-start-1').show();
		if ($('.training-type.active').data('id') != 2) {
			$('#page-start-2').show();
		} else {
			$('#page-start-2').hide();
		}
		$('#page-courses').show();
		$('#page-corporate-participants').hide();
		$('#page-corporate-calendar').hide();
		$('#page-corporate-contact').hide();
		$('#page-corporate-address').hide();
		$('#page-public-instance').show();
		$('#page-public-contact').show();
		$('#page-public-participants').show();
		$('#page-public-match').hide();
		$('#page-payment').hide();
		$('#page-finish').hide();
		$('#page-error').hide();

		_error = false;

		$.each($('#participants input[name="public-participant-first[]"]'), function (k, v) {
			if($(v).val().trim().length == 0) {
				_error = true;
				$(v).addClass('validation-error');
			}
		});

		$.each($('#participants input[name="public-participant-last[]"]'), function (k, v) {
			if($(v).val().trim().length == 0) {
				_error = true;
				$(v).addClass('validation-error');
			}
		});

		$.each($('#participants input[name="public-participant-email[]"]'), function (k, v) {
			if (!validateEmail($(v).val())) {
				_error = true;
				$(v).addClass('validation-error');
			}
		});

		$.each($('#participants input[name="public-participant-phone[]"]'), function (k, v) {
			if($(v).val().trim().length == 0) {
				_error = true;
				$(v).addClass('validation-error');
			}
		});

		if (!_error) {
			updateCosts();
			flashSummary();
			showMatchPage();
			$('#progress-6').addClass('active');
		}
	}
});

$(document).on('click', '.match-button', function() {
	trackClick($(this));

	//Find any filled input in this container and compare it to the container's attributes.
	_updated = false;
	$parent = $(this).parent().parent();
	$matchInfo = $parent.parent();
	_dob_d = '';
	_dob_m = '';
	_dob_y = '';
	_contact_id = 0;
	$.each($parent.find('input, select'), function(k,v) {
		if($(v).val().length > 0) {
			if($(v).hasClass('input-dob-d')) {
				s = '00' + $(v).val();
				_dob_d = s.substr(s.length - 2);
			}
			if($(v).hasClass('input-dob-m')) {
				_dob_m = $(v).val();
			}
			if($(v).hasClass('input-dob-y')) {
				_dob_y = $(v).val();
			}
		}
	});

	if(_dob_d.length > 0 && _dob_m.length > 0 && _dob_y.length > 0) {
		$.each($matchInfo.find('.pMatch[data-dob="' + _dob_y + '-' + _dob_m + '-' + _dob_d +  '"]'), function(l,w) {
			//console.log($(w));
			$matchInfo.find('.public-participant').val($(w).data('id'));
			//$parent.find('.public-participant-detail').remove();
			_updated = true;
		});
	}

	if(_updated) {
		//flash a success?
		$(this).attr('disabled', 'disabled');
		$matchInfo.append('<div class="col-12"><p>This user has been found in our system.</p></div>');
	} else {
		$(this).html('<i class="fa fa-ban"></i>').attr('disabled', 'disabled').addClass('btn-danger').removeClass('btn-success');
		$matchInfo.append('<div class="col-12"><p>This user can\'t be found in our system.  A new user will be created.</p></div>');
	}
});

function showMatchPage() {
	$.ajax({
		'type' : 'POST',
		'url' : _baseurl + 'axcel-person.php',
		'dataType' : 'json',
		'data' : $('#participant-form').serialize(),
		'success' : function(data) {
			$('#participant-match-form').html(data.html);
			if(_formType == 'Website') {
				$('#div-credit').show();
			}
			//$('#btn-credit').hide();
			//$('#btn-invoice').hide();
			$('#progress-7').addClass('active');
			$('#progress-6').addClass('active');

			scrollPage('#page-public-match');

			$('#coupon-errors').hide();
			$('#coupon-success').hide();
			$('#apply-coupon').removeAttr('disabled').removeClass('disabled');
			$('#grp-coupon').show();

			$('#participant-text').remove();
			$('#participant-ul').remove();

			_bookType = $('.booking-for.btn-primary').data('id');
			if(_bookType > 0) {
				_realCount = $('#participants').children('div').length + ($('.booking-for.btn-primary').data('id') != '2' ? 1 : 0);
				var _text = '<p id="participant-text">There ' + (_realCount == 1 ? 'is' : 'are') + ' <a href="#" class="back-link" id="back-public-participant">' + _realCount + ' ' + (_realCount == 1 ? 'person' : 'people') + '</a> being booked:</p><ul id="participant-ul">';
				$.each($('.participant-match'), function (k, v) {
					_text += '<li>' + $(v).data('given') + ' ' + $(v).data('surname') + '</li>';
				});
				_text += '</ul>';
				$('#booking-summary').append(_text);
			}
			$('#progress-6').addClass('active');

			if(data.errors) {
				$('#public-match-button').attr('disabled', 'disabled');
			} else {
				$('#public-match-button').removeAttr('disabled');
			}
		}
	});
}

$(document).on('click', '#show-coupon', function(e) {
	trackClick($(this));
	e.preventDefault();
	$('#grp-coupon-inner').slideDown();
});

$(document).on('click', '#show-invoice-details', function(e) {
	trackClick($(this));
	e.preventDefault();
	$('#div-invoice-details').slideDown();
});

$(document).on('click', '#show-conf-details', function(e) {
	trackClick($(this));
	e.preventDefault();
	$('#div-conf-details').slideDown();
});

$(document).on('blur', '#participants .form-control', function(e) {
	$('#page-public-match').hide();
	$('#page-payment').hide();
	$('#page-finish').hide();
	$('#page-error').hide();
});

$(document).on('click', '#public-match-button', function(e) {
	trackClick($(this));
	if($('.training-course.active').data('coupon').length > 0) {
		//Apply the coupon.
		var _coupon = $('.training-course.active').data('coupon');
		$('#coupon-code').val(_coupon);
		$('#apply-coupon').click();
	}

	if($('#allow_invoice').val() == "1") {
		$('#invoice_contact_words').hide();
	} else {
		$('#invoice_contact_words').show();
	}

	if(_formType == 'Portal' || $('#allow_invoice').val() == "1" || $('#allow_invoice').val() == "2") {
		if($('#allow_invoice').val() == "1") {
			$('#btn-credit').hide();
			$('#btn-invoice').hide();
			$('#div-credit').hide();
			$('#div-invoice').show();
		} else {
			$('#btn-credit').show();
			$('#btn-invoice').show();
			$('#div-credit').hide();
			$('#div-invoice').hide();
		}
	} else {
		$('#btn-credit').hide();
		$('#btn-invoice').hide();
		$('#div-credit').show();
		$('#div-invoice').hide();
	}

	if($('#total-amount').val() == 0) {
		//Hide a lot of things on the payment page;
		zeroDollarFixes();
	}

	$('#page-start-1').show();
	if($('.training-type.active').data('id') != 2) {
		$('#page-start-2').show();
	} else {
		$('#page-start-2').hide();
	}
	$('#page-courses').show();
	$('#page-corporate-participants').hide();
	$('#page-corporate-calendar').hide();
	$('#page-corporate-contact').hide();
	$('#page-corporate-address').hide();
	$('#page-public-instance').show();
	$('#page-public-contact').show();
	$('#page-public-participants').show();
	$('#page-public-match').show();
	$('#page-payment').hide();
	$('#page-finish').hide();
	$('#page-error').hide();

	$('#progress-7').addClass('active');

	scrollPage('#page-payment');

	$('#coupon-errors').hide();
	$('#coupon-success').hide();
	$('#apply-coupon').removeAttr('disabled').removeClass('disabled');
	$('#grp-coupon').show();
});

function zeroDollarFixes() {
	$('#btn-credit').hide();
	$('#btn-invoice').hide();
	$('#div-invoice').hide();
	$('#grp-coupon').hide();
	$('#div-invoice p').hide();
	//payment form is inside div-credit
	$('#div-credit').show();
	$('#payment-form').hide();
	$('#payment-button').text('Confirm');
}

$(document).on('click', '.booking-for', function(e) {
	e.preventDefault();
	trackClick($(this));

	$('.booking-for').removeClass('btn-primary').addClass('btn-default');
	$(this).removeClass('btn-default').addClass('btn-primary');
});

$(document).on('click', '#date-button', function(e) {
	trackClick($(this));

	$('#error-date').hide();
	$('#error-start-time').hide();

	_error = false;
	if($('#selected-date').val().length == 0) {
		$('#error-date').show();
		_error = true;
	}

	if($('#start-time').val() == 0) {
		$('#error-start-time').show();
		_error = true;
	}

	if(!_error) {
    	$('#progress-5').addClass('active');
		$('#date-text').remove();
		$('#booking-summary').append('<p id="date-text">The course will be on the<br><a href="#" class="back-link" id="back-date">' + nicedate($('#selected-date').val()) + ', starting at ' + $('#start-time').val() + '</a></p>');
		scrollPage('#page-corporate-contact');

        flashSummary();
    }
});

$(document).on('click', '#contact-button', function(e) {
	trackClick($(this));

	$('#error-first').hide();
    $('#error-last').hide();
    $('#error-email').hide();
    $('#error-number').hide();
    $('#error-organisation').hide();

	_error = false;
	if($('#contact-first').val().trim().length == 0) {
		$('#error-first').show();
		_error = true;
	}
	
	if($('#contact-last').val().trim().length == 0) {
		$('#error-last').show();
		_error = true;
	}
	
	if($('#contact-email').val().trim().length == 0) {
		$('#error-email').show();
		_error = true;
	}
	
	if($('#contact-number').val().trim().length == 0) {
		$('#error-number').show();
		_error = true;
	}
	
	if($('#contact-organisation').val().trim().length == 0) {
		$('#error-organisation').show();
		_error = true;
	}
	
	if(!_error) {
		scrollPage('#page-corporate-address');

    	$('#progress-6').addClass('active');
    	$('#contact-text').remove();
    	$('#booking-summary').append('<p id="contact-text">The point of contact will be<br><a href="#" class="back-link" id="back-contact">' + $('#contact-first').val() + ' ' + $('#contact-last').val() + '</a></p>');
    	$('#contactEmail').val($('#contact-email').val());
    	$('#myEmail').text($('#contact-email').val());

        flashSummary();
    }
});

$(document).on('click', '#address-button', function(e) {
	trackClick($(this));

	_error = false;
	
	if($('#address-street').val().trim().length == 0) {
		$('#error-street').show();
		_error = true;
	}
	
	if($('#address-city').val().trim().length == 0) {
		$('#error-city').show();
		_error = true;
	}
	
	if($('#address-state').val().trim().length == 0) {
		$('#error-state').show();
		_error = true;
	}
	
	if($('#address-postcode').val().trim().length == 0) {
		$('#error-postcode').show();
		_error = true;
	}
	
	if(!_error) {
		scrollPage('#page-payment');

		$('#coupon-errors').hide();
		$('#coupon-success').hide();
		$('#grp-coupon').hide();
    	$('#progress-7').addClass('active');
		$('#address-text').remove();
    	$('#booking-summary').append('<p id="address-text">The course will be held at<br><a href="#" class="back-link" id="back-address">' + $('#address-street').val() + '<br>' + $('#address-city').val() + ' ' + $('#address-state').val() + ' ' + $('#address-postcode').val() + '</a></p>');

        flashSummary();
    }
});

$(document).on('click', '#payment-button', function(e) {
	trackClick($(this));

	//If online the T&C button must be checked
	$('#online-tc-alert').hide();
	if(!$('#online-tc').is(':hidden') && !$('#online-tc-check').is(':checked')) {
		$('#online-tc-alert').show();
	} else {
		if(!$('#online-tc').is(':hidden') && !$('#confirmation-check').is(':checked')) {
			$('#online-tc-alert').show();
		} else {
			$(this).html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', 'disabled');
			if ($('#payment-form').is(':hidden')) {
				//probably safe to assume it's a $0 booking, right?
				processPayment(0);
			} else {
				getToken();
			}
		}
	}
});

$(document).on('click', '#confirmation-check', function(e) {
	$('#online-tc-alert').hide();
});

$(document).on('click', '#online-tc-check', function(e) {
	$('#online-tc-alert').hide();
});

$(document).on('click', '#error-contact', function(e) {
	$('.error-block').removeClass('active');
    $(this).addClass('active');
	scrollPage('#page-error-contact');
    $('#page-error-phone').slideUp();
});

$(document).on('click', '#error-phone', function(e) {
	trackClick($(this));

    $('.error-block').removeClass('active');
    $(this).addClass('active');
	scrollPage('#page-error-phone');
    $('#page-error-contact').slideUp();
});

$(document).on('click', '#btn-credit', function(e) {
	trackClick($(this));

    e.preventDefault();
    $(this).removeClass('btn-default').addClass('btn-primary');
    $('#btn-invoice').removeClass('btn-primary').addClass('btn-default');
    $('#div-invoice').hide();
    $('#div-credit').slideDown();
});

$(document).on('click', '#btn-invoice', function(e) {
	trackClick($(this));

    e.preventDefault();
    $(this).removeClass('btn-default').addClass('btn-primary');
    $('#btn-credit').removeClass('btn-primary').addClass('btn-default');
    $('#div-credit').hide();
    if($('#allow_invoice').val() == "0") {
		$('#div-invoice-details').slideDown();
	}
    $('#div-invoice').slideDown();
});

$(document).on('blur', '#public-contact-first', function(e) {
	updateInvoiceContactPublic();
});

$(document).on('blur', '#public-contact-last', function(e) {
	updateInvoiceContactPublic();
});

$(document).on('blur', '#public-contact-email', function(e) {
	updateInvoiceContactPublic();
});

$(document).on('blur', '#contact-first', function(e) {
	updateInvoiceContactCorp();
});

$(document).on('blur', '#contact-last', function(e) {
	updateInvoiceContactCorp();
});

$(document).on('blur', '#contact-email', function(e) {
	updateInvoiceContactCorp();
});

$(document).on('blur', '#contact-organisation', function(e) {
	updateInvoiceContactCorp();
});

function updateInvoiceContactPublic() {
	$('#invoice-contact-first').val($('#public-contact-first').val());
	$('#invoice-contact-last').val($('#public-contact-last').val());
	$('#invoice-contact-email').val($('#public-contact-email').val());
	$('#confirm_email').val($('#public-contact-email').val());
	$('#invoice-contact-organisation').val('');
}

function updateInvoiceContactCorp() {
	$('#invoice-contact-first').val($('#contact-first').val());
	$('#invoice-contact-last').val($('#contact-last').val());
	$('#invoice-contact-email').val($('#contact-email').val());
	$('#confirm_email').val($('#contact-email').val());
	$('#invoice-contact-organisation').val($('#contact-organisation').val());
}

$(document).on('click', '.back-link', function(e) {
	if($('#iid').val() > 0) {
		return;
	}
	trackClick($(this));

    e.preventDefault();
    $('#booking-summary p').hide();
    $('#participant-ul').hide();

    $('#page-error-phone').hide();
    $('#page-error-contact').hide();

    $('.prefilled').removeClass('prefilled');
	var _id = $(this).attr('id');
    switch(_id) {
		case 'back-state' :
			$('#summary-total').hide();
			$('.training-type').removeClass('active');
			$('.training-state').removeClass('active');
			$('.training-location').removeClass('active');
			$('.training-course').removeClass('active');
			$('.online-delivery').removeClass('active');
			scrollPage('#page-state');
			_parent = $('#page-state').parents('.booking-page-container').nextAll().hide();
			break;

		case 'back-location' :
			$('#summary-total').hide();
			$('.training-type').removeClass('active');
			$('.training-location').removeClass('active');
			$('.training-course').removeClass('active');
			$('.online-delivery').removeClass('active');
			scrollPage('#page-location');
			_parent = $('#page-location').parents('.booking-page-container').nextAll().hide();
			break;

		case 'back-type' :
			$('#summary-total').hide();
			$('.training-type').removeClass('active');
			$('.training-course').removeClass('active');
			$('.online-delivery').removeClass('active');
			scrollPage('#page-type');
			_parent = $('#page-type').parents('.booking-page-container').nextAll().hide();
			break;

        case 'back-course' :
			$('#summary-total').hide();
			$('.training-course').removeClass('active');
			$('.online-delivery').removeClass('active');
			scrollPage('#page-courses');
			_parent = $('#page-courses').parents('.booking-page-container').nextAll().hide();
			$('#location-text').show();
            break;

        case 'back-participant':
			$('#page-start-1').show();
			if($('.training-type.active').data('id') != 2) {
				$('#page-start-2').show();
			} else {
				$('#page-start-2').hide();
			}
			$('#page-courses').show();
			$('#page-corporate-participants').show();
			$('#page-corporate-calendar').hide();
			$('#page-corporate-contact').hide();
			$('#page-corporate-address').hide();
			$('#page-public-instance').hide();
			$('#page-public-contact').hide();
			$('#page-public-participants').hide();
			$('#page-public-match').hide();
			$('#page-payment').hide();
			$('#page-finish').hide();
			$('#page-error').hide();
			var _scroll = $('#current-page').scrollTop() + $('#page-corporate-participants').position().top;
			$('#current-page').animate({scrollTop: _scroll}, 500);

			$('#progress-4').removeClass('active');
            $('#progress-5').removeClass('active');
            $('#progress-6').removeClass('active');
            $('#progress-7').removeClass('active');
            $('#location-text').show();
			$('#course-text').show();
            break;

        case 'back-date':
			$('#page-start-1').show();
			if($('.training-type.active').data('id') != 2) {
				$('#page-start-2').show();
			} else {
				$('#page-start-2').hide();
			}
			$('#page-courses').show();
			$('#page-corporate-participants').show();
			$('#page-corporate-calendar').show();
			$('#page-corporate-contact').hide();
			$('#page-corporate-address').hide();
			$('#page-public-instance').hide();
			$('#page-public-contact').hide();
			$('#page-public-participants').hide();
			$('#page-public-match').hide();
			$('#page-payment').hide();
			$('#page-finish').hide();
			$('#page-error').hide();
			var _scroll = $('#current-page').scrollTop() + $('#page-corporate-calendar').position().top;
			$('#current-page').animate({scrollTop: _scroll}, 500);

			$('#progress-5').removeClass('active');
            $('#progress-6').removeClass('active');
            $('#progress-7').removeClass('active');
			$('#location-text').show();
			$('#course-text').show();
			$('#participant-text').show();
			break;

        case 'back-contact':
			$('#page-start-1').show();
			if($('.training-type.active').data('id') != 2) {
				$('#page-start-2').show();
			} else {
				$('#page-start-2').hide();
			}
			$('#page-courses').show();
			$('#page-corporate-participants').show();
			$('#page-corporate-calendar').show();
			$('#page-corporate-contact').show();
			$('#page-corporate-address').hide();
			$('#page-public-instance').hide();
			$('#page-public-contact').hide();
			$('#page-public-participants').hide();
			$('#page-public-match').hide();
			$('#page-payment').hide();
			$('#page-finish').hide();
			$('#page-error').hide();
			var _scroll = $('#current-page').scrollTop() + $('#page-corporate-contact').position().top;
			$('#current-page').animate({scrollTop: _scroll}, 500);

			$('#progress-6').removeClass('active');
            $('#progress-7').removeClass('active');
            $('#location-text').show();
			$('#course-text').show();
			$('#participant-text').show();
			$('#date-text').show();
			break;

        case 'back-address':
			$('#page-start-1').show();
			if($('.training-type.active').data('id') != 2) {
				$('#page-start-2').show();
			} else {
				$('#page-start-2').hide();
			}
			$('#page-courses').show();
			$('#page-corporate-participants').show();
			$('#page-corporate-calendar').show();
			$('#page-corporate-contact').show();
			$('#page-corporate-address').show();
			$('#page-public-instance').hide();
			$('#page-public-contact').hide();
			$('#page-public-participants').hide();
			$('#page-public-match').hide();
			$('#page-payment').hide();
			$('#page-finish').hide();
			$('#page-error').hide();
			var _scroll = $('#current-page').scrollTop() + $('#page-corporate-address').position().top;
			$('#current-page').animate({scrollTop: _scroll}, 500);

			$('#progress-7').removeClass('active');
			$('#location-text').show();
			$('#course-text').show();
			$('#participant-text').show();
			$('#date-text').show();
			$('#contact-text').show();
            break;

		case 'back-public-instance' :
			$('#page-start-1').show();
			if($('.training-type.active').data('id') != 2) {
				$('#page-start-2').show();
			} else {
				$('#page-start-2').hide();
			}
			$('#page-courses').show();
			$('#page-corporate-participants').hide();
			$('#page-corporate-calendar').hide();
			$('#page-corporate-contact').hide();
			$('#page-corporate-address').hide();
			$('#page-public-instance').show();
			$('#page-public-contact').hide();
			$('#page-public-participants').hide();
			$('#page-public-match').hide();
			$('#page-payment').hide();
			$('#page-finish').hide();
			$('#page-error').hide();
			var _scroll = $('#current-page').scrollTop() + $('#page-public-instance').position().top;
			$('#current-page').animate({scrollTop: _scroll}, 500);

			$('#progress-4').removeClass('active');
			$('#progress-5').removeClass('active');
			$('#progress-6').removeClass('active');
			$('#progress-7').removeClass('active');
			$('#location-text').show();
			$('#course-text').show();
			break;

		case 'back-public-contact':
			$('#page-start-1').show();
			if($('.training-type.active').data('id') != 2) {
				$('#page-start-2').show();
			} else {
				$('#page-start-2').hide();
			}
			$('#page-courses').show();
			$('#page-corporate-participants').hide();
			$('#page-corporate-calendar').hide();
			$('#page-corporate-contact').hide();
			$('#page-corporate-address').hide();
			$('#page-public-instance').show();
			$('#page-public-contact').show();
			$('#page-public-participants').hide();
			$('#page-public-match').hide();
			$('#page-payment').hide();
			$('#page-finish').hide();
			$('#page-error').hide();
			var _scroll = $('#current-page').scrollTop() + $('#page-public-contact').position().top;
			$('#current-page').animate({scrollTop: _scroll}, 500);

			$('#progress-5').removeClass('active');
			$('#progress-6').removeClass('active');
			$('#progress-7').removeClass('active');
			$('#location-text').show();
			$('#course-text').show();
			$('#date-text').show();
			break;

		case 'back-public-participant':
			$('#page-start-1').show();
			if($('.training-type.active').data('id') != 2) {
				$('#page-start-2').show();
			} else {
				$('#page-start-2').hide();
			}
			$('#page-courses').show();
			$('#page-corporate-participants').hide();
			$('#page-corporate-calendar').hide();
			$('#page-corporate-contact').hide();
			$('#page-corporate-address').hide();
			$('#page-public-instance').show();
			$('#page-public-contact').show();
			$('#page-public-participants').show();
			$('#page-public-match').hide();
			$('#page-payment').hide();
			$('#page-finish').hide();
			$('#page-error').hide();
			var _scroll = $('#current-page').scrollTop() + $('#page-public-participants').position().top;
			$('#current-page').animate({scrollTop: _scroll}, 500);

			$('#progress-6').removeClass('active');
			$('#progress-7').removeClass('active');
			$('#location-text').show();
			$('#course-text').show();
			$('#date-text').show();
			$('#participant-text').show();
			$('#participant-ul').show();
			break;
    }

	updateLocationText();
});

$(document).on('change', '#participant-number', function() {
    updateCosts();
});

function updateCosts(show) {
	if(show === undefined) {
		show = true;
	}

    var _costs = 0;
    var _participants = $('#participant-number').spinner('value');
    var _text = '';

    if($('.training-type.active strong').text().trim() == 'Onsite') {
        if ($('.training-course.active').length > 0) {
            $.each($('.training-course.active').data('pricing'), function (k, v) {
                if (_participants >= parseInt(v.minimum_participants) && _participants <= parseInt(v.maximum_participants)) {
                    if (parseInt(v.pricing_type) == 0) {
                        _costs = parseFloat(v.price_per_participant).toFixed(2);
                    } else {
                        _costs = (_participants * parseFloat(v.price_per_participant)).toFixed(2);
                    }
                    _text = v.information;
                }
            });

			if($('.training-type.active').data('id') == '2' && $('.online-delivery.active').data('id') == '1') {
				_costs += (parseFloat($('#deliveryFee').val()) * _participants);
			}

            if (_costs.length > 0) {
                $('.total-amount').text('$' + _costs);
				$('#total-amount').val(_costs);
                $('#courseAmt').val(_costs * 100);
                $('#participant-info').text(_text);
                if(show) {
					$('#summary-total').show();
				}
            }
        }
    } else {
        //Public pricing
		_count = $('#participants').children('div').length + ($('.booking-for.btn-primary').data('id') != '2' ? $('.booking-for.btn-primary').data('id') == 3 ? 0 : 1 : 0);

		if(_count == 0 && $('.booking-for.btn-primary').data('id') == 3) {
			_count = 1;
		}

		_costs = $('#courseAmtEach').val() * _count;

		if($('.booking-for.btn-primary').data('id') == 3) {
			if($('#participants').children('div').length == 1) {
				_costs += $('.training-course.active').data('participant');
			}
		}

		if($('.training-type.active').data('id') == '2' && $('.online-delivery.active').data('id') == '1') {
			_costs += (parseFloat($('#deliveryFee').val()) * _count);
		}

		$('.total-amount').text('$' + _costs);
		$('#total-amount').val(_costs);
		$('#courseAmt').val(_costs * 100);
		if(_costs > 0 && show) {
			$('#summary-total').show();
		} else {
			$('#summary-total').hide();
		}
    }
}

function nicedate(_date) {
    return _date.substring(8,10) + '/' + _date.substring(5,7) + '/' + _date.substring(0,4);
}

$(document).on('keyup', 'input[name="cardnumber"]', function(e) {
	$('#payment-button').removeAttr('disabled').text('Pay and confirm');
});

$(document).on('keyup', 'input[name="exp-date"]', function(e) {
	$('#payment-button').removeAttr('disabled').text('Pay and confirm');
});

$(document).on('keyup', 'input[name="cvc"]', function(e) {
	$('#payment-button').removeAttr('disabled').text('Pay and confirm');
});

if($('#card-element').length > 0) {
	attachStripe();
}

function attachStripe() {
	/* Stripe */
	stripe = Stripe(_privkey);
	var elements = stripe.elements();
	// Custom styling can be passed to options when creating an Element.
	var style = {
		base: {
			// Add your base input styles here. For example:
			fontSize: '16px',
			color: '#32325d'
		}
	};

	// Create an instance of the card Element.
	cardNumber = elements.create('cardNumber', {style: style});
	cardExpiry = elements.create('cardExpiry', {style: style});
	cardCVC = elements.create('cardCvc', {style: style});

	// Add an instance of the card Element into the `card-element` <div>.
	cardNumber.mount('#card-element');
	cardExpiry.mount('#card-expiry');
	cardCVC.mount('#card-cvc');

	// Create a token or display an error when the form is submitted.
	var form = document.getElementById('payment-form');
	form.addEventListener('submit', function (event) {
		event.preventDefault();
		getToken();
	});
}

function getToken() {
    $('#card-errors').hide();

    var tokenData = {};
    tokenData.name = $('#back-contact').text();
    tokenData.address_country = 'AU';

    stripe.createToken(cardNumber, tokenData).then(function(result) {
        if (result.error) {
            // Inform the customer that there was an error.
            $('#card-errors').html(result.error.message);
            $('#card-errors').show();
            $('#payment-button').html('Pay and confirm').removeAttr('disabled');
        } else {
            // Send the token to your server.
            stripeTokenHandler(result.token);
        }
    });
}

function stripeTokenHandler(token) {
    // Insert the token ID into the form so it gets submitted to the server
    $('#stripeToken').val(token.id);
    processPayment(1);
}

$(document).on('click', '#invoice-button', function(e) {
	trackClick($(this));

    $(this).html('<i class="fa fa-spinner fa-pulse"></i>').attr('disabled', 'disabled');
    processPayment(0);
});

function processPayment(_stripe) {
	if(_stripe === undefined) {
		_stripe = 1;
	}

	_type = $('.training-type.active strong').text().trim();
	$location =  $('.training-location.active').text().trim();
	if($location.length == 0) {
		$location = $('#back-location').text().trim();
	}
	_price = $('#total-amount').val();
	_priceEach = $('#courseAmtEach').val();
	_priceTotal = $('#courseAmt').val();
	_amount = parseFloat(_price) * 100;
	_template = $('.public-instance-button.active').data('template');

	//Send the form off to Stripe'
	if(_type == 'Onsite') {
		_email = $('#contact-email').val();
		_first = $('#contact-first').val();
		_last = $('#contact-last').val();
		_org = $('#contact-organisation').val();
		_phone = $('#contact-number').val();
		_participants = $('#participant-number').val();
		_participant_detail = null;
	} else {
		_email = $('#public-contact-email').val();
		_first = $('#public-contact-first').val();
		_last = $('#public-contact-last').val();
		_org = '';
		_phone = $('#public-contact-phone').val();
		if (_theParticipants !== undefined) {
			let _pList = [];
			$.each(_theParticipants, function (k, v) {
				_pList.push('public-participant-id%5B%5D=' + v);
			});

			_participants = _pList.join('&');
		} else {
			_participants = $('.public-participant').serialize();
		}

		if (_theParticipantDetails !== undefined) {
			_participant_detail = _theParticipantDetails;
		} else {
			_participant_detail = $('.public-participant-detail').serialize();
		}
	}

	formdata = {
		'first_name': _first,
		'last_name': _last,
		'email' : _email,
		'phone': _phone,
		'org' : _org,
		'position': $('#contact-position').val(),
		'takePayment' : _stripe,
		'token' : $('#stripeToken').val(),
		'amount' : _amount,
		'course_id' : $('.training-course.active').data('id'),
		'course_name': $('.training-course.active p strong').text().trim(),
		'location_id' : $location,
		'location_name' : $location,
		'ctype': $('.training-course.active').data('type'),
		'ctemp': _template,
		'state_name' : $('.training-state.active').text().trim(),
		'date': $('#selected-date').val(),
		'type': _type,
		'instance': $('#courseInstance').val(),
		'geelong' : $('#geelong').val(),
		'invoice_first' : $('#invoice-contact-first').val(),
		'invoice_last' : $('#invoice-contact-last').val(),
		'invoice_email' : $('#invoice-contact-email').val(),
		'invoice_org' : $('#invoice-contact-organisation').val(),
		'invoice_po' : $('#invoice-po').val(),
		'calendar': $location,
		'client': _org,
		'time' : $('#start-time').val(),
		'finish-time' : $('#fin-time').val(),
		'street_name': $('#address-street').val(),
		'city': $('#address-city').val(),
		'state': $('#address-state').val(),
		'postcode': $('#address-postcode').val(),
		'comments': $('#address-comments').val(),
		'price': _price,
		'priceEach': _priceEach,
		'priceTotal': _priceTotal,
		'participants': _participants,
		'participant-detail': _participant_detail,
		'coupon' : $('#coupon-code-valid').val(),
		'clientid' : $('#booking-clientid').val(),
		'gclid' : $('#booking-gclid').val(),
		'trafficsource' : $('#booking-trafficsource').val(),
		'createdfrom' : _formType,
		'clicks' : $('#clicks').val(),
		'thisURL' : $('#thisURL').val(),
		'referer' : $('#refererURL').val(),
		'template_id' : $('#template_id').val(),
		'confirm_email' : $('#confirm_email').val(),
		'conf_email' : $('#conf_email').val(),
		'slack_channel' : $('#slackChannel').val(),
		'custom_fields' : $('[name="custom_fields[]"]').serialize()
	};

	if($('#allow_invoice').val() == '1') {
		formdata.inv_guid = $('#xero_guid').val();
	}

	if(_type == 'Online') {
		switch($('.online-delivery.active').data('id')) {
			case 1:
				//Delivery
				formdata.to_deliver = 'Delivery';
				formdata.delivery_address = $('#online-address-street').val().trim();
				formdata.delivery_city = $('#online-address-city').val().trim();
				formdata.delivery_state = $('#online-address-state').val().trim();
				formdata.delivery_postcode = $('#online-address-postcode').val().trim();
				formdata.delivery_comments = $('#address-online-comments').val().trim();
				break;

			case 0:
				//Pickup
				formdata.to_deliver = 'Pickup';
				formdata.pickup_location = $('.pickup-location.active').data('id');
				break;

			case 2:
				//Face To Face
				formdata.to_deliver = 'Face-to-Face';
				break;
		}
	}

	$.ajax({
		'type' : 'POST',
		'url' : _baseurl + 'charge.php',
		'dataType' : 'json',
		'data' : formdata,
		success : function(data) {
			console.log(data);
			if(!data.testing && data.success) {
				if(typeof fbq === 'undefined') {
					//Do nothing.
				} else {
					fbq('track', 'Purchase', {'currency': 'AUD', 'value': _price});
				}
				processEmailSuccess((data.coupon ? 'coupon' : _type.toLowerCase()), _email);
			} else {
				$('#card-errors').html(data.message).show();
				$('#payment-button').html('Pay and confirm').removeAttr('disabled');
				//something about participants
				_theParticipants = data.participants['public-participant-id'];
				_theParticipantDetails = data['participant-details'];
				console.log(_theParticipants);
			}
		},
		error: function (jqXHR, textStatus, errorThrown) {
			console.log(data);
			$('#card-errors').html(jqXHR.responseJSON.message).show();
			$('#payment-button').html('Pay and confirm').removeAttr('disabled');
		}
	});
}

function processEmailSuccess(_type, _email) {
	$('#booking-summary').find('a').contents().unwrap();
	//console.log('email success');

	var url = $('#homeURL').val() + '/success-' + ($('.training-course.active').data('type') == 'p' ? 'online' : (_type == 'online' ? 'public' : _type)) + '.php';
	if(_formType == 'Website') {
		var form = $('<form action="' + url + '" method="post" style="display: none">' +
			'<input type="text" name="email" value="' + _email + '" />' +
			'<textarea name="message">' + $('#booking-summary').html() + '</textarea>' +
			'</form>');
		//console.log(form);
		$('body').append(form);
		form.submit();
	} else {
		$('#current-page').html('<h1>Congratulations</h1><p>The booking was successful and confirmation emails will be sent to ' + _email + '.</p>')
	}
}

function setUpMCB() {
	var spinner = $( "input[type=number]" ).spinner({
		stop: function(event, ui) {
			updateCosts();
		}
	});
	
	$("#booking_location").CreateMultiCheckBox({ width: '250px',
		defaultText : 'Select location(s)', height:'250px' });

	$(document).on("click", ".MultiCheckBox", function () {
		var detail = $(this).next();
		detail.show();
	});

	$(document).on("click", ".MultiCheckBoxDetailHeader input", function (e) {
		e.stopPropagation();
		var hc = $(this).prop("checked");
		$.each($(this).closest(".MultiCheckBoxDetail").find(".MultiCheckBoxDetailBody input"), function(k,v) {
			if($(v).parents('.cont').attr('style') === undefined) {
				$(v).prop("checked", hc);
			}
		});
		$(this).closest(".MultiCheckBoxDetail").next().UpdateSelect();
	});

	$(document).on("click", ".MultiCheckBoxDetailHeader", function (e) {
		var inp = $(this).find("input");
		var chk = inp.prop("checked");
		inp.prop("checked", !chk);
		$.each($(this).closest(".MultiCheckBoxDetail").find(".MultiCheckBoxDetailBody input"), function(k,v) {
			if($(v).parents('.cont').attr('style') === undefined) {
				$(v).prop("checked", !chk);
			}
		});
		$(this).closest(".MultiCheckBoxDetail").next().UpdateSelect();
	});

	$(document).on("click", ".MultiCheckBoxDetail .cont input", function (e) {
		e.stopPropagation();
		$(this).closest(".MultiCheckBoxDetail").next().UpdateSelect();

		var val = ($(".MultiCheckBoxDetailBody input:checked").length == $(".MultiCheckBoxDetailBody input").length)
		$(".MultiCheckBoxDetailHeader input").prop("checked", val);
	});

	$(document).on("click", ".MultiCheckBoxDetail .cont", function (e) {
		var inp = $(this).find("input");
		var chk = inp.prop("checked");
		inp.prop("checked", !chk);

		var multiCheckBoxDetail = $(this).closest(".MultiCheckBoxDetail");
		var multiCheckBoxDetailBody = $(this).closest(".MultiCheckBoxDetailBody");
		multiCheckBoxDetail.next().UpdateSelect();

		var val = ($(".MultiCheckBoxDetailBody input:checked").length == $(".MultiCheckBoxDetailBody input").length)
		$(".MultiCheckBoxDetailHeader input").prop("checked", val);
	});

	$(document).mouseup(function (e) {
		var container = $(".MultiCheckBoxDetail");
		if (!container.is(e.target) && container.has(e.target).length === 0) {
			if(container.is(':visible')) {
				getAxcelCourses();
			}
			container.hide();
		}
	});
}

var defaultMultiCheckBoxOption = { width: '220px', defaultText: 'Select Below', height: '200px' };

jQuery.fn.extend({
	CreateMultiCheckBox: function (options) {

		var localOption = {};
		localOption.width = (options != null && options.width != null && options.width != undefined) ? options.width : defaultMultiCheckBoxOption.width;
		localOption.defaultText = (options != null && options.defaultText != null && options.defaultText != undefined) ? options.defaultText : defaultMultiCheckBoxOption.defaultText;
		localOption.height = (options != null && options.height != null && options.height != undefined) ? options.height : defaultMultiCheckBoxOption.height;

		this.hide();
		this.attr("multiple", "multiple");
		var divSel = $("<div class='MultiCheckBox'>" + localOption.defaultText + "<span class='k-icon k-i-arrow-60-down'><svg aria-hidden='true' focusable='false' data-prefix='fas' data-icon='sort-down' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 320 512' class='svg-inline--fa fa-sort-down fa-w-10 fa-2x'><path fill='currentColor' d='M41 288h238c21.4 0 32.1 25.9 17 41L177 448c-9.4 9.4-24.6 9.4-33.9 0L24 329c-15.1-15.1-4.4-41 17-41z' class=''></path></svg></span></div>").insertBefore(this);
		divSel.css({ "width": localOption.width });

		var detail = $("<div class='MultiCheckBoxDetail'><div class='MultiCheckBoxDetailHeader'><input type='checkbox' class='mulinput' value='-1982' /><div>Select All</div></div><div class='MultiCheckBoxDetailBody'></div></div>").insertAfter(divSel);
		detail.css({ "width": parseInt(options.width) + 10, "max-height": localOption.height });
		var multiCheckBoxDetailBody = detail.find(".MultiCheckBoxDetailBody");

		this.find("option").each(function () {
			var val = $(this).attr("value");

			if (val == undefined)
				val = '';

			multiCheckBoxDetailBody.append("<div class='cont'><div><input type='checkbox' class='mulinput' value='" + val + "' data-state='" + $(this).data('state') + "'/></div><div>" + $(this).text() + "</div></div>");
		});

		multiCheckBoxDetailBody.css("max-height", (parseInt($(".MultiCheckBoxDetail").css("max-height")) - 28) + "px");
	},
	UpdateSelect: function () {
		var arr = [];

		this.prev().find(".mulinput:checked").each(function () {
			//console.log();
			if($(this).parents('.cont').attr('style') === undefined) {
				arr.push($(this).val());
			}
		});

		this.val(arr);
	},
});