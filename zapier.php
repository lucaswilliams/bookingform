<?php
include_once('../wp-config.php');
include_once('../wp-includes/wp-db.php');

global $wpdb;

var_dump($_POST);

if(isset($_POST['type']) && $_POST['type'] == 'delete') {
    $wpdb->delete($wpdb->prefix . 'rr_booking_dates',[
        'id' => $_POST['id']
    ]);
} else {
    $results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}rr_booking_dates WHERE id = '" . $_POST['id'] . "'", OBJECT);
    var_dump($results);
    
    $start = str_replace('T', ' ', substr($_POST['start'], 0, 19));
    $finish = str_replace('T', ' ', substr($_POST['finish'], 0, 19));
    
    if (count($results) > 0) {
        //it's in the DB, update it
        $wpdb->update($wpdb->prefix . 'rr_booking_dates', [
            'calendar' => $_POST['calendar'],
            'start' => $start,
            'finish' => $finish,
            'category' => $_POST['category']
        ], [
            'id' => $_POST['id']
        ]);
        
        echo 'updated';
    } else {
        $wpdb->insert($wpdb->prefix . 'rr_booking_dates', [
            'id' => $_POST['id'],
            'calendar' => $_POST['calendar'],
            'start' => $start,
            'finish' => $finish,
            'category' => $_POST['category']
        ]);
        
        echo 'inserted';
    }
}