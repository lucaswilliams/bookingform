<?php
//die();
require_once('./config.php');
$sessionid = uniqid();
header('HTTP/1.1 200 OK');

date_default_timezone_set('Australia/Sydney');
require_once((__DIR__).'/vendor/autoload.php');
//die();
$cq = false;
if(strpos($_SERVER['HTTP_HOST'], 'cq') !== false) {
    $cq = true;
}
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

use Stripe\Stripe;
use Stripe\HttpClient;

ob_start();
\Stripe\Stripe::setApiKey($stripe['secret_key']);
$curl = new \Stripe\HttpClient\CurlClient([
    CURLOPT_SSL_VERIFYHOST => false,
    CURLOPT_SSL_VERIFYPEER => false
]);
// tell Stripe to use the tweaked client
\Stripe\ApiRequestor::setHttpClient($curl);

ini_set('display_errors', 'off');

function strip_tags_content($text, $tags = '', $invert = FALSE) {
    
    preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
    $tags = array_unique($tags[1]);
    
    if(is_array($tags) AND count($tags) > 0) {
        if($invert == FALSE) {
            return preg_replace('@<(?!(?:'. implode('|', $tags) .')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
        }
        else {
            return preg_replace('@<('. implode('|', $tags) .')\b.*?>.*?</\1>@si', '', $text);
        }
    }
    elseif($invert == FALSE) {
        return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
    }
    return $text;
}

function log_error($text, $first_name, $last_name, $course_name, $course_date) {
    global $cq;
    //Log to LW #logging-rr channel
    /*$slack = new Slack('https://hooks.slack.com/services/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');
    $slack->setDefaultUsername($_SERVER['HTTP_HOST']);
    $slack->setDefaultChannel("#logging-rr");
    $message = new SlackMessage($slack);
    $message->setText($first_name.' '.$last_name.' just tried booking in '.$course_name.' on '.$course_date.', but got the error'."\n".$text);
    $message->send();*/
    
    //Log to RR #sales channel.  Not if localdev.
    if(strpos($_SERVER['HTTP_HOST'], '.local') === FALSE) {
        $rrslack = new Slack('https://hooks.slack.com/services/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');
        if(strpos($_SERVER['SERVER_NAME'], 'dev.') > 0) {
            $rrslack->setDefaultUsername("Booking Form Robot - Test Mode");
        } else {
            $rrslack->setDefaultUsername("Booking Form Robot");
        }
        $rrslack->setDefaultChannel("#booking-errors-from-website");
        if($cq) {
            $rrslack->setDefaultIcon("https://booking.cqfirstaid.com.au/img/favicon.jpg");
        } else {
            $rrslack->setDefaultIcon("https://www.realresponse.com.au/booking/img/realresponse.png");
        }
        $message = new SlackMessage($rrslack);
        $message->setText($first_name.' '.$last_name.' just tried booking in '.$course_name.' on '.$course_date.', but got the error'."\n".$text);
        $message->send();
    }
}

global $wpdb;
$wpdb->insert('wp_rr_booking_log', [
    'id' => $sessionid,
    'postdata' => print_r($_POST, true)
]);

$token = $_REQUEST['token'];
$amount = $_REQUEST['amount'];
$org = $_REQUEST['org'];
$type = $_REQUEST['type'];

$inv_first = $_REQUEST['invoice_first'];
$inv_last = $_REQUEST['invoice_last'];
$inv_email = $_REQUEST['invoice_email'];
$inv_org = $_REQUEST['invoice_org'];
$inv_po = $_REQUEST['invoice_po'];
$inv_guid = $_REQUEST['inv_guid'] ?? '';

$course = $_REQUEST['course_id'];
//get the course type.
$terms = get_the_terms($course, 'rfa_course_types');
$taxcode = 'EXEMPTOUTPUT';
$taxrate = 0;
if($type != 'Onsite') {
    $account_code = get_field('account_code_public', 5059);
    $bank_code = get_field('bank_account_code_public', 5059);
    if(is_array($terms) && count($terms) > 0) {
        $term_id = $terms[0]->term_id;
        $new_account_code = get_field('xero_account_code', 'rfa_course_types_' . $term_id);
        if (strlen($new_account_code) > 0) {
            $account_code = $new_account_code;
        }

        $gst_status = get_field('gst_status', 'rfa_course_types_' . $term_id);
        if ($gst_status == 1) {
            $taxcode = 'OUTPUT';
            $taxrate = 10;
        }
    }
} else {
    $account_code = get_field('account_code_onsite', 5059);
    $bank_code = get_field('bank_account_code_onsite', 5059);
}

$chargeid = '';
$customerid = '';
$success = true;
$message = 'Your payment was processed successfully';
$invId = 0;
$invoice_number = '';
$wpid = 0;

$instance = $_POST['instance'];
$first_name = $_POST['first_name'];
$last_name = $_POST['last_name'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$course = $_POST['course_id'];
$course_name = $_POST['course_name'];
$date = $_POST['date'];
$pickup_location = $_POST['pickup_location'];
parse_str(urldecode($_POST['participants']), $participants);
parse_str(urldecode($_POST['participant-detail']), $participant_details);

$errorMessages = [];

if($type != 'Onsite') {
    if (count($participant_details) > 0) {
        $headers = array(
            'wstoken: ' . $ws_token,
            'apitoken: ' . $api_token
        );

        foreach ($participant_details['public-participant-id'] as $key => $id) {
            //Do something.
            $pgiven = $participant_details['participant-given'][$key];
            $psurname = $participant_details['participant-surname'][$key];
            $pemail = $participant_details['participant-email'][$key];
            $pphone = $participant_details['participant-phone'][$key];

            $participant_names[] = [
                'given' => $pgiven,
                'surname' => $psurname,
                'email' => $pemail,
                'phone' => $pphone
            ];

            $participant_name_list[] = $pgiven . ' ' . $psurname;

            if ($id == 0) {
                $service_url = $ax_url . 'contact';

                $params = array(
                    'givenName' => $pgiven,
                    'surname' => $psurname,
                    'emailAddress' => $pemail,
                    'mobilephone' => $pphone
                );

                $fieldsstring = http_build_query($params);

                $curl = curl_init($service_url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $fieldsstring);
                if ($proxy) {
                    curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                }

                $curl_response = curl_exec($curl);
                $data = json_decode($curl_response);

                $databaseQuery = $wpdb->get_results("SELECT ax_person_in, ax_person_out from wp_rr_booking_log where id = '$sessionid'");
                $databaseInfo = $databaseQuery[0];
                $wpdb->update('wp_rr_booking_log', [
                    'ax_person_in' => $databaseInfo->ax_person_in."\n".print_r($params, true),
                    'ax_person_out' => $databaseInfo->ax_person_out."\n".print_r($data, true)
                ], ['id' => $sessionid]);
                if ($data->ERROR) {
                    //There was an error creating the contact in aXcelerate.  We should just log this
                    log_error($data->DETAILS, $first_name, $last_name, $course_name, $date);
                    $success = false;
                    $message = $data->DETAILS;
                }
                $participants['public-participant-id'][$key] = $data->CONTACTID;
                $participant_details['public-participant-id'][$key] = $data->CONTACTID;
            } else {
                $databaseQuery = $wpdb->get_results("SELECT ax_person_in, ax_person_out from wp_rr_booking_log where id = '$sessionid'");
                $databaseInfo = $databaseQuery[0];

                $url = $ax_url.'contact/'.$id;
                //var_dump($url);
                $fieldsstring = http_build_query(['emailAddress' => $pemail]);
                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($curl, CURLOPT_POSTFIELDS, $fieldsstring);
                if ($proxy) {
                    curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                }

                $curl_response = curl_exec($curl);
                $data = json_decode($curl_response);

                $wpdb->update('wp_rr_booking_log', [
                    'ax_person_in' => $databaseInfo->ax_person_in."\n".print_r($params, true)
                ], ['id' => $sessionid]);
            }
        }
    }
}

if($success) {
//STEP 1: STRIPE.  This way we can return a card error.
    $takepayment = $_POST['takePayment'];
    //$takepayment = 0;
    if ($takepayment == 1 && $amount > 0) {
        try {
            $cParams = array(
                'email' => $email,
                'source' => $token
            );
            $customer = \Stripe\Customer::create($cParams);
            $customerid = $customer->id;
        } catch (\Exception $ex) {
            $message = $ex->getMessage() . '<br>Please check your details, then use the "Pay and confirm" button to try again.';
            $success = false;
            log_error(print_r($ex->getMessage(), true), $first_name, $last_name, $course_name, $date);
        }

        if ($success) {
            try {
                $chargeArray = array(
                    'customer' => $customer->id,
                    'amount' => $amount,
                    'currency' => 'aud'
                );
                $charge = \Stripe\Charge::create($chargeArray);

                if (strcmp($charge->status, 'succeeded') == 0) {
                    $chargeid = $charge->id;
                } else {
                    $success = false;
                }
            } catch (\Exception $ex) {
                $message = $ex->getMessage() . '<br>Please check your details, then use the "Pay and confirm" button to try again.';
                $success = false;
                log_error(print_r($ex->getMessage(), true), $first_name, $last_name, $course_name, $date);
            }
        }
    }
}

//STEP 2: WPDB.  This way we capture EVERYONE where there's a successful payment, or if they don't need one.
if($success) {
    $participant_names = [];
    $participant_name_list = [];

    if (count($participant_details) > 0) {
        foreach ($participant_details['public-participant-id'] as $key => $id) {
            //Do something.
            $pgiven = $participant_details['participant-given'][$key];
            $psurname = $participant_details['participant-surname'][$key];
            $pemail = $participant_details['participant-email'][$key];
            $pphone = $participant_details['participant-phone'][$key];

            $participant_names[] = [
                'given' => $pgiven,
                'surname' => $psurname,
                'email' => $pemail,
                'phone' => $pphone
            ];

            $participant_name_list[] = $pgiven . ' ' . $psurname;
        }
    }

    $coupon_code = '';
    $coupon_cat = '';
    $track = false;
    $pcount = 0;

    if (strlen($_POST['coupon']) > 0) {
        $posts = new WP_Query([
            'title' => $_POST['coupon'],
            'post_type' => 'rfa_coupons'
        ]);

        if ($posts->have_posts()) {
            while ($posts->have_posts()) {
                $partCount = 1;
                if (is_array($participants['public-participant-id'])) {
                    $partCount = count($participants['public-participant-id']);
                }
                $posts->the_post();
                $post = get_post();
                //remove one use from the coupon
                $uses = get_field('uses_remaining', $post->ID);
                $pcount = get_field('usage_type', $post->ID) == 2 ? $partCount : 1;

                $ccd = get_the_terms($post->ID, 'rfa_coupon_types')[0];
                //echo '<pre>'; var_dump($ccd); echo '</pre>';
                if ($ccd->name != '(none)') {
                    $coupon_code = $_POST['coupon'] . ' - ' . $ccd->name;
                } else {
                    $coupon_code = $_POST['coupon'];
                }
                $track = get_field('tracking', $post->ID);
                if (strlen($coupon_code) > 0) {
                    $coupon_cat = ' [' . $coupon_code . ']';
                }
            }
        }
    }

    $instance_url = "https://realresponse.app.axcelerate.com/management/management2/ProgramStatus.cfm?PDataID=$instance";

    if($type != "Onsite") {
        $location = (strlen($_POST['location_name']) > 0 ? $_POST['location_name'] : $_POST['state_name']);
        $admin_email = "<p>A new public booking has arrived through the " . $_POST['createdfrom'] . ".</p>
<p>The details are as follows</p>
<ul>
<li>Course: $course_name</li>
<li>Location: $location</li>
<li>Course date: $date</li>
<li>aXcelerate URL: <a href='$instance_url'>$instance_url</a></li>
<li>Contact: $first_name $last_name<br>
$email<br>
$phone</li>

";

        $participants_email = "<li>Participants: <ul>";
        foreach ($participant_names as $part) {
            $participants_email .= "<li>" . $part['given'] . " " . $part['surname'] . "</li>
<li>Email: " . $part['email'] . "</li>
<li>Phone: " . $part['phone'] . "</li>

";
        }
        $participants_email .= "</ul></li>";

        $admin_email .= $participants_email;

        $admin_email .= "<li>Price: " . $_POST['price'] . "</li>
";

        if (strlen($coupon_code) > 0) {
            $admin_email .= "<li>Coupon: $coupon_code</li>
";
        }

        $admin_email .= '<li>Client ID: ' . $_POST['clientid'] . '</li>';
        $admin_email .= '<li>GCLID: ' . $_POST['gclid'] . '</li>';
        $admin_email .= '<li>Traffic Source: ' . $_POST['trafficsource'] . '</li>';
        $admin_email .= '<li>Page URL: ' . $_POST['thisURL'] . '</li>';
        $admin_email .= '<li>Referer: ' . $_POST['referer'] . '</li>';

        if($type == "Online") {
            switch($_POST['to_deliver']) {
                case 'Delivery':
                    $admin_email .= '<li>They have indicated they need equipment dropped off/collected</li>';
                    $admin_email .= '<li>' . $_POST['delivery_address'] . '<br>';
                    $admin_email .= $_POST['delivery_city'] . ' ' . $_POST['delivery_state'] . ' ' . $_POST['delivery_postcode'] . '</li>';
                    $admin_email .= '<li>' . $_POST['delivery_comments'] . '</li>';
                    break;

                case 'Pickup':
                    $admin_email .= '<li>They have indicated they will pick up equipment</li>';
                    $admin_email .= '<li><strong>Pickup Location:</strong> ' . $pickup_location . '</li>';
                    break;

                case 'Face-to-Face':
                    $admin_email .= '<li>They have indicated they will train face-to-face</li>';
                    break;
            }
        }

        $admin_email .= "</ul>";
    } else {
        if($takepayment > 0) {
            $admin_invoice = "Paid with Credit Card.";
        } else {
            //Get the extra words
            $posts = get_posts([
                'post_type' => 'page',
                'name' => 'booking'
            ]);
            if($posts) {
                $page = $posts[0];
            }

            $invoice_words = "
        
    ".get_field('invoice_text', $page->ID);

            $admin_invoice = "An invoice is required.";
        }

        //var_dump($admin_invoice);
        //die();

        if(strlen($comments) > 0) {
            $comment = "

Additional Information: $comments";
        }

        $time = $_POST['time'];
        $finishtime = $_POST['finish-time'];
        $price = $_POST['price'];
        $client = $_POST['client'];
        $street = $_POST['street_name'];
        $city = $_POST['city'];
        $state = $_POST['state'];
        $postcode = $_POST['postcode'];
        $location = (strlen($_POST['location_name']) > 0 ? $_POST['location_name'] : $_POST['state_name']);

        $description = "<p>Dear $first_name,</p>
<p>Your booking for training has been received and is being processed by our team. You will receive a formal confirmation email shortly.$invoice_words</p>

<p>For your information here are your booking details:</p>
<ul>
<li>Course type: $course_name</li>
<li>Location: $location</li>
<li>Number of participants: ".$_POST['participants']."</li>
<li>Course date: $date</li>
<li>Preferred time: $time - $finishtime</li>
<li>Contact: $first_name $last_name ".(strlen($position) > 0 ? '('.$position.')' : '')."</li>
<li>Address: $street<br>
$city $state $postcode</li>
</ul>
<p>$comment</p>";
        if($cq) {
            $description .= "<p>Feel free to contact our team at any point with any questions you may have at training@cqfirstaid.com.au or on (07) 4978 1112.</p>";
        } else {
            $description .= "<p>Feel free to contact our team at any point with any questions you may have at training@realresponse.com.au or on 03 9021 8156.</p>";
            }

$description .= "<p>Regards<br>
The Real Response Team</p>";

        $admin_email = "<p>A new booking has arrived through the ".$_POST['createdfrom'].".</p>
<p>The details are as follows</p>
<ul>
<li>Course: $course_name</li>
<li>Location: $location</li>
<li>Course date: $date</li>
<li>Preferred time: $time - $finishtime</li>
<li>Participants: ".$_POST['participants']."</li>
<li>Price: $price</li>
<li>Contact: $first_name $last_name ".(strlen($position) > 0 ? '('.$position.')' : '')."<br>
$client<br>
$email<br>
$phone</li>
<li>Address: $street<br>
$city $state $postcode</li>
</ul>

<p>$comment</p>

<p>$admin_invoice</p>";

        $admin_email .= '<ul></ul><li>Client ID: '.$_POST['clientid'].'</li>';
        $admin_email .= '<li>GCLID: '.$_POST['gclid'].'</li>';
        $admin_email .= '<li>Traffic Source: '.$_POST['trafficsource'].'</li></ul>';
    }

    $participant_count = 0;
    if($type != 'Onsite') {
        $participant_count = count($participants['public-participant-id']);
    } else {
        $participant_count = $_POST['participants'];
    }

    $urlparts = explode('/', $_POST['thisURL']);

    $wpdb->insert('wp_rr_bookings', [
        'course_name' => $_POST['course_name'],
        'location_name' => $_POST['location_name'],
        'booking_contact' => $first_name . ' ' . $last_name,
        'booking_content' => $admin_email,
        'coupon' => $_POST['coupon'],
        'booking_date' => date('Y-m-d H:i:s'),
        'course_date' => $date,
        'course_id' => $course,
        'price' => $_POST['price'],
        'places' => $pcount,
        'participants' => $participant_count,
        'participant_names' => implode(', ', $participant_name_list),
        'clientid' => $_POST['clientid'],
        'gclid' => $_POST['gclid'],
        'trafficsource' => $_POST['trafficsource'],
        'stripe_payment_id' => $chargeid,
        'stripe_customer_id' => $customerid,
        'first_name' => $inv_first,
        'last_name' => $inv_last,
        'organisation' => $inv_org,
        'po_number' => $inv_po,
        'email' => $inv_email,
        'booking_url' => $urlparts[count($urlparts) - 1],
        'custom_fields' => $_POST['custom_fields']
    ]);
    $wpid = $wpdb->insert_id;

    $wpdb->update('wp_rr_booking_log', [
        'booking_id' => $wpid
    ], ['id' => $sessionid]);

    $zapierWebhook = 'https://hooks.zapier.com/hooks/catch/4343612/orosiyj/';
    //Log to RR #sales channel.  Not if localdev.
    if(strpos($_SERVER['HTTP_HOST'], '.local') === FALSE) {
        $slack = new Slack('https://hooks.slack.com/services/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');
        if(strpos($_SERVER['SERVER_NAME'], 'dev.') > 0) {
            $slack->setDefaultUsername("Booking Form Robot - Test Mode");
        } else {
            $slack->setDefaultUsername("Booking Form Robot");
        }
        if($type == 'Onsite') {
            $slack->setDefaultChannel("#onsite-bookings-from-website");
        } else {
            if($cq) {
                if($_POST['slack_channel'] == 'sales') {
                    $slack->setDefaultChannel("#cqfa-sales");
                } else {
                    $slack->setDefaultChannel("#".$_POST['slack_channel']);
                }
            } else {
                $slack->setDefaultChannel("#".$_POST['slack_channel']);
            }
        }
        $slack->setDefaultIcon("https://www.realresponse.com.au/booking/img/realresponse.png");
        $message = new SlackMessage($slack);
        $message->setText(strip_tags($admin_email));
        $message->send();
        $zapierWebhook = 'https://hooks.zapier.com/hooks/catch/1253732/ororlxx/';
    }

    if($type == 'Online' && $_REQUEST['ctype'] == 'w') {
        $price = $_POST['price'];
        //chuck em into Zapier/Google Sheets
        /*$fields = http_build_query(array(
            'FirstName' => $first_name,
            'LastName' => $last_name,
            'Email' => $email,
            'Phone' => $phone,
            'CourseName' => $course_name,
            'CourseDate' => $date,
            'Location' => $location,
            'Participants' => implode('; ', $participant_name_list),
            'Delivery' => (strlen($pickup_location) > 0 ? 'Pickup' : 'Deliver'),
            'DeliveryLocation' => (strlen($pickup_location) > 0 ? $pickup_location : $_POST['delivery_address'] . ' ' . $_POST['delivery_city'] . ' ' . $_POST['delivery_state'] . ' ' . $_POST['delivery_postcode']),
            'Price' => $price,
            'BookingFrom' => $_POST['createdfrom']
        ));*/

        $fields = http_build_query([
            'Status' => 'Booked',
            'BookingDate' => date('Y-m-d'), //'c' for ISO
            'FirstName' => $first_name,
            'LastName' => $last_name,
            'Email' => $email,
            'Phone' => $phone,
            'CourseName' => $course_name,
            'CourseDate' => $date,
            'SelectedState' => $_POST['state_name'],
            'SelectedCity' => $_POST['location_name'],
            'NumberOfParticipants' => count($participant_name_list),
            'NamesOfParticipants' => implode('; ', $participant_name_list),
            'DeliveryPickup' => $_POST['to_deliver'],
            'PickupLocation' => $pickup_location,
            'DeliveryLocation' => $_POST['delivery_address'] . ' ' . $_POST['delivery_city'] . ' ' . $_POST['delivery_state'] . ' ' . $_POST['delivery_postcode'],
            'Price' => $price,
            'Method' => $_POST['createdfrom']
        ]);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $zapierWebhook);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        if($proxy) {
            curl_setopt($ch, CURLOPT_PROXY, '127.0.0.1:8888');
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);
        $wpdb->update('wp_rr_booking_log', [
            'zapier_in' => $fields,
            'zapier_out' => $server_output
        ], ['id' => $sessionid]);

        curl_close($ch);
    }

    //send description to email address
    require_once 'PHPMailer/src/Exception.php';
    require_once 'PHPMailer/src/PHPMailer.php';
    require_once 'PHPMailer/src/SMTP.php';

    $mail = new PHPMailer2\PHPMailer\PHPMailer(true);                              // Passing `true` enables exceptions
    try {
        //Server settings
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = $mail_server;  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = $mail_user;                 // SMTP username
        $mail->Password = $mail_pass;                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $mail_port;                                    // TCP port to connect to

        if($type == "Onsite") {
            if($cq) {
                $mail->setFrom('training@cqfirstaid.com.au', 'CQ First Aid');
                $mail->addReplyTo('training@cqfirstaid.com.au', 'CQ First Aid');
            } else {
                $mail->setFrom('training@realresponse.com.au', 'Real Response');
                $mail->addReplyTo('training@realresponse.com.au', 'Real Response');
            }
            $mail->addAddress($email, $first_name.' '.$last_name);

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'Your Course Booking';
            $mail->Body    = str_replace("\r\n", '<br>', $description);
            $mail->AltBody = $description;

            $mail->send();

            $mail->clearAddresses();
            $mail->clearAllRecipients();

            //Send to admin
            if($cq) {
                $mail->setFrom('training@cqfirstaid.com.au', 'CQ First Aid');
            } else {
                $mail->setFrom('training@realresponse.com.au', 'Real Response');
            }

            $mail->addReplyTo($email, $first_name . ' ' . $last_name);

            if (intval($_REQUEST['geelong']) == 1) {
                $mail->addAddress('jross@realresponse.com.au', 'Real Response');
                $mail->addAddress('csnowden@realresponse.com.au', 'Real Response');
                $mail->addAddress('sgroen@realresponse.com.au', 'Real Response');
            } else {
                $mail->addAddress('sales@realresponse.com.au', 'Real Response');
            }

            //Content
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = 'New Course Booking';
            $mail->Body    = str_replace("\r\n", '<br>', $admin_email);
            $mail->AltBody = $admin_email;

            $mail->send();
        }

        //$mail->send();
        $msgs[] = 'Message has been sent';
    } catch (Exception $e) {
        $msgs[] = 'Message could not be sent. Mailer Error: ' . $mail->ErrorInfo;
    }

    //Now they're in wordpress, we put them into axcelerate.  ONLY IF IT'S PUBLIC
    if($type != 'Onsite') {
        $parts = array();
        $msgs = array();
        $particiapntCount = count($participants['public-participant-id']);
        $totalprice = $_POST['price'];
        $pricePer = $_POST['priceEach'];
        $priceTotal = $_POST['priceTotal'] / 100;

        foreach ($participants['public-participant-id'] as $key => $contact_id) {
            $service_url = $ax_url . 'course/enrol';

            $headers = array(
                'wstoken: ' . $ws_token,
                'apitoken: ' . $api_token
            );

            $params = array(
                'contactId' => $contact_id,
                'instanceId' => $instance,
                'type' => $_REQUEST['ctype'],
                'generateInvoice' => true,
                'cost' => floatval($pricePer),
                'discountIDList' => '1528',
                'suppressNotifications' => 'true',
                'FundingNational' => '20'
            );

            if(false) {
                $params['forceBooking'] = 'true';
            }

            $fieldsstring = http_build_query($params);

            $curl = curl_init($service_url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $fieldsstring);
            if ($proxy) {
                curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            }

            $curl_response = curl_exec($curl);
            $data = json_decode($curl_response);
            $databaseQuery = $wpdb->get_results("SELECT ax_enrol_in, ax_enrol_out from wp_rr_booking_log where id = '$sessionid'");
            $databaseInfo = $databaseQuery[0];
            $wpdb->update('wp_rr_booking_log', [
                'ax_enrol_in' => $databaseInfo->ax_enrol_in."\n".print_r($params, true),
                'ax_enrol_out' => $databaseInfo->ax_enrol_out."\n".print_r($data, true)
            ], ['id' => $sessionid]);

            if ($data->ERROR) {
                //There was an error enrolling in aXcelerate.  We should just log this
                log_error($data->DETAILS, $first_name, $last_name, $course_name, $date);
            } else {
                $invoice_id = $data->INVOICEID;
                $axIDs = [
                    'axcel_contact_id' => $contact_id,
                    'axcel_enrolment_id' => $data->LEARNERID,
                    'axcel_invoice_id' => $invoice_id,
                    'axcel_instance_id' => $instance
                ];
                //var_dump($axIDs);
                //echo $wpid;
                $wpdb->update('wp_rr_bookings', $axIDs, ['id' => $wpid]);

                //was a payment made, and successful?
                if (strlen($chargeid) > 0 || ($pricePer > 0 && $priceTotal == 0)) {
                    //record a payment against that invoice
                    $invParams = array(
                        'contactID' => $contact_id,
                        'invoiceID' => $invoice_id,
                        'amount' => $pricePer,
                        'reference' => $chargeid,
                        'paymentMethodID' => 2
                    );

                    $fieldsstring = http_build_query($invParams);
                    $service_url = $ax_url . 'accounting/transaction/';
                    $curl = curl_init($service_url);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($curl, CURLOPT_POST, true);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $fieldsstring);
                    if ($proxy) {
                        curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                    }
                    $curl_response = curl_exec($curl);
                } else {
                    if (strlen($_POST['invoice']) > 0) {
                        //echo 'Inserting '.$_POST['invoice'].'; '.$invoice_id.'<br>';
                        $wpdb->insert('xero_invoices', ['xero_guid' => $_POST['invoice'], 'axcelerate_id' => $invoice_id, 'amount' => $pricePer, 'contact_id' => $contact_id]);
                    }
                }

                //Now send the email.
                $params = array(
                    'contactID' => $contact_id,
                    'instanceID' => $instance,
                    'planID' => $_REQUEST['ctemp'],
                    'type' => $_REQUEST['ctype']
                );

                if($cq) {
                    $params['from'] = 12674291;
                }

                $fieldsstring = http_build_query($params);
                $service_url = $ax_url . 'template/email/';
                $curl = curl_init($service_url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $fieldsstring);
                if ($proxy) {
                    curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                }
                $curl_response = curl_exec($curl);

                $databaseQuery = $wpdb->get_results("SELECT template_request, template_response from wp_rr_booking_log where id = '$sessionid'");
                $databaseInfo = $databaseQuery[0];
                $wpdb->update('wp_rr_booking_log', [
                    'template_request' => $databaseInfo->template_request."\n".$params,
                    'template_response' => $databaseInfo->template_response."\n".$curl_response
                ], ['id' => $sessionid]);
            }
        }
    } //end aXcelerate

    if($type != "Onsite") {
        try {
            //XERO
            if ($amount > 0) {
                $xerosuccess = true;
                require_once('config.php');
                include_once('xero.class.php');
                $xeroObject = new \XeroFunctions($xeroclientid, $xerosecret, $first_name, $last_name, $course_name, $date);

                $xerosuccess = $xeroObject->getOrCreateXeroContact($inv_guid, $inv_org, $inv_first, $inv_last, $inv_email, $wpid, $sessionid);

                //Invoice Stuff
                if ($xerosuccess) {
                    $divisor = 100.0;

                    if ($_REQUEST['type'] != 'Onsite') {
                        //If this is a public course, with "Calculate GST" turned on, we need to divide by 110 as the price "Includes GST".
                        if ($gst_status == 1) {
                            $divisor = 110.0;
                        }
                    }
                    $xerosuccess = $xeroObject->getOrCreateXeroInvoice($xerosuccess, $gst_status, $amount, $course, $account_code, $taxcode, $wpid, $divisor, $sessionid, $inv_po);

                    $xeroInvoiceId = $xerosuccess->getInvoiceId();
                    $invoice_number = $xerosuccess->getInvoiceNumber();
                }

                //$chargeid = 'testcharge';
                //Payment Stuff
                if ($xerosuccess && $chargeid != '') {
                    $xeroObject->addPaymentToInvoice($xerosuccess, ($amount / 100), $chargeid, $wpid, $bank_code, $sessionid);
                }

                if ($xeroInvoiceId) {
                    if($cq) {
                        $pdf = $xeroObject->getXeroInvoicePDF($xeroInvoiceId);
                        //echo '<pre>'; var_dump($pdf); echo '</pre>';
                        $path = $pdf->getPathName();
                        //echo $path.'<br>';

                        //var_dump(file_exists($path));
                        $invoice_pdf = $invoice_path.'/'.$invoice_number.'.pdf';
                        $move = copy($path, $invoice_pdf);
                        //var_dump($move);
                        //Attach to an email from training@cqfirstaid.com.au
                        $body = '<p>Please find attached your invoice, '.$invoice_number.', for your recent course booking with CQ First Aid.</p><p>Please note that CQ First Aid has recently been acquired by Real Response Pty Ltd. We are still trading under the same name but our ABN has changed to 49164984490 and our RTO number is now 45022. Please also note that our bank account has changed as per the attached invoice.</p>';
                        $mail = new PHPMailer2\PHPMailer\PHPMailer(true);                                // Passing `true` enables exceptions
                        try {
                            //Server settings
                            $mail->isSMTP();                                      // Set mailer to use SMTP
                            $mail->Host = $mail_server;  // Specify main and backup SMTP servers
                            $mail->SMTPAuth = true;                               // Enable SMTP authentication
                            $mail->Username = $mail_user;                 // SMTP username
                            $mail->Password = $mail_pass;                           // SMTP password
                            $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
                            $mail->Port = $mail_port;                                    // TCP port to connect to

                            $mail->setFrom('training@cqfirstaid.com.au', 'CQ First Aid');
                            $mail->addReplyTo('training@cqfirstaid.com.au', 'CQ First Aid');
                            $mail->addAddress($email, $first_name . ' ' . $last_name);

                            //Content
                            $mail->isHTML(true);                                  // Set email format to HTML
                            $mail->Subject = 'Your Course Invoice';
                            $mail->Body = $body;
                            $mail->AltBody = $body;
                            $mail->AddAttachment($invoice_pdf, $invoice_number.'.pdf');

                            $mail->send();
                        } catch (Exception $ex) {
                            //
                        }
                    } else {
                        //Just send through xero
                        $xeroObject->emailInvoice($xeroInvoiceId);
                    }
                }
            }
        } catch (\Exception $ex) {
            //Just fail nicely.  This will be an admin issue for RR.  Maybe send a Slack Message?
            log_error($ex->getMessage(), $first_name, $last_name, $course_name, $date);
        }
    }
}

$successmessage = [
    'success' => $success,
    'message' => $message,
    'participants' => $participants,
    'participant-details' => http_build_query($participant_details)
];

if(!$cq) {
//Template Time!
    $find = [
        '[[first_name]]',
        '[[last_name]]',
        '[[email]]',
        '[[phone]]',
        '[[course_name]]',
        '[[date]]',
        '[[student_list]]',
        '[[location]]'
    ];

    $replace = [
        $first_name,
        $last_name,
        $email,
        $phone,
        $course_name,
        $date,
        $participants_email,
        $location
    ];

    if (strlen($_POST['confirm_email']) > 0) {
        //send the confirmation, either single or multi.
        if (count($participant_name_list) > 1) {
            //Send the multi person one
            $templateQuery = "SELECT content FROM email_templates WHERE multi_participant = 1";
        } else {
            //Send the single person one
            $templateQuery = "SELECT content FROM email_templates WHERE single_participant = 1";
        }
        $wpdb->select($portaldb);
        $templates = $wpdb->get_results($templateQuery, ARRAY_A);
        if (count($templates) > 0) {
            $content = str_replace($find, $replace, $templates[0]['content']);
            try {
                //Server settings
                $mail->isSMTP();
                $mail->Host = $mail_server;
                $mail->SMTPAuth = true;
                $mail->Username = $mail_user;
                $mail->Password = $mail_pass;
                $mail->SMTPSecure = 'tls';
                $mail->Port = $mail_port;
                $mail->clearAddresses();
                $mail->clearAllRecipients();

                $mail->setFrom('training@realresponse.com.au', 'Real Response');
                $mail->addReplyTo('training@realresponse.com.au', 'Real Response');
                $mail->addAddress($_POST['confirm_email']);

                //Content
                $mail->isHTML(true);
                $mail->Subject = 'Your Course Booking';
                $mail->Body = str_replace("\r\n", '<br>', $content);
                $mail->AltBody = $content;

                $mail->send();
            } catch (Exception $e) {
                $msgs[] = 'Confirmation email could not be sent. Mailer Error: ' . $mail->ErrorInfo;
            }
        }
    }

    if ($_POST['template_id'] > 0) {
        //we need the content of the template from the portal db, and to then send it to the specified email address.
        $template_id = $_POST['template_id'];
        $wpdb->select($portaldb);
        $templates = $wpdb->get_results("SELECT content FROM email_templates WHERE id = " . $template_id, ARRAY_A);
        if (count($templates) > 0) {
            $content = str_replace($find, $replace, $templates[0]['content']);

            try {
                //Server settings
                $mail->isSMTP();
                $mail->Host = $mail_server;
                $mail->SMTPAuth = true;
                $mail->Username = $mail_user;
                $mail->Password = $mail_pass;
                $mail->SMTPSecure = 'tls';
                $mail->Port = $mail_port;
                $mail->clearAddresses();
                $mail->clearAllRecipients();

                $mail->setFrom('training@realresponse.com.au', 'Real Response');
                $mail->addReplyTo('training@realresponse.com.au', 'Real Response');
                $mail->addAddress($_POST['conf_email']);

                //Content
                $mail->isHTML(true);
                $mail->Subject = 'Your Course Booking';
                $mail->Body = str_replace("\r\n", '<br>', $content);
                $mail->AltBody = $content;

                $mail->send();
            } catch (Exception $e) {
                $msgs[] = 'Confirmation email could not be sent. Mailer Error: ' . $mail->ErrorInfo;
            }
        }
        //Back to normal wordpress db.
        $wpdb->select(DB_NAME);
    }

    $any_errors = ob_get_clean();
    ob_end_flush();
    $wpdb->select(DB_NAME);
    $wpdb->update('wp_rr_booking_log', [
        'success_message' => $any_errors . $successmessage
    ], ['id' => $sessionid]);
}

if($_SERVER['HTTP_HOST'] == 'realresponse.local') {
    //Die on localhost.  I need to be able to check out charge.php and resend easily.
    $successmessage['testing'] = true;
}
echo json_encode($successmessage);