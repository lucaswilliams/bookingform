<?php
    header('HTTP/1.1 200 OK');
    include 'config.php';
    
    $service_url = $ax_url . 'course/instance/search';

    function sortCoursesByDate($a, $b)
    {
        if ($a->STARTDATE == $b->STARTDATE) {
            return 0;
        }
        return ($a->STARTDATE < $b->STARTDATE) ? -1 : 1;
    }
    
    switch($_GET['lid']) {
        case 'WA' :
            date_default_timezone_set('Australia/Perth');
            break;
            
        default:
            date_default_timezone_set('Australia/Sydney');
            break;
    }
    $posts = 50;
    if(isset($_GET['number'])) {
        $posts = $_GET['number'];
    }

    $postcount = $posts;
    if(isset($_GET['more_button'])) {
        $postcount++;
    }

    $getType = 'w';
    if(isset($_GET['type'])) {
        $getType = $_GET['type'];
    }
    
    //Is this a course post (e.g. 5183) instead of aXcelerate codes (e.g. 41290, 65346)
    
    $data = [];
    $templates = [];
    $blended_courses = [];
    if(isset($_GET['venue_ids']) && count($_GET['venue_ids']) > 0) {
        $venues = explode(',', $_GET['venue_ids']);
    } else {
        if(isset($_GET['state'])) {
            //If state is set and venues isn't, it's probably the course details page.
            //Get location from aXcelerate
            $headers = array(
                'WSToken: ' . $ws_token,
                'APIToken: ' . $api_token
            );
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $ax_url . 'venues/?displayLength=50');
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            if ($proxy) {
                curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            }

            $curl_response = curl_exec($curl);
            curl_close($curl);
            $venuedata = json_decode($curl_response);
            $venues = [];
            foreach($venuedata as $venue) {
                if($venue->SSTATE == $_GET['state']) {
                    $venues[] = $venue->CONTACTID;
                }
            }
        }
    }

    if(isset($_GET['aid'])) {
        $courseid = str_replace(' ', '', $_GET['aid']);
        $cids = explode(',', $courseid);
        $fields = get_field('axcelerate_options', $_GET['cid']);

        $private = false;
        $public = true;
        if(isset($_GET['private']) && $_GET['private'] == 1) {
            $private = true; //This is from the portal.
            $public = false;
        }

        foreach($fields as $field) {
            $cid = $field['course_id'];
            $type = 'w';
            if($field['axcelerate_type'] == 'p') {
                $type = $field['axcelerate_type'];
            }
            if($field['is_blended']) {
                $blended_courses[] = $cid;
            }

            //if company set and not private
            if($public && $type == 'w'){
                foreach ($venues as $venue_id) {
                    $post = array(
                        'type' => $type,
                        'displayLength' => $postcount,
                        'startDate_min' => date('Y-m-d', strtotime('-1 day')),
                        'startDate_max' => '2100-01-01',
                        'finishDate_min' => date('Y-m-d', strtotime('-1 day')),
                        'finishDate_max' => '2100-01-01',
                        'enrolmentOpen' => 1,
                        'sortColumn' => 9,
                        'ID' => $cid
                    );

                    if ($getType == 'p') {
                        $post['startDate_min'] = '2020-01-01';
                    }

                    if (isset($_GET['state'])) {
                        $post['state'] = $_GET['state'];
                    }

                    $templateQuery = "SELECT pm.post_id, pm.meta_value AS course_id, pm2.meta_value AS template_id 
    FROM wp_postmeta pm
    JOIN wp_postmeta pm2 ON pm2.post_id = pm.post_id AND pm2.meta_key = REPLACE(pm.meta_key, 'course_id', 'template_id')
    WHERE pm.meta_key LIKE 'axcelerate_options_%_course_id' AND pm.meta_value = " . $cid;

                    $templateResults = $wpdb->get_results($templateQuery);
                    foreach ($templateResults as $template) {
                        $templates[$template->course_id] = $template->template_id;
                    }

                    if (isset($_GET['lid']) && strlen($_GET['lid']) > 0) {
                        $post['state'] = $_GET['lid'];
                    }

                    if ($venue_id > 0) {
                        $post['venueContactId'] = $venue_id;
                    }

                    $postfields = http_build_query($post);

                    $headers = array(
                        'WSToken: ' . $ws_token,
                        'APIToken: ' . $api_token,
                        'Expect: ',
                        'Content-Length: ' . strlen($postfields)
                    );

                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $service_url);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($curl, CURLOPT_POST, 1);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    if ($proxy === true) {
                        curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                    }

                    $curl_response = curl_exec($curl);
                    curl_close($curl);
                    $ret = json_decode($curl_response);
                    $newret = [];
                    foreach ($ret as $myret) {
                        $myret->course_id = $cid;
                        $myret->pubpriv = 0;
                        $newret[] = $myret;
                    }
                    $data = array_merge($data, $newret);
                }
            }

            if($private && $type == 'p') {
                //We do another get that isn't dependent on venue, but rather the contact being from the org
                //Get the private ones
                $post = array(
                    'type' => $type,
                    'displayLength' => $postcount,
                    'startDate_min' => date('Y-m-d', strtotime('-1 day')),
                    'startDate_max' => '2100-01-01',
                    'finishDate_min' => date('Y-m-d', strtotime('-1 day')),
                    'finishDate_max' => '2100-01-01',
                    'enrolmentOpen' => 0,
                    'sortColumn' => 9,
                    'ID' => $cid,
                    'orgID' => $_GET['company']
                );

                $templateQuery = "SELECT pm.post_id, pm.meta_value AS course_id, pm2.meta_value AS template_id 
FROM wp_postmeta pm
JOIN wp_postmeta pm2 ON pm2.post_id = pm.post_id AND pm2.meta_key = REPLACE(pm.meta_key, 'course_id', 'template_id')
WHERE pm.meta_key LIKE 'axcelerate_options_%_course_id' AND pm.meta_value = ".$cid;

                $templateResults = $wpdb->get_results($templateQuery);
                foreach($templateResults as $template) {
                    $templates[$template->course_id] = $template->template_id;
                }

                $postfields = http_build_query($post);

                $headers = array(
                    'WSToken: ' . $ws_token,
                    'APIToken: ' . $api_token,
                    'Expect: ',
                    'Content-Length: ' . strlen($postfields)
                );

                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $service_url);
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                if ($proxy === true) {
                    curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                }

                $curl_response = curl_exec($curl);
                curl_close($curl);
                $ret = json_decode($curl_response);
                $newret = [];
                foreach ($ret as $myret) {
                    //go to course/instance/detail
                    $myret->course_id = $cid;
                    $re = '/linked Course ID ([0-9]+)/m';
                    if(strpos($myret->NAME, $field['description']) !== false) {
                        preg_match_all($re, $myret->NAME, $matches, PREG_SET_ORDER, 0);

                        //Use the match result to get the linked course id
                        $postfields = http_build_query([
                            'type' => 'w',
                            'instanceID' => $matches[0][1]
                        ]);

                        $headers = array(
                            'WSToken: ' . $ws_token,
                            'APIToken: ' . $api_token,
                            'Expect: '
                        );

                        $curl2 = curl_init();
                        curl_setopt($curl2, CURLOPT_URL, $ax_url.'course/instance/detail?'.$postfields);
                        curl_setopt($curl2, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($curl2, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
                        if ($proxy === true) {
                            curl_setopt($curl2, CURLOPT_PROXY, '127.0.0.1:8888');
                            curl_setopt($curl2, CURLOPT_SSL_VERIFYHOST, 0);
                            curl_setopt($curl2, CURLOPT_SSL_VERIFYPEER, 0);
                        }

                        $curl_response = curl_exec($curl2);
                        curl_close($curl2);
                        $cidret = json_decode($curl_response);
                        //echo '<pre>'; var_dump($cidret); echo '</pre>';
                        $cidret->pubpriv = 1;
                        $newret[] = $cidret;
                    }
                }
                $data = array_merge($data, $newret);
            }
        }
    }

    if(isset($_GET['aid']) && isset($_GET['cid'])) {
        if($_GET['aid'] == $_GET['cid']) {
            $cid = $_GET['aid'];

            foreach ($venues as $venue_id) {
                $post = array(
                    'type' => $type,
                    'displayLength' => $postcount,
                    'startDate_min' => date('Y-m-d', strtotime('-1 day')),
                    'startDate_max' => '2100-01-01',
                    'finishDate_min' => date('Y-m-d', strtotime('-1 day')),
                    'finishDate_max' => '2100-01-01',
                    'enrolmentOpen' => 1,
                    'sortColumn' => 9,
                    'ID' => $cid
                );

                if ($getType == 'p') {
                    $post['startDate_min'] = '2020-01-01';
                }

                if (isset($_GET['state'])) {
                    $post['state'] = $_GET['state'];
                }

                $templateQuery = "SELECT pm.post_id, pm.meta_value AS course_id, pm2.meta_value AS template_id 
    FROM wp_postmeta pm
    JOIN wp_postmeta pm2 ON pm2.post_id = pm.post_id AND pm2.meta_key = REPLACE(pm.meta_key, 'course_id', 'template_id')
    WHERE pm.meta_key LIKE 'axcelerate_options_%_course_id' AND pm.meta_value = " . $cid;

                $templateResults = $wpdb->get_results($templateQuery);
                foreach ($templateResults as $template) {
                    $templates[$template->course_id] = $template->template_id;
                }

                if (isset($_GET['lid']) && strlen($_GET['lid']) > 0) {
                    $post['state'] = $_GET['lid'];
                }

                if ($venue_id > 0) {
                    $post['venueContactId'] = $venue_id;
                }

                $postfields = http_build_query($post);

                $headers = array(
                    'WSToken: ' . $ws_token,
                    'APIToken: ' . $api_token,
                    'Expect: ',
                    'Content-Length: ' . strlen($postfields)
                );

                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $service_url);
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                if ($proxy === true) {
                    curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                }

                $curl_response = curl_exec($curl);
                curl_close($curl);
                $ret = json_decode($curl_response);
                $newret = [];
                foreach ($ret as $myret) {
                    $myret->course_id = $cid;
                    $myret->pubpriv = 0;
                    $newret[] = $myret;
                }
                $data = array_merge($data, $newret);
            }
        }
    }

    //echo '<pre>'; var_dump($data); echo '</pre>'; die();

    //re-order a bunch of things
    usort($data, function($a, $b) {
        if($a->STARTDATE > $b->STARTDATE) {
            return 1;
        } elseif($a->STARTDATE < $b->STARTDATE) {
            return -1;
        } else {
            return 0;
        }
    });

    if(is_array($data) && count($data) > 0) {
        echo '<div class="course-grid container">';

        usort($data, 'sortCoursesByDate');
        //echo '<pre>'; var_dump($data); echo '</pre>';
        $coursedata = [];
        $locations = [];

        foreach ($data as $c) {
            $venuecontact = $c->VENUECONTACTID ?? 0;
            if (!isset($coursedata[$venuecontact])) {
                $coursedata[$venuecontact] = [];

                if ($venuecontact > 0) {
                    //Get location from aXcelerate
                    $headers = array(
                        'WSToken: ' . $ws_token,
                        'APIToken: ' . $api_token
                    );
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $ax_url . 'contact/' . $venuecontact);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    if ($proxy) {
                        curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                    }

                    $curl_response = curl_exec($curl);
                    curl_close($curl);
                    $venuedata = json_decode($curl_response);

                    $locations[$venuecontact] = [
                        'name' => $venuedata->GIVENNAME,
                        'address' => trim($venuedata->ADDRESS1 . ' ' . $venuedata->ADDRESS2) . ', ' . $venuedata->CITY . ' ' . $venuedata->STATE,
                        'street' => trim($venuedata->ADDRESS1 . ' ' . $venuedata->ADDRESS2),
                        'city' => $venuedata->CITY,
                        'state' => $venuedata->STATE
                    ];
                } else {
                    $locations[$venuecontact] = [
                        'name' => 'Online',
                        'address' => 'Online',
                        'street' => '',
                        'city' => '',
                        'state' => ''
                    ];
                }
            }
            if(isset($_GET['widget']) && $_GET['widget'] == 1) {
                $coursedata[1][] = $c;
            } else {
                $coursedata[$venuecontact][] = $c;
            }
        }
        
        //Need a good way to use this in the front-end that isn't inefficient.
        $discountprices = $wpdb->get_results('SELECT * FROM wp_axcel_courses WHERE coursedate >= NOW() AND (newprice IS NOT null OR availability > 0 OR priority = 1) ORDER BY coursedate');
        $discounts = [];
        foreach($discountprices as $discount) {
            $dis = new stdClass();
            $dis->price = $discount->newprice;
            $dis->available = $discount->availability;
            $dis->priority = $discount->priority;
            $discounts[$discount->id] = $dis;
        }

        //echo '<pre>'; var_dump($discounts); echo '</pre>';
        //echo '<pre>'; var_dump($coursedata); echo '</pre>'; die();
        echo '    <div class="row header">
                    Available Sessions
                    <a href="#" id="showFull" style="color: #FFFFFF; position: absolute; right: 50px;
">Show Full Courses</a>
                </div>';

        foreach($coursedata as $location => $data) {
            $l = $locations[$location];
            //var_dump($l);
            //echo '<pre>'; var_dump($data); echo '</pre>';
            echo '<div class="axcel-location-courses" data-id="'.$location.'" '.($activeLoc == $location ? 'style="display: block"' : '').'>';
            for ($i = 0; $i < min(count($data), $posts); $i++) {
                if(is_array($data)) {
                    $instance = $data[$i];
                } else {
                    $instance = $data;
                }
                //echo '<pre>'; var_dump($instance); echo '</pre>';
                $cid = $instance->ID;
                $priority = false;
                $cost = $instance->COST;
                $costmodified = false;
                if (isset($discounts[$instance->INSTANCEID])) {
                    $dis = $discounts[$instance->INSTANCEID];
                    //var_dump($dis);
                    if(!$dis->priority) {
                        $cost = $dis->price;
                        $costmodified = true;
                        $available = $dis->available;
                    } else {
                        $priority = true;
                    }
                }

                //is there a price override passed through?
                if(isset($_GET['price'])) {
                    $cost = floatval($_GET['price']);
                    $costmodified = false;
                }

                //echo '<pre>'; var_dump($cost); echo '</pre>';

                if(!isset($_GET['priority']) || $_GET['priority'] == 1 || ($_GET['priority'] == 0 && !$priority)) {
                    $l = $locations[$instance->VENUECONTACTID];
                    //echo '<pre>'; var_dump($l); echo '</pre>';
                    if ($l == null) {
                        //make up an "online" location for them
                        $l = [
                            "name" => 'Online',
                            "address" => 'Your chosen virtual location'
                        ];
                    }
                    if (strlen($time) == 0) {
                        if (isset($_GET['days']) && $_GET['days'] > 0) {
                            $time = '-' . $_GET['days'] . ' weekdays + 3 hours';
                            //$time = ' -3 hours';
                        } else {
                            //If a time was not found above, we lock out by 3 (public) or 36 (online) hours
                            if (isset($_GET['lid'])) {
                                $time = '-3 hours';
                            } else {
                                $time = '-36 hours';
                            }
                        }
                    }
                    //if(true) {
                    if ($getType == 'p' || strtotime($instance->STARTDATE . ' ' . $time) > time()) {
                        echo '<div class="row session '.($instance->PARTICIPANTVACANCY > 0 ? '' : 'full').'">';
                        //echo '<div class="col-12 d-none d-md-block">'.$instance->NAME.'</div>';
                        echo '    <div class="col-10">
                        <div class="row">';
                        if ($instance->pubpriv == 1) {
                            echo '<div class="address col-12">';
                            echo $instance->LOCATION;
                        } else {
                            echo '<div class="address col-12 col-lg-6">';
                            echo '<a tabindex="0" role="button" data-toggle="popover" data-trigger="focus" title="' . $l['name'] . '" data-content="' . $l['address'] . '"><i class="fa fa-map-marker"></i></a> ' . $l['name'];
                        }
                        if (in_array(intval($cid), $blended_courses)) {
                            echo '<a tabindex="0" role="button" data-toggle="popover" data-trigger="focus" title="Blended Delivery" data-content="Due to COVID-19 this course is now being run as a blended course. This means you will complete more of the learning in your own time at home and only come to a short face to face practical assessment."><i class="fa fa-laptop" style="margin-left: 16px;"></i></a>';
                        }
                        echo ' <br>';
                        //echo $instance->course_id;
                        if ($getType == 'p') {
                            echo '<span class="date-text">Always available</span><br>&nbsp;';
                        } elseif ($instance->pubpriv == 1) {
                            echo '<span class="date-text">' . date('j F Y', strtotime($instance->STARTDATE)) . '</span><br>' . date('g:ia', strtotime($instance->STARTDATE)) . ' - ' . date('g:ia', strtotime($instance->FINISHDATE)) . '</span>';
                        } else {
                            echo '<span class="date-text">' . (isset($_GET['lid']) ? $instance->DATEDESCRIPTOR : date('j F Y', strtotime($instance->STARTDATE))) . '</span><br>' . date('g:ia', strtotime($instance->STARTDATE)) . ' - ' . date('g:ia', strtotime($instance->FINISHDATE));
                            if ($_GET['showTimezone'] == 1) {
                                if (strtotime($instance->STARTDATE) > strtotime('October 4 2020') && strtotime($instance->STARTDATE) < strtotime('April 4 2021')) {
                                    echo ' AEDT';
                                } else {
                                    echo ' AEST';
                                }
                            }
                        }
                        echo '</div><!-- address col-12 col-lg-8 -->';

                        //$cost = 0;
                        $available = 0;
                        if ($instance->pubpriv == 1) {
                            echo '    </div><!-- row -->';
                            echo '</div><!-- col-12 col-lg-9 -->';
                            $mytemplate = 46979;
                            if (isset($templates[$instance->course_id]) && strlen($templates[$instance->course_id]) > 0) {
                                $mytemplate = $templates[$instance->course_id];
                            }
                            echo '<div class="col-2 text-right">
                        <a href="#" class="btn btn-primary public-instance-button ' . ($getType == 'p' || $_GET['iid'] == $instance->INSTANCEID ? 'preclick' : '') . '" data-id="' . $instance->INSTANCEID . '" data-amt="' . $cost . '" data-date="' . substr($instance->STARTDATE, 0, 10) . '" data-lid="' . $instance->STATE . '" data-cid="' . $_GET['cid'] . '" data-places="' . $instance->PARTICIPANTVACANCY . '" data-template="' . $mytemplate . '" data-aid="' . $cid . '" data-available="' . $available . '">
                          <i class="fa fa-chevron-right"></i>
                        </a>
                      </div><!-- col-3 col-lg-2 text-right -->';
                        } else {
                            $placestext = '';
                            $pricetext = '';
                            if ($instance->PARTICIPANTVACANCY > 0 || $getType == 'p') {
                                $pricetext = '';
                                if ($costmodified) {
                                    $pricetext .= '<strike>$' . number_format($instance->COST, 2) . '</strike>';
                                }
                                if($cost > 0) {
                                    $pricetext .= '$' . number_format($cost, 2);
                                }

                                if ($instance->PARTICIPANTVACANCY < 6 && $getType == 'w') {
                                    $placestext = '  <span class="text-danger">Only ' . $instance->PARTICIPANTVACANCY . ' places left</span>';
                                }
                            }

                            echo '<div class="col-6 col-lg-4 d-lg-none">';
                            echo $pricetext;
                            echo '<br>';
                            echo $placestext;
                            echo '</div>';
                            if ($priority) {
                                echo '<div class="col-6 col-lg-2 text-right">
            <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus" title="Priority Course" data-content="Priority courses are generally available at shorter notice with more intimate group sizes to give excelled learning experiences.  Training equipment is sent immediately so you’re ready for your training."><img src="/booking/img/priority.svg" style="height: calc(100% - 18px);"></a>
         </div>';
                            } else {
                                echo '<div class="col-6 col-lg-2"></div>';
                            }
                            echo '<div class="col-6 col-lg-4 d-none d-lg-block text-lg-right">';
                            echo $pricetext;
                            echo '<br>';
                            echo $placestext;
                            echo '</div>';

                            echo '    </div>
                        </div>';
                            //echo '    <div class="col-3 col-md-1 text-right">'.$instance->PARTICIPANTVACANCY.'</div>';
                            if ($instance->PARTICIPANTVACANCY > 0 || $getType == 'p') {
                                $mytemplate = 46979;
                                if (isset($templates[$instance->course_id]) && strlen($templates[$instance->course_id]) > 0) {
                                    $mytemplate = $templates[$instance->course_id];
                                }

                                echo '<div class="col-2 text-right">
                        <a href="#" class="btn btn-primary public-instance-button ' . ($getType == 'p' || $_GET['iid'] == $instance->INSTANCEID ? 'preclick' : '') . '" data-id="' . $instance->INSTANCEID . '" data-amt="' . $cost . '" data-date="' . substr($instance->STARTDATE, 0, 10) . '" data-lid="' . $instance->STATE . '" data-cid="' . $_GET['cid'] . '" data-places="' . $instance->PARTICIPANTVACANCY . '" data-template="' . $mytemplate . '" data-aid="' . $cid . '" data-available="' . $available . '">
                          <i class="fa fa-chevron-right"></i>
                        </a>
                      </div>';
                            } else {
                                echo '<div class="col-2 text-right"><span class="text-danger">FULL</span></div><!-- col-3 col-lg-2 text-right -->';
                            }
                            //echo '</div>';
                        }
                        //echo '</div><!-- 12/4 -->';
                        echo '</div><!-- row session -->';
                    }
                }
            }
            echo '</div><!-- axcel-location-courses -->';
            //echo '<div style="display:none;">'.count($data).' '.$posts.' '.$postcount.'</div>';
            /*if (count($data) > $posts) {
                $toencode = '{"tid" : 1, "cid" : "' . $_GET['cid'] . '", "lid" : "' . $_GET['lpid'] . '"}';
                //echo $toencode;
                $link = get_site_url() . '/booking/' . base64_encode($toencode);
                echo '<div class="row" style="margin-top: 16px"><div class="col-12 text-right"><a href="' . $link . '" class="btn btn-primary">See more courses</a></div></div>';
            }*/
        }
        echo '</div><!-- course-grid-container -->';
    } else {
        echo '<div class="row">';
        echo '    <div class="col-12 text-center">';
        //var_dump($_GET['altLocation']);
        if($_GET['delType'] == 1) {
            //Public
            the_field('unavailable_public_course', 5059);
            if($_GET['altLocation'] === 'true') {
                the_field('also_delivered_online', 5059);
            }
        } else {
            the_field('unavailable_online_course', 5059);
            if($_GET['altLocation'] === 'true') {
                the_field('also_delivered_public', 5059);
            }
        }
        echo '    </div>
        </div>';
    }